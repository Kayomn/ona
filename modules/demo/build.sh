#!/bin/bash

mkdir -p out
gcc -std=c11 -fPIC -c -I../../source/ona-runtime ./src/app.c -o ./out/app.o -Wall -Werror
gcc -rdynamic -shared ./out/app.o -o ../../assets/libdemo.so
