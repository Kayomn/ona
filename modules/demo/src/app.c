#include "ona.h"

typedef struct {
	Vector2 position;
} PlayerData;

void PlayerInit(PlayerData* player, Device* device) {
	player->position = Vector2_Zero;

	WriteFile(OutFile, 16, "Initializing...\n", NULL);
}

void PlayerProcess(PlayerData* player, DeviceFrame* frame) {
	player->position = Vector2_Add(player->position, (Vector2){1, 1});

	// printf("%f, %f\n", player->position.x, player->position.y);
	// WriteFile(OutFile);
}

void PlayerExit(PlayerData* player) {
	WriteFile(OutFile, 12, "Exiting...\n", NULL);
}

void OnaModuleInit(Device* device) {
	RegisterDeviceSystem(device, sizeof(PlayerData), &PlayerInit, &PlayerProcess, &PlayerExit);
}
