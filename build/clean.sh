#!/bin/bash

cd ../source

for dir in ./*/
do
	(cd "$dir" && dub clean)
done
