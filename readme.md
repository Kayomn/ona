# Ona

## Introduction

### Overview

Ona is a general-purpose game engine designed for people who do not like the higher level abstractions that tools like Unity and Unreal try to offer.

### Goals

Ona is built with the following philosophy...

1. Build to solve problems, not abstract them.

    Game engines like Unity, Unreal, and Godot invest a lot of effort in hiding what the software is actually doing to push frames to the screen. Engines typically provide some abstract representation of a unit of code: either as a "game object", "entity", or object-oriented class.

    This kind of philosophy generally fragments code structure in exchange for a very pretty approximation to the naive human understanding of everything as some sort of object.

    Ona instead tries to encourage the use of game systems over game entities. An entire system is represented as a single unit of code, whereby each "entity" that acts under that system is updated as part of it. This may sound similar to ECS, however Ona does no follow its tenants exactly either due to it being another attempt at democraticing data in an overly generic way.

1. Errors should be expected and handled explicitly.

    Ona aims to minimize the scenarios where a process can "fail unexpectedly" to zero via the usage of a standardized `Result` type, error flags, boolean returns, and `null` pointers. This approach is also walked carefully along the line of not making the user-facing API too difficult to use with ridiculous amounts of error checking.

    A lot of focus is paid on what type of error something is. If a file could not be retrieved from the file-system that the runtime depends on then this is considered a "hard error" that will employ the usage of the aforementioned `Result` type. Meanwhile, things like allocation failures are considered to be soft errors, which should not need handling and instead validating for the expected result, which is typically a non-`null` value.

    The compromise of not making everything explicitly handle a hard error was decided due to the impracticality of forcing every allocation and every `String` instance to be manually unwrapped at the collsite, instead encouraging the checking of the returned data compared to an "unexpected result value".

1. Do not hide anything that reduces the developer's control over the runtime.

    As a side-effect of unnecessary abstarction, many game engines reduce the kind of real-world control a developer can have over their game's performance characteristics. Usually this comes in the form of some sort high-level scripting language runtime which the developer is restricted to interacting through.

    Instead, Ona provides a full C API to the engine with every conceivably usable functionality that is both built into the engine and needed. This API may be further extended by third parties if they so wish, providing support for other languages by wrapping it.

1. Where it is possible, avoid hiding any dynamic resource allocation in the engine.

    Unless either constrainted by a third-party dependency or the it is simply impractical, Ona will avoid hiding any dynamic allocations. Typically, anything that needs to allocate will have a parameter that accepts a `const (Allocator)*` for specificying the memory management method to use.

    Certain runtime features built into the core, like `String` and `Ref`, will prefer to use some internally defined allocator for the sake of brevity. For structures like `String` where the runtime is fully aware of the data that will be stored within it these choices make a lot of sense, and allow for further optimization that wouldn't have otherwise been possible.

1. Things should be structured in a predictable way, avoiding as much hidden state as possible.

    Ona avoids constructors, thread-global state, operator overloading, default arguments, and other behavioral abstractions as much as possible. Functions are named descriptively, global state of any kind of minimzed as much as possible to lower-level components, and constructors are avoided due to how they both shackle the return value of the function to a specific type and couple data initialization and complex logic.

1. Out of the box, no abstract object model should be enforced on the developer by the tooling.

    By default, the engine tooling will try its best to stay out of your way as it lets you loose on a pure code-based interface - similar to tools like Love2D. The native entry point provided to a module allows for initialization and de-initialization, allowing for job-like "systems" of code to be set up that may run asynchronously, or allow the programmer to write code on the main thread directly that has full interaction with everything.

    This approach allows the developer to build a codebase that is not based off abstract "entity" structures and instead allows the actual logic of the game to be expressed directly, through the use of direct data transformation.
