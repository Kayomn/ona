module ona.ascii;

/**
 * Returns `true` if `c` is a valid ASCII alphabet character, otherwise `false`.
 */
@safe
public bool isAlphabet(dchar c) pure nothrow {
	return (((c > dchar(64)) && (c < dchar(91))) || ((c > dchar(96)) && (c < dchar(123))));
}

/**
 * Returns `true` if `c` is a valid ASCII digit character, otherwise `false`.
 */
@safe
public bool isDigit(dchar c) pure nothrow {
	return ((c > dchar(47)) && (c < dchar(58)));
}

/**
 * Returns `true` if `c` is a valid ASCII whitespace character, otherwise `false`.
 *
 * ASCII whitespace characters are:
 *   - Space (0x20)
 *   - Horizontal Tab (0x09)
 *   - Newline LF (0x0A)
 *   - Vertical Tab (0x0B)
 *   - Feed (0x0C)
 *   - Carriage Return CR (0x0D)
 */
@safe
public bool isWhitespace(dchar c) pure nothrow {
	return ((c == ' ') || (c == '\t') || (c == '\n') || (c == '\v') || (c == '\f') || (c == '\r'));
}
