module ona.os.linux;

private {
	import ona.os.common;
	import ona.string;
	import core.stdc.errno;
	import core.sys.posix.dlfcn;
	import core.sys.posix.fcntl;
	import core.sys.posix.unistd;
}

/**
 * `FileHandle` wrapper that supplies additional metadata and methods of managing the system
 * resource.
 */
public struct File {
	/**
	 * Opaque handle to the operating system file representation.
	 */
	public int handle;

	/**
	 * Bit flags specifying the level of IO access that the `File` has.
	 *
	 * `IoFlags.unknown` is not to be confused with "no file". Use `File.exists` to check if a file
	 * is open.
	 */
	private IoFlags accessFlags;

	/**
	 * Returns `true` if `ioFlag` is present in `this.accessFlags`, otherwise `false`.
	 */
	private bool hasAccess(IoFlags ioFlag) nothrow {
		return cast(bool)(this.accessFlags & ioFlag);
	}

	/**
	 * Attempts to free resources allocated to granting file access and reset the `File` instance.
	 */
	public void release() nothrow {
		if (this.handle) {
			close(this.handle);
		}

		this = this.init;
	}
}

/**
 * Opaque handle to a unit of executable code loaded at runtime.
 */
public struct Library {
	/**
	 * Opaque pointer to the runtime-loaded binary data.
	 */
	void* handle;

	/**
	 * Attempts to release any dynamic resources allocated to the `Library` and unload it.
	 *
	 * Attempting to use references to symbols retrieved from the `Library` after it is released is
	 * undefined behavior and not advised.
	 */
	void release() nothrow {
		if (this.handle != null) {
			dlclose(this.handle);
		}

		this = this.init;
	}
}

/**
 * Max supported file path length on the current operating system.
 */
public enum filePathMax = 4096;

/**
 * Memory buffer used for resolving file paths as zero-terminating sentineled character buffers.
 */
private static char[filePathMax + 1] filePathBuffer;

/**
* Standard process output channel `File`.
*/
pragma(mangle, "OutFile")
public __gshared immutable (File) outFile = {
	handle: STDOUT_FILENO,
	accessFlags: IoFlags.write
};

/**
 * Returns `true` if a file exists and can be accessed by the process at `filePath`, otherwise
 * `false` if the file does not exist or the operation failed to complete.
 */
pragma(mangle, "CheckFile")
public extern (C) bool checkFile(String filePath) nothrow {
	return (access(sentineledBuffer(filePathBuffer, filePath.all()).ptr, F_OK) != -1);
}

/**
 * Attempts to open a file at `filePath` with `ioFlags` as its access flags. Should `openFile` fail
 * then a default-initialized `File` is returned and its errors are written to `error` if it is not
 * `null`.
 *
 * Failure to locate a file at `filePath` will result in a `FileError.notFound` error.
 *
 * Failure to resolve the path will result in a `FileError.badPath` error. The details of what
 * constitutes a "bad path" varies between operating systems, but generally it encompasses path
 * formatting errors and limitations imposed by the operating system.
 *
 * Failure to acquire the necessary access to the file at `filePath`, due to some OS-specific lock,
 * insufficient permissions, or otherwise will result in a `FileError.badAccess` error.
 *
 * Failure to open the file at `filePath` due to a lack of available resources will result in a
 * `FileError.outOfResources` error.
 *
 * Any other kind of uncategorizable, OS-specific error results in `FileError.os`.
 */
pragma(mangle, "OpenFile")
public extern (C) File openFile(String filePath, IoFlags ioFlags, FileOpenError* error) nothrow {
	if (filePath.count() > filePathMax) {
		if (error) *error = FileOpenError.badPath;
	} else {
		int flags;

		if (!checkFile(filePath)) {
			// File does not yet exist, create it.
			flags |= O_CREAT;
		}

		if (ioFlags & IoFlags.read) {
			flags |= O_RDONLY;
		}

		if (ioFlags & IoFlags.write) {
			flags |= O_WRONLY;
		}

		/**
		*         Read Write Execute
		*        -------------------
		* Owner | yes  yes   no
		* Group | yes  no    no
		* Other | yes  no    no
		*/
		File file = {
			handle: open(
				sentineledBuffer(filePathBuffer, filePath.all()).ptr,
				flags,
				(S_IRUSR | S_IRGRP | S_IROTH | S_IWUSR)
			),

			accessFlags: ioFlags
		};

		if ((file.handle == -1) && error) {
			handleError: switch (errno) with (FileOpenError) {
				case EMFILE, ENFILE, EDQUOT, ENOMEM, ENOSPC, EOVERFLOW: {
					*error = outOfResources;
				} break handleError;

				case EACCES, EFAULT,
						EISDIR, ENXIO, EOPNOTSUPP, EROFS, EPERM, ETXTBSY, EWOULDBLOCK, ENOTDIR: {

						*error = badAccess;
				} break handleError;

				case ELOOP, ENAMETOOLONG: {
					*error = badPath;
				} break handleError;

				case ENOENT: {
					*error = notFound;
				} break handleError;

				default: {
					*error = os;
				} break handleError;
			}
		}

		return file;
	}

	return File();
}

/**
 * Attempts to load a dynamic library of code from the location specified in `libPath`. Failure to
 * load the library will result in an invalid `Library` being returned, with the error code written
 * to `error` if it is not `null`.
 *
 * As dynamic libraries are a largely OS-specific feature, the formatting of `libPath` may vary
 * depending on the platform. For Windows and Unix systems, `libPath` is formatted the same way a
 * regular file system path would be - with a few caveats.
 *
 *   * On Unix systems a `libPath` that does not begin with relative (`"./"`) or absolute ("/")
 *     path will default to checking the linker path.
 *
 * `libPath` exceding the maximum length or using invalid syntax on the current OS will result in
 * a `LibraryError.badPath` error.
 *
 * Failure to locate a dynamic library at `libPath` will result in a `LibraryError.notFound` error.
 */
pragma(mangle, "OpenLibrary")
public extern (C) Library openLibrary(String libPath, LibraryError* error) nothrow {
	if (libPath.count() > filePathBuffer.length) {
		if (error) *error = LibraryError.badPath;
	} else {
		// void* libContext = dlopen(sentineledBuffer(filePathBuffer, libPath.all()).ptr, RTLD_NOW);
		void* libContext = null;

		if (libContext) {
			return Library(libContext);
		}

		if (error) *error = LibraryError.notFound;
	}

	return Library();
}

/**
 * Attempts to read the file at `file` up to either the maximum capacity of `outBuffer` or the end
 * of file. To adjust the amount of bytes read, pass a sub-slice of the target output buffer as the
 * `outBuffer`.
 *
 * The amount of bytes actually read into `outBuffer` are returned, with an error returning `0` and
 * writing the error to `error` if it is not `null`. The return value can be checked to confirm
 * that the expected number of bytes has been read.
 *
 * If `file` is invalid then the result is a `FileReadError.badFile` error.
 *
 * If the file at `file` fails to be read from, typically due to either lacking permissions or the
 * file / filesystem not being readable then the result is a `FileWriteError.badAccess` error.
 *
 * Otherwise, if an error that does not fit into any of the above error categories occurs, such as
 * an OS or hardware-level failure, then the result is a `FileWriteError.io` error.
 */
pragma(mangle, "ReadFile")
public extern (C) size_t readFile(File file, void[] outBuffer, FileReadError* error) nothrow {
	immutable (ptrdiff_t) bytesRead = read(file.handle, outBuffer.ptr, outBuffer.length);

	if (bytesRead > -1) {
		return cast(size_t)bytesRead;
	} else if (error) handleError: switch (errno()) with (FileReadError) {
		case EBADF: {
			*error = badFile;
		} break handleError;

		case EFAULT, EINVAL, EAGAIN, EISDIR: {
			*error = badAccess;
		} break handleError;

		default: {
			*error = io;
		} break handleError;
	}

	return 0;
}

/**
 * Attempts to find a symbol named `symbolName` in `library`, returning its address if found.
 * Failure to locate a symbol matching the name `symbolName` will instead return `null`.
 */
pragma(mangle, "SearchLibrary")
public extern (C) void* searchLibrary(Library library, string symbolName) {
	return dlsym(library.handle, symbolName.ptr);
}

/**
 * Attempts to seek through the file at `file` from the offset anchor `offset` by `base` bytes.
 *
 * The amount of bytes actually sought are returned, with an error returning `0` and writing the
 * error to `error` if it is not `null`. The return value can be checked to confirm that the
 * expected number of bytes has been read.
 *
 * If `file` is invalid then the result is a `FileSeekError.badFile` error.
 *
 * If the file at `file` fails to be seeked, typically due to an invalid seek destination or the
 * file / filesystem not being seekable, then the result is a `FileSeekError.badAccess` error.
 *
 * Otherwise, if an error that does not fit into any of the above error categories occurs, such as
 * an OS or hardware-level failure, then the result is a `FileSeekError.io` error.
 */
pragma(mangle, "SeekFile")
public extern (C) size_t seekFile(
		File file,
		FileOffset offset,
		int base,
		FileSeekError* error
	) nothrow {

	immutable (ptrdiff_t) bytesSought = lseek(file.handle, offset, base);

	if (bytesSought > -1) {
		return cast(size_t)bytesSought;
	} else if (error) handleError: switch (errno()) with (FileSeekError) {
		case EBADF: {
			*error = badFile;
		} break handleError;

		case EINVAL, ESPIPE: {
			*error = badAccess;
		} break handleError;

		default: {
			*error = io;
		} break handleError;
	}

	return 0;
}

/**
 * Attempts to write `buffer` to `file`.
 *
 * The amount of bytes actually written from `inBuffer` are returned, with an error returning `0`
 * and writing the error to `error` if it is not `null`. The returned value can be checked to
 * confirm that the expected number of bytes has been written.
 *
 * If `file` is invalid then the result is a `FileWriteError.badFile` error.
 *
 * If the file at `file` fails to be written to, typically due to either lacking permissions or the
 * file / filesystem not being writeable then the result is a `FileWriteError.badAccess` error.
 *
 * If the file at `file` is too big to be further written to, there's no more space in the current
 * file and / or filesystem, or some other OS-specific resource required to write to the file is
 * unavailable then the result is a `FileWriteError.outOfResources` error.
 *
 * Otherwise, if an error that does not fit into any of the above error categories occurs, such as
 * an OS or hardware-level failure, then the result is a `FileWriteError.io` error.
 */
pragma(mangle, "WriteFile")
public extern (C) size_t writeFile(
		File file,
		scope const (void)[] inBuffer,
		FileWriteError* error
	) nothrow {

	immutable (ptrdiff_t) bytesWritten = write(file.handle, inBuffer.ptr, inBuffer.length);

	if (bytesWritten > -1) {
		return cast(size_t)bytesWritten;
	} else if (error) handleError: switch (errno()) with (FileWriteError) {
		case EBADF: {
			*error = badFile;
		} break handleError;

		case EFAULT, EINVAL, EAGAIN, EDESTADDRREQ, EINTR, EPERM, EPIPE: {
			*error = badAccess;
		} break handleError;

		case EDQUOT, EFBIG, ENOSPC: {
			*error = outOfResources;
		} break handleError;

		default: {
			*error = io;
		} break handleError;
	}

	return 0;
}
