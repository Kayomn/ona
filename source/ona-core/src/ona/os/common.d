module ona.os.common;

/**
 * Offset anchors in a file for seeking from.
 */
public enum FileOffset {
	head,
	current,
	tail
}

/**
 * Error codes used when failing open a file on the file system.
 */
public enum FileOpenError {
	none,
	notFound,
	badAccess,
	badPath,
	outOfResources,
	os
}

/**
 * Error codes used when failing to read from the file system.
 */
public enum FileReadError {
	none,
	badFile,
	badAccess,
	io
}

/**
 * Error codes used when failing to seek through a file on the file system.
 */
public enum FileSeekError {
	none,
	badFile,
	badAccess,
	io
}

/**
 * Error codes used when failing to write to the file system.
 */
public enum FileWriteError {
	none,
	badFile,
	badAccess,
	outOfResources,
	io
}

/**
 * Error codes used when a dynamic library of code fails to open.
 */
public enum LibraryError {
	none,
	badPath,
	notFound
}

/**
 * Input and output access mode bit flags.
 */
public enum IoFlags {
	unknown = 0,
	read = 0x1,
	write = 0x2
}
