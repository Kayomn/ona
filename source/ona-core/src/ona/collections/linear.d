module ona.collections.linear;

private {
	import ona.memory;
}

/**
 * Appendable buffer of memory managed by the specified `ona.memory.Allocator`, or the process
 * default if nothing is specified.
 */
public struct Appender(Type) {
	private const (Allocator)* allocator;

	private Type[] buffer;

	private size_t count_;

	/**
	 * Constructs with the allocation strategy `allocator`.
	 *
	 * If `null` is supplied then the `Appender` will use the default runtime allocation strategy.
	 * Alternatively, if no specialized `ona.memory.Allocator` is needed then the default
	 * constructor will work as well.
	 */
	@safe
	public this(const (Allocator)* allocator) pure nothrow {
		this.allocator = allocator;
	}

	/**
	 * Retruns a slice containing all elements in the `Appender`.
	 *
	 * The returned slice may not address the full memory buffer and as such should not be assumed
	 * to do.
	 */
	@safe
	public Type[] all() pure nothrow {
		return this.buffer[0 .. this.count_];
	}

	/**
	 * Returns the `ona.memory.Allocator` used by the `Appender`.
	 *
	 * If `null` is returned then the `Appender` is using the default runtime allocation strategy.
	 */
	@safe
	public const (Allocator)* allocatorOf() pure nothrow {
		return this.allocator;
	}

	/**
	 * Attempts to append `element` to the end of the `Appender`, returning a pointer to the
	 * inserted value or `null` if the `Appender` has no `ona.memory.Allocator` assigned to it.
	 *
	 * If the `Appender` is full then the element buffer will be re-allocated for enough space to
	 * hold the existing elements and the new one multiplied by two (`(n + 1) * 2`).
	 *
	 * Appending will fail when the `Appender` is full and it cannot acquire more capacity.
	 */
	public Type* append(inout (Type) element) nothrow {
		return this.append(element);
	}

	/**
	 * Attempts to append `element` to the end of the `Appender`, returning a pointer to the
	 * inserted value or `null` if allocation fails.
	 *
	 * If the `Appender` is full then the element buffer will be re-allocated for enough space to
	 * hold the existing elements and the new one multiplied by two (`(n + 1) * 2`).
	 *
	 * Appending will fail when the `Appender` is full and it cannot acquire more capacity.
	 */
	public Type* append(ref inout (Type) element) nothrow {
		Type* bufferIndex = void;

		if (this.count_ >= this.buffer.length) {
			if (!this.reserve(this.count_ ? this.count_ : 2)) {
				// Allocation failure.
				return null;
			}
		}

		bufferIndex = (this.buffer.ptr + this.count_);
		// TODO(Kayomn): This isn't mutability-safe, is it?
		(*bufferIndex) = cast(Type)element;
		this.count_ += 1;

		return bufferIndex;
	}

	/**
	 * Attempts to append the contents of `elements` to the end of the `Appender`, returning a
	 * slice containing the inserted values or `null` if allocation fails.
	 *
	 * If the `Appender` is full then the element buffer will be re-allocated for enough space to
	 * hold both the existing and new elements multiplied by two (`(n + e) * 2`).
	 *
	 * Appending will fail when the `Appender` is full and it cannot acquire more capacity.
	 */
	public Type[] appendAll(inout (Type[]) elements) nothrow {
		if (this.reserve(elements.length)) {
			immutable (size_t) oldCount = this.count_;

			foreach (ref element; elements) {
				if (!this.append(element)) {
					return null;
				}
			}

			return this.buffer[oldCount .. $];
		}

		return null;
	}

	/**
	 * Returns the element at `index` by reference.
	 *
	 * If `index` is beyond the range of `0` to `Appender.count` then an assertion is raised.
	 */
	@safe
	public ref inout (Type) at(size_t index) pure inout nothrow {
		assert(index < this.count_);

		return this.buffer[index];
	}

	/**
	 * Returns the last element of the `Appender`.
	 *
	 * If the `Appender` is empty then an assertion is raised.
	 */
	@safe
	public ref Type back() pure nothrow {
		assert(this.count_);

		return this.buffer[this.count_ - 1];
	}

	/**
	 * Returns the number of elements that the `Appender` is capable of accomodating before re-
	 * allocating.
	 *
	 * This is not to be confused with `Appender.count`, which is the number of elements that are
	 * currently in the `Appender`.
	 */
	@safe
	public size_t capacity() pure nothrow {
		return this.buffer.length;
	}

	/**
	 * Clears all elements without altering the capacity of the `Appender`.
	 *
	 * If `Type` has a non-trivial destructor then it is invoked for each value in the `Appender`.
	 *
	 * For clearing the `Appender` and deallocating all memory used by it` see `Appender.release`
	 * instead.
	 */
	public void clear() nothrow {
		static if (hasDestructor!(Type)) {
			foreach (ref element; this.all()) {
				element.__xdtor();
			}
		}

		this.count_ = 0;
	}

	/**
	 * Compresses the capacity of the `Appender` down to the space required to store all elements
	 * currently in the list and no more via re-allocation. Attempting to append after a
	 * compression is a guaranteed allocation. `Appender.compress` returns `true` if it was
	 * successful, otherwise `false` if not.
	 *
	 * Attempting to compress an already compressed `Appender` will make the assumption that the
	 * action was intended and re-allocate regardless. Because of this, it is important to check
	 * that `Appender.capacity` is greater than `Appender.count` before calling
	 * `Appender.compress`.
	 *
	 * A call to `Appender.compress` will fail if a the requested `capacity` cannot be allocated.
	 * If this occurs it is worth checking that the `Appender` is not now empty, as the failing
	 * behavior of `ona.memory.realloc` may vary between `ona.memory.Allocator` implementations.
	 */
	public bool compress() nothrow {
		this.buffer = realloc(this.allocator, this.buffer, this.count_);

		return (this.buffer.ptr != null);
	}

	/**
	 * Returns the number of elements currently in the `Appender`.
	 *
	 * This is not to be confused with `Appender.capacity`, which is the number of elements that
	 * the `Appender` is capable of accomodating before re-allocating.
	 */
	@safe
	public size_t count() pure inout nothrow {
		return this.count_;
	}

	/**
	 * Returns the first element of the `Appender`.
	 *
	 * If the `Appender` is empty then an assertion is raised.
	 */
	@safe
	public ref Type head() pure nothrow {
		assert(this.count_);

		return this.buffer[0];
	}

	/**
	 * Returns a pointer to the beginning of the `Appender` memory buffer, granting raw access to
	 * the memory buffer.
	 *
	 * If the `Appender` has a capacity of `0` then the returned value is `null`.
	 */
	@safe
	public Type* ptr() pure nothrow {
		return &this.buffer[0];
	}

	/**
	 * Retruns a slice containing all elements in the `Appender` from `offset` to `length`.
	 *
	 * If `length` is greater than `Appender.count` an assertion is raised.
	 *
	 * Other assertions on incorrect argument usage may be raised if bounds checking is enabled.
	 */
	@safe
	public Type[] slice(size_t offset, size_t length) pure nothrow {
		assert(length <= this.count_);

		return this.buffer[offset .. length];
	}

	/**
	 * Returns the last element of the `Appender`.
	 *
	 * If the `Appender` is empty then an assertion is raised.
	 */
	@safe
	public ref Type tail() pure nothrow {
		assert(this.count_);

		return this.buffer[this.count_ - 1];
	}

	/**
	 * Attempts to free any dynamic memory allocated to the `Appender` and clears all values.
	 *
	 * If `Type` has a non-trivial destructor then it is invoked for each value in the `Appender`.
	 *
	 * For only clearing the `Appender` see `Appender.clear` instead.
	 */
	public void release() nothrow {
		if (this.buffer) {
			static if (hasDestructor!(Type)) {
				foreach (ref element; this.all()) {
					element.__xdtor();
				}
			}

			dealloc(this.allocator, this.buffer);

			this.count_ = 0;
		}
	}

	/**
	 * Grows the capacity of the `Appender` by `capacity` elements, returning `true` if it was
	 * successful, otherwise `false` if not.
	 *
	 * A `capacity` value of `0` will re-allocate the existing buffer with no changes to its
	 * capacity.
	 *
	 * A call to `Appender.reserve` will fail if a the requested `capacity` cannot be allocated. If
	 * this occurs it is worth checking that the `Appender` is not now empty, as the failing
	 * behavior of `ona.memory.realloc` may vary between `ona.memory.Allocator` implementations.
	 */
	public bool reserve(size_t capacity) nothrow {
		this.buffer = realloc(this.allocator, this.buffer, (this.buffer.length + capacity));

		return (this.buffer.ptr != null);
	}

	/**
	 * Removes `n` elements from the list without altering the current capacity of the `Appender`.
	 *
	 * If `Type` has a non-trivial destructor then it is invoked for each value targetted for
	 * removal in `Appender`.
	 *
	 * If `n` is greater than `Appender.count` an assertion is raised.
	 */
	public void truncate(size_t n) nothrow {
		assert(n <= this.count_);

		static if (hasDestructor!(Type)) {
			foreach (ref element; this.slice((this.count_ - n), this.count_)) {
				element.__xdtor();
			}
		}

		this.count_ -= n;
	}
}

/**
 * Creates a new `Appender` containing `elements` using memory allocated with `allocator`.
 *
 * As it is possible that `Allocator`s may fail to allocate memory, it is important that the
 * returned `Appender.count` be checked that it is the expected value given the `elements` input.
 */
public Appender!(Type) appenderOf(Type)(const (Allocator)* allocator, Type[] elements...) nothrow {
	Appender!(Type) appender = allocator;

	appender.appendAll(elements);

	return appender;
}
