module ona.collections.hashed;

private {
	import ona.algorithm;
	import ona.memory;
}

/**
 * A data structure of `KeyType`-`ValueType` associated elements. Values are stored and queried
 * using their associated keys.
 *
 * If `ValueType` is `void` then a variant of `Table` is created that is specialized around a
 * unique set of `KeyType` values.
 *
 * A unique hashing function is required in the field `Hasher` for generating semi-unique hash
 * values for `KeyType` values.
 */
public struct Table(KeyType, ValueType, alias Hasher) {
	private struct Bucket {
		KeyType key;

		static if (!keysOnly) {
			ValueType value;
		}

		Bucket* next;
	}

	/**
	 * The capacity value used the first time `Appender.insert` is called on an initialized `Table`
	 * with no allocated capacity
	 */
	public enum defaultCapacity = 128;

	/**
	 * Indicates if the `Table` can only store keys.
	 */
	public enum keysOnly = is(ValueType == void);

	private alias Defaulter = ValueType delegate() nothrow;

	/**
	 * Pointer to the current allocation strategy.
	 *
	 * Changing this value while memory is allocated to the `Table` will cause undefined behavior.
	 */
	private const (Allocator)* allocator;

	static if (keysOnly) {
		/**
		 * If assigned a non-`null` value, `Table.bucketDestructor` is called every time a key-
		 * value pair is replaced or removed from the `Table`.
		 */
		public void delegate(ref KeyType, const (Allocator)*) nothrow bucketDestructor;
	} else {
		/**
		 * If assigned a non-`null` value, `Table.bucketDestructor` is called every time a key
		 * is replaced or removed from the `Table`.
		 */
		public void delegate(ref KeyType, ref ValueType, const (Allocator)*) nothrow bucketDestructor;
	}

	/**
	 * Functions as the first-layer of the querying process for finding a value stored within the
	 * `Table`. From this root, buckets with reduced hashes that collide are linked together for
	 * further querying.
	 */
	public Bucket*[] index;

	/**
	 * Linked list of free buckets awaiting re-use or freeing.
	 *
	 * Allocation may be expensive, so recycling previously used buckets ensures a lower overhead
	 * when inserting new elements.
	 */
	public Bucket* freeBuckets;

	/**
	 * Number of currently stored elements.
	 */
	public size_t count;

	/**
	 * The maximum load factor that a `Table` may reach before requesting an increase in capacity.
	 *
	 * A maximum load of `0.0` means that the `Table` will never make any attempt to increase in
	 * capacity past its already existing capacity. A maximum load of `0.0` will not prevent new
	 * values from being inserted, but it will mean that values will become heavily nested and will
	 * be slower to lookup than values which would have a larger capacity.
	 */
	public float loadMaximum = 0.0f;

	/**
	 * Constructs with the allocation strategy `allocator`.
	 */
	public this(inout (Allocator)* allocator) nothrow {
		this.allocator = allocator;
	}

	/**
	 * Returns an anonymous range to iterate over all key-value pairs in the `Table`.
	 */
	public Enumerator!(KeyType, ValueType) all() nothrow {
		size_t indexPointer;

		return Enumerator!(KeyType, ValueType)((i, produce) {
			if (indexPointer < this.index.length) {
				Bucket* bucket = this.index[indexPointer];

				while (bucket) {
					scope (exit) bucket = bucket.next;

					return produce(bucket.key, bucket.value);
				}

				indexPointer += 1;
			}
		});
	}

	/**
	 * Returns the `ona.memory.Allocator` used by the `Table`.
	 *
	 * If no `ona.memory.Allocator` has been assigned yet then the returned value is `null`.
	 */
	public const (Allocator)* allocatorOf() nothrow {
		return this.allocator;
	}

	/**
	 * Calculates the load factor of the `Table` using the formula `l = (n / k)`.
	 *
	 * The load factor represents the overall occupancy of the `Table` as a value between `0` and
	 * `1`.
	 */
	public float calculateLoadFactor() nothrow {
		return (this.count / this.index.length);
	}

	/**
	 * Clears all elements from the `Table`, moving all existing `Bucket` instances to
	 * `Table.freeBuckets` linked list for future re-use.
	 *
	 * To forcibly destroy all allocated memory belonging to the `Table`, use `Table.release`.
	 */
	public void clear() nothrow {
		Bucket** freeBucketsTail = &this.freeBuckets;

		if (this.bucketDestructor) {
			// Clear with destructors implementation.
			foreach (ref bucket; this.index) {
				if (bucket) {
					*freeBucketsTail = bucket;

					// Call the destructor for the index-level bucket.
					static if (keysOnly) {
						this.bucketDestructor(bucket.key, this.allocator);
					} else {
						this.bucketDestructor(bucket.key, bucket.value, this.allocator);
					}

					// Walk to the end of the linked bucket list to find out where the new tail is
					// and call the destructor of each bucket along the way.
					while (bucket.next) {
						bucket = bucket.next;

						// It is known that the bucket is not null because it is checked in the
						// loop condition.
						static if (keysOnly) {
							this.bucketDestructor(bucket.key);
						} else {
							this.bucketDestructor(bucket.key, bucket.value, this.allocator);
						}
					}

					freeBucketsTail = &bucket.next;
					bucket = null;
				}
			}
		} else {
			// No destructor clear implementation variant.
			foreach (ref bucket; this.index) {
				if (bucket) {
					*freeBucketsTail = bucket;

					// Walk to the end of the linked bucket list to find out where the new tail is and
					// call the destructor of each bucket along the way.
					while (bucket.next) {
						bucket = bucket.next;
					}

					freeBucketsTail = &bucket.next;
					bucket = null;
				}
			}
		}

		this.count = 0;
	}

	static if (keysOnly) {
		/**
		 * Inserts `key` into the `Table`, returning `true` if it was sucessful.
		 *
		 * If the `Table` does not already have an allocated capacity then an attempt to allocate
		 * one of size `Table.defaultCapacity` will be made.
		 *
		 * If an insertion will excede the load factor specified in `Table.loadMaximum` then an
		 * attempt to increase the `Table` capacity will be made. Setting `Table.loadMaximum` to
		 * `0` will disable this behavior from happening.
		 *
		 * `Table` will prefer to use buckets that are already allocated but unused in
		 * `Table.freeBuckets`. However, if no buckets are available in this linked list then
		 * `Table.insert` will attempt to allocate one.
		 *
		 * `Table.insert` will return `false` if it doesn't have an `ona.sys.Allocator` assigned to
		 * it or any of the previously mentioned allocations fail.
		 */
		public bool insert(KeyType key) nothrow {
			return this.insert(key);
		}

		/**
		 * Inserts `key` into the `Table`, returning `true` if it was sucessful.
		 *
		 * If the `Table` does not already have an allocated capacity then an attempt to allocate
		 * one of size `Table.defaultCapacity` will be made.
		 *
		 * If an insertion will excede the load factor specified in `Table.loadMaximum` then an
		 * attempt to increase the `Table` capacity will be made. Setting `Table.loadMaximum` to
		 * `0` will disable this behavior from happening.
		 *
		 * `Table` will prefer to use buckets that are already allocated but unused in
		 * `Table.freeBuckets`. However, if no buckets are available in this linked list then
		 * `Table.insert` will attempt to allocate one.
		 *
		 * `Table.insert` will return `false` if it doesn't have an `ona.sys.Allocator` assigned to
		 * it or any of the previously mentioned allocations fail.
		 */
		public bool insert(ref KeyType key) nothrow {
			Bucket* makeBucket() nothrow {
				if (this.freeBuckets) {
					Bucket* bucket = this.freeBuckets;
					this.freeBuckets = this.freeBuckets.next;
					*bucket = Bucket(key);

					return bucket;
				}

				if (this.allocator) {
					Bucket* bucket =  alloc!(Bucket)(this.allocator);

					if (bucket) {
						(*bucket) = Bucket(key);
					}

					return bucket;
				}

				return null;
			}

			if (this.allocator) {
				Bucket** rootBucket = void;
				Bucket* currentBucket = void;

				if (!this.index) {
					// No table exists yet, create a default table.
					this.index = alloc!(Bucket*)(this.allocator, defaultCapacity);

					if (!this.index) {
						return false;
					}
				} else if (this.loadMaximum && (this.calculateLoadFactor() >= this.loadMaximum)) {
					// Current load is at a load factor greater than the allowed maximum, double
					// the existing table size.
					if (!this.reserve(this.index.length)) {
						return false;
					}
				}

				// Compute the reduced bucket index hash.
				rootBucket = (this.index.ptr + (Hasher(key) % this.index.length));
				currentBucket = *rootBucket;

				if (currentBucket) {
					// Walk through the linked list until the target bucket is found.
					while (currentBucket.next) {
						if (currentBucket.key == key) {
							// No new bucket needs making, just overwrite the existing one.
							if (this.bucketDestructor) {
								this.bucketDestructor(currentBucket.key);
							}

							currentBucket.key = key;

							return true;
						}

						currentBucket = currentBucket.next;
					}

					this.count += 1;

					return ((currentBucket.next = makeBucket()) != null);
				}

				this.count += 1;

				return (((*rootBucket) = makeBucket()) != null);
			}

			return false;
		}
	} else {
		/**
		 * Inserts the respective key-value pair `key` and `value` into the `Table`, returning a
		 * pointer to the inserted `value` if successful.
		 *
		 * If the `Table` does not already have an allocated capacity then an attempt to allocate
		 * one of size `Table.defaultCapacity` will be made.
		 *
		 * If an insertion will excede the load factor specified in `Table.loadMaximum` then an
		 * attempt to increase the `Table` capacity will be made. Setting `Table.loadMaximum` to
		 * `0` will disable this behavior from happening.
		 *
		 * `Table` will prefer to use buckets that are already allocated but unused in
		 * `Table.freeBuckets`. However, if no buckets are available in this linked list then
		 * `Table.insert` will attempt to allocate one.
		 *
		 * `Table.insert` will return `null` if it doesn't have an `ona.sys.Allocator` assigned to
		 * it or any of the previously mentioned allocations fail.
		 */
		public ValueType* insert(KT, VT)(auto ref KT key, auto ref VT value) nothrow
				if (is(KT == KeyType) && is(VT == ValueType)) {

			Bucket* makeBucket() nothrow {
				if (this.freeBuckets) {
					Bucket* bucket = this.freeBuckets;
					this.freeBuckets = this.freeBuckets.next;
					*bucket = Bucket(key, value);

					return bucket;
				}

				if (this.allocator) {
					Bucket* bucket =  alloc!(Bucket)(this.allocator);

					if (bucket) {
						(*bucket) = Bucket(key, value);
					}

					return bucket;
				}

				return null;
			}

			if (this.allocator) {
				Bucket** rootBucket = void;
				Bucket* currentBucket = void;

				if (!this.index) {
					// No table exists yet, create a default table.
					this.index = alloc!(Bucket*)(this.allocator, defaultCapacity);

					if (!this.index) {
						return null;
					}
				} else if (this.loadMaximum && (this.calculateLoadFactor() >= this.loadMaximum)) {
					// Current load is at a load factor greater than the allowed maximum, double
					// the existing table size.
					if (!this.reserve(this.index.length)) {
						return null;
					}
				}

				// Compute the reduced bucket index hash.
				rootBucket = (this.index.ptr + (Hasher(key) % this.index.length));
				currentBucket = *rootBucket;

				if (currentBucket) {
					// Walk through the linked list until the target bucket is found.
					while (currentBucket.next) {
						if (currentBucket.key == key) {
							// No new bucket needs making, just overwrite the existing one.
							if (this.bucketDestructor) {
								this.bucketDestructor(
										currentBucket.key, currentBucket.value, this.allocator);
							}

							currentBucket.key = key;
							currentBucket.value = value;

							return &currentBucket.value;
						}

						currentBucket = currentBucket.next;
					}

					this.count += 1;

					return &(currentBucket.next = makeBucket()).value;
				}

				this.count += 1;

				return &((*rootBucket) = makeBucket()).value;
			}

			return null;
		}
	}

	static if (keysOnly) {
		/**
		 * Queries the `Table` for `key`, returning `true` if it is found, otherwise `false`.
		 */
		public bool lookup(inout (KeyType) key) nothrow {
			return this.lookup(key);
		}

		/**
		 * Queries the `Table` for `key`, returning `true` if it is found, otherwise `false`.
		 */
		public bool lookup(ref inout (KeyType) key) nothrow {
			if (this.index) {
				// Compute the reduced bucket index hash.
				Bucket* bucketQuery = this.index[Hasher(key) % this.index.length];

				// Walk through the linked list until the target bucket is found.
				while (bucketQuery && (bucketQuery.key != key)) {
					bucketQuery = bucketQuery.next;
				}

				return (bucketQuery != null);
			}

			return false;
		}
	} else {
		/**
		 * Queries the `Table` for `key`, returning the element associated with it, or `null` if
		 * nothing was found.
		 */
		public ValueType* lookup(inout (KeyType) key) nothrow {
			return this.lookup(key);
		}

		/**
		 * Queries the `Table` for `key`, returning the element associated with it, or `null` if
		 * nothing was found.
		 */
		public ValueType* lookup(ref inout (KeyType) key) nothrow {
			if (this.index) {
				// Compute the reduced bucket index hash.
				Bucket* bucketQuery = this.index[Hasher(key) % this.index.length];

				// Walk through the linked list until the target bucket is found.
				while (bucketQuery && (bucketQuery.key != key)) {
					bucketQuery = bucketQuery.next;
				}

				return (bucketQuery ? &bucketQuery.value : null);
			}

			return null;
		}

		/**
		 * Attempts a lookup using `key`, returning the value associated with it if one exists,
		 * otherwise inserting whatever the result of `defaulter` evaluates to and returning its
		 * inserted value instead.
		 *
		 * If any allocation fails along the way when inserting the value of `defaulter` then
		 * `null` is returned.
		 *
		 * If `defaulter` is `null` then `ValueType.init` becomes the default insertion value.
		 */
		public ValueType* lookupOrInsert(KeyType key, Defaulter defaulter) nothrow {
			return this.lookupOrInsert(key, defaulter);
		}

		/**
		 * Attempts a lookup using `key`, returning the value associated with it if one exists,
		 * otherwise inserting whatever the result of `defaulter` evaluates to and returning its
		 * inserted value instead.
		 *
		 * If any allocation fails along the way when inserting the value of `defaulter` then
		 * `null` is returned.
		 *
		 * If `defaulter` is `null` an assertion is raised.
		 */
		public ValueType* lookupOrInsert(ref KeyType key, Defaulter defaulter) nothrow {
			ValueType* value = this.lookup(key);

			return (value ? value : this.insert(key, (defaulter ? defaulter() : ValueType.init)));
		}
	}

	/**
	 * Attempts to free any dynamic resources allocated to the `Table` if it has an
	 * `ona.memory.Allocator`.
	 *
	 * As a side-effect, `Table.release` clears the elements in the `Table`.
	 */
	public void release() nothrow {
		if (this.allocator) {
			// Destroy any buckets in the lookup table.
			Bucket* nextBucket = void;

			if (this.bucketDestructor) {
				// With destructor.
				foreach (bucket; this.index) {
					while (bucket) {
						nextBucket = bucket.next;

						this.bucketDestructor(bucket.key, bucket.value, this.allocator);
						dealloc(this.allocator, bucket);

						bucket = nextBucket;
					}
				}
			} else {
				// Without destructor.
				foreach (bucket; this.index) {
					while (bucket) {
						nextBucket = bucket.next;

						dealloc(this.allocator, bucket);

						bucket = nextBucket;
					}
				}
			}

			// Destroy free buckets linked list.
			while (this.freeBuckets) {
				nextBucket = this.freeBuckets.next;

				dealloc(this.allocator, this.freeBuckets);

				this.freeBuckets = nextBucket;
			}

			this.count = 0;
		}
	}

	/**
	 * Removes the value associated with the key `key`, returning `true` if it was removed or
	 * `false` if no such element existed.
	 */
	public bool remove(KeyType key) nothrow {
		return this.remove(key);
	}

	/**
	 * Removes the value associated with the key `key`, returning `true` if it was removed or
	 * `false` if no such element existed.
	 */
	public bool remove(ref KeyType key) nothrow {
		if (this.index) {
			// Compute the reduced bucket index hash.
			Bucket** bucketIndex = (this.index.ptr + Hasher(key) % this.index.length);
			Bucket* bucketQuery = *bucketIndex;

			if (bucketQuery) {
				Bucket* bucketPrev;

				// Walk through the linked list until the target bucket is found.
				while (bucketQuery && (bucketQuery.key != key)) {
					bucketPrev = bucketQuery;
					bucketQuery = bucketQuery.next;
				}

				if (bucketQuery) {
					// Bucket removal and reference updating.
					if (bucketPrev) {
						// A previous bucket means that there was linked list traversal, so the
						// next node of the previous bucket must be updated.
						bucketPrev.next = bucketQuery.next;
					} else {
						// No previous bucket means that the bucket was at the root table-level, so
						// the table address index itself must be reset.
						*bucketIndex = null;
					}

					static if (keysOnly) {
						this.bucketDestructor(bucketQuery.key, this.allocator);
					} else {
						this.bucketDestructor(bucketQuery.key, bucketQuery.value, this.allocator);
					}

					// Append the removed bucket to the head of the free buckets linked list.
					bucketQuery.next = this.freeBuckets;
					this.freeBuckets = bucketQuery;
					this.count -= 1;

					return true;
				}
			}
		}

		return false;
	}

	/**
	 * Grows the capacity of the `Table` by `capacity`, returning `true` if it was successful,
	 * otherwise `false` if not.
	 *
	 * A capacity of `0` will re-allocate the existing bufer with no changes to its capacity.
	 *
	 * A call to reserve will fail under the following circumstances:
	 *
	 *   - The `Table` does not have an `ona.memory.Allocator` assigned to it.
	 *
	 *   - The assigned `ona.memory.Allocator` failed to re-allocate for the requested capacity. If
	 *     this occurs it is worth checking that `Table.index` is not `null`, as the failing return
	 *     value of `ona.memory.realloc` may vary between `ona.memory.Allocator` implementations.
	 */
	public bool reserve(size_t capacity) nothrow {
		immutable (size_t) oldLength = this.index.length;
		this.index = realloc!(Bucket*)(this.allocator, this.index, (this.index.length + capacity));

		if (this.index && (this.index.length != oldLength)) {
			zeroMemory(this.index[oldLength .. this.index.length]);

			return true;
		}

		return false;
	}
}
