module ona.serial;

/**
 * Attempts to parse the text in `str` as a base-10 formatted number of type `Type`.
 *
 * A decimal string may contain the number expression beginning with a value between `1` and `9`
 * with no trailing, non-digit characters on the beginning or end.
 *
 * Values being parsed into a floating-point `Type` may contain a single decimal place in any
 * position except as the last digit of the numeric string. Otherwise, values being parsed into an
 * integral `Type` cannot contain any decimal places.
 *
 * Values being parsed into an unsigned `Type` cannot be negative.
 *
 * Regardless of the `Type` specified, `str` cannot be empty / `null`.
 *
 * See `ParseError` for more information on error handling the `ona.error.Result`.
 */
@safe
public ParseResult!(Type) parseDecimal(Type)(string str) pure nothrow {
	if (str.length) {
		Type result;

		static if (__traits(isIntegral, Type)) {
			enum isUnsignedType = __traits(isUnsigned, Type);

			static if (isUnsignedType) {
				// Unsigned types cannot be negative.
				if (str[0] == '-') return ParseResult!(Type).badFormat;
			} else {
				// Ignore the sign in the string for now, we'll come back to it tho.
				bool isNegative;

				if (str[0] == '-') {
					str = str[1 .. $];
					isNegative = true;
				}
			}

			// Length may have changed if a sign was removed.
			if (str.length && (str[0] > '0') && (str[0] < ':')) {
				char c = void;

				foreach (i; 0 .. str.length) {
					c = str[str.length - (i + 1)];

					if (!isDigit(c)) return ParseResult!(Type).badFormat;

					result += cast(Type)((c - '0') * pow(10, i));
				}

				static if (isUnsignedType) {
					if (isNegative) result *= -1;
				}
			}
		} else static if (__traits(isFloating, Type)) {
			Type fact = 1;
			int d = void;
			bool hasDecimal;

			if (str[0] == '-') {
				str = str[1 .. $];
				fact = -1;
			}

			// Length may have changed if a sign was removed.
			if (str.length) {
				if (str[0] == '.') {
					hasDecimal = true;
				} else if (str[0] && (str.length > 1) && (str[1] == '0')) {
					// A floating point value cannot begin with sequential zeroes.
					return ParseResult!(Type).badFormat;
				}

				foreach (c; str) {
					if (c == '.') {
						if (hasDecimal) {
							// Multiple decimal places are not valid.
							return ParseResult!(Type).badFormat;
						} else {
							hasDecimal = true;
						}
					} else if (isDigit(c)) {
						d = (c - '0');

						if ((d >= 0) && (d <= 9)) {
							if (hasDecimal) {
								fact /= 10.0f;
							}

							result = ((result * 10.0f) + cast(float)d);
						}
					}
				}
			}

			result *= fact;
		}

		return ParseResult!(Type).ok(result);
	}

	return ParseResult!(Type).badFormat;
}
