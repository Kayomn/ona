module ona.math;

private {
	import core.stdc.math;
}

/**
 * `n`-dimensional matrix represented as a `2`-dimensional array.
 */
public struct Matrix {
	/**
	 * Number of dimensions in the matrix.
	 */
	enum length = 4;

	/**
	 * `Matrix` row type.
	 */
	alias RowType = Vector!(float, length);

	/**
	 * Matrix with zero-initialized elements save for the main diagonal, which contains matrix type
	 * values equivalent to `1`.
	 */
	static immutable (Matrix) identity = () {
		Matrix mat = of(0.0f);

		static foreach (i; 0 .. length) {
			mat[i, i] = 1.0f;
		}

		return mat;
	}();

	/**
	 * Memory is aligned in a single block for performance and leaves open the option of utilizing
	 * vectorizable operations.
	 */
	float[length * length] elements;

	/**
	 * Initializes a `Mat` with `value` as the value of all elements.
	 */
	@safe
	static inout (Matrix) of(inout (float) value) pure nothrow {
		Matrix mat;
		mat.elements = value;

		return mat;
	}

	/**
	 * Initializes all specified elements to the values in `values`.
	 */
	@safe
	this(typeof(elements) values...) pure nothrow {
		this.elements = values;
	}

	/**
	 * Performs a pure binary arithmetic operation between `this` and `that`.
	 */
	@safe
	inout (Matrix) opBinary(immutable (char)[] op)(
				inout (Matrix) that) inout pure nothrow if (op == "*") {

		Matrix result;

		for (size_t i; i < length; i += 1) {
			for (size_t j; j < length; j += 1) {
				float sum = 0;

				for (size_t k; k < length; k += 1) {
					sum += (this[i, k] * that[k, j]);
				}

				result[i, j] = sum;
			}
		}

		return result;
	}

	/**
	 * Performs a pure binary arithmetic operation between `this` and `n`-component vector `that`.
	 */
	@safe
	inout (RowType) opBinary(immutable (char)[] op)(
				inout (RowType) that) inout pure nothrow if (op == "*") {

		immutable (float[]) m0 = this[0];
		immutable (float[]) m1 = this[1];
		immutable (float[]) m2 = this[2];
		immutable (float[]) m3 = this[3];

		return RowType(
			((m0[0] * that.x) + (m0[1] * that.y) + (m0[2] * that.z) + (m0[3] * that.w)),
			((m1[0] * that.x) + (m1[1] * that.y) + (m1[2] * that.z) + (m1[3] * that.w)),
			((m2[0] * that.x) + (m2[1] * that.y) + (m2[2] * that.z) + (m2[3] * that.w)),
			((m3[0] * that.x) + (m3[1] * that.y) + (m3[2] * that.z) + (m3[3] * that.w))
		);
	}

	/**
	 * Returns the row at index `row` as a `Vec` equal in length to the number of dimensions
	 * represented by the `Mat` by reference.
	 */
	ref inout (RowType) opIndex(size_t row) inout pure nothrow {
		// Memory hack to reinterpret the slice of memory being accessed as a Float4.
		return *(cast(inout (RowType)*)(&this.elements[row * length]));
	}

	/**
	 * Returns the element value at row `row` and column `column` indices by reference.
	 */
	@safe
	ref inout (float) opIndex(size_t row, size_t column) inout pure nothrow {
		return this.elements[column + (row * length)];
	}

	/**
	 * Performs a binary arithmetic assignment operation between `this` and `that`.
	 */
	@safe
	ref Matrix opOpAssign(immutable (char)[] op)(Matrix that) pure nothrow if (op[0] == '*') {
		for (size_t i; i < length; i++) {
			for (size_t j; j < length; j++) {
				float sum = 0;

				for (size_t k; k < length; k++) {
					sum += (this[i, k] * that[k, j]);
				}

				this[i, j] = sum;
			}
		}

		return this;
	}
}

/**
 * `n`-component vector capable of being used as a static array or slice.
 */
public struct Vector(Type, size_t n) {
	alias values this;

	/**
	 * Number of components in the vector.
	 */
	enum length = n;

	/**
	 * Characters representing the `Vector` type component symbols.
	 */
	static immutable (char[]) components = ['x', 'y', 'z', 'w'][0 .. n];

	union {
		struct {
			static foreach (c; components) {
				// Generates the components at compile-time based on the number of components
				// specified by the n template property.
				mixin("Type " ~ c ~ "= cast(Type)0;");
			}
		}

		/**
		 * Vector values as an array.
		 */
		Type[n] values;
	}

	/**
	 * Initializes a `Vector` with `value` as the value of all components.
	 */
	@safe
	static inout (Vector) of(inout (Type) value) pure nothrow {
		Vector vec;

		static foreach (c; components) {
			mixin("vec." ~ c ~ " = cast(Type)value;");
		}

		return vec;
	}

	/**
	 * Assigns all components to `value`.
	 */
	@safe
	ref Vector opAssign(Type value) pure nothrow {
		static foreach (c; components) {
			mixin("this." ~ c ~ " = value;");
		}

		return this;
	}

	/**
	 * Assigns all components supplied by `that` to its values.
	 */
	@safe
	ref Vector opAssign(size_t s)(Vector!(Type, s) that) pure nothrow {
		import ona.algorithm : min;

		static foreach (i, c; components[0 .. min(s, n)]) {
			mixin("this." ~ c ~ " = that." ~ c ~ ";");
		}

		return this;
	}

	/**
	 * Performs a pure binary arithmetic operation between `this` and `that`.
	 */
	@safe
	Vector opBinary(immutable (char)[] op, size_t s)(
				inout (Vector!(Type, s)) that) inout pure nothrow {

		Vector result = this;

		static foreach (c; components[0 .. min(s, n)]) {
			mixin("result." ~ c ~ ' ' ~ op ~ "= that." ~ c ~ ';');
		}

		return result;
	}

	/**
	 * Performs a binary arithmetic operation between `this` and `value`.
	 */
	@safe
	Vector opBinary(immutable (char)[] op)(inout (Type) value) inout pure nothrow {
		Vector result = this;

		static foreach (c; components) {
			mixin("result." ~ c ~ ' ' ~ op ~ "= value;");
		}

		return result;
	}

	/**
	 * Performs a unary arithmetic operation.
	 */
	@safe
	Vector opUnary(immutable (char)[] op)() pure nothrow {
		Vector result = this;

		static foreach (c; components) {
			mixin("result." ~ c ~ " = " ~ op ~ "result." ~ c ~ ';');
		}

		return result;
	}

	/**
	 * OpenGL-like vector swizzling pseudo member function.
	 */
	@safe
	Vector!(Type, op.length) opDispatch(immutable (char)[] op)() inout pure nothrow {
		Vector!(Type, op.length) result;

		static foreach (i, c; op) {
			mixin("result[i] = this. " ~ c ~ ';');
		}

		return result;
	}
}

/**
 * Two-component structure of `int`s.
 */
public alias Point2 = Vector!(int, 2);

/**
 * Three-component structure of `int`s.
 */
public alias Point3 = Vector!(int, 3);

/**
 * Four-component structure of `int`s.
 */
public alias Point4 = Vector!(int, 4);

/**
 * Two-component structure of `float`s for use with vector math.
 */
public alias Vector2 = Vector!(float, 2);

/**
 * Three-component structure of `float`s for use with vector math.
 */
public alias Vector3 = Vector!(float, 3);

/**
 * Four-component structure of `float`s for use with vector math.
 */
public alias Vector4 = Vector!(float, 4);

/**
 * Returns the absolute value of `value`.
 *
 * Absolute value is defined as the non-negative value of `value` without regard to its sign.
 */
public Type abs(Type)(
			Type value) if (__traits(isArithmetic, Type) && !__traits(isUnsigned, Type)) {

	return (value < 0 ? value * -1 : value);
}

/**
 * Returns `true` if `value` is identifiable as infinite according to the IEEE floating point
 * standard.
 *
 * https://www.doc.ic.ac.uk/~eedwards/compsys/float/nan.html
 */
public bool isInf(real value) nothrow {
	return (isinf(value) != 0);
}

/**
 * Returns `true` if `value` is identifiable as NaN, or "not a number", according to the IEEE
 * floating point standard.
 *
 * https://www.doc.ic.ac.uk/~eedwards/compsys/float/nan.html
 */
public bool isNaN(real value) nothrow {
	return (isnan(value) != 0);
}

/**
 * Returns a 3D orthographic projection matrix.
 */
@safe
public Matrix orthographicMatrix3D(
			float left, float right, float bottom, float top, float near, float far) pure nothrow {

	Matrix result = Matrix.identity;
	result.elements[0 + 0 * 4] = 2.0f / (right - left);
	result.elements[1 + 1 * 4] = 2.0f / (top - bottom);
	result.elements[2 + 2 * 4] = 2.0f / (near - far);
	result.elements[3 + 0 * 4] = (left + right) / (left - right);
	result.elements[3 + 1 * 4] = (bottom + top) / (bottom - top);
	result.elements[3 + 2 * 4] = (far + near) / (far - near);

	return result;
}

/**
 * Returns the power of `value` by `exponent`.
 */
@safe
public double pow(double value, long exponent) pure nothrow {
	if (exponent) {
		immutable (double) temp = pow(value, (exponent / 2));

		return (exponent % 2) ? (value * temp * temp) : (temp * temp);
	}

	return 1.0;
}

/**
 * Returns a 3D rotation matrix for the X axis.
 */
@safe
public Matrix rotationMatrixX(float rotation) pure nothrow {
	return Matrix(
		1f, 0f, 0f, 0f,
		0f, cos(rotation), -sin(rotation), 0f,
		0f, sin(rotation), cos(rotation), 0f,
		0f, 0f, 0f, 1f
	);
}


/**
 * Returns a 3D rotation matrix for the Y axis.
 */
@safe
public Matrix rotationMatrixY(float rotation) pure {
	return Matrix(
		cos(rotation), 0f, sin(rotation), 0f,
		0f, 1f, 0f, 0f,
		-sin(rotation), 0f, cos(rotation), 0f,
		0f, 0f, 0f, 1f
	);
}

/**
 * Returns a 3D rotation matrix for the Z axis.
 */
@safe
public Matrix rotationMatrixZ(float rotation) pure {
	return Matrix(
		cos(rotation), -sin(rotation), 0f, 0f,
		sin(rotation), cos(rotation), 0f, 0f,
		0f, 0f, 1f, 0f,
		0f, 0f, 0f, 1f
	);
}

/**
 * Returns a 3D scale matrix.
 */
@safe
public Matrix scaleMatrix(float x, float y, float z) pure {
	return Matrix([
		x, 0f, 0f, 0f,
		0f, y, 0f, 0f,
		0f, 0f, z, 0f,
		0f, 0f, 0f, 1f
	]);
}

/**
 * Returns a 3D translation matrix.
 */
@safe
public Matrix translationMatrix(float x, float y, float z) pure {
	return Matrix([
		1, 0, 0, x,
		0, 1, 0, y,
		0, 0, 1, z,
		0, 0, 0, 1
	]);
}
