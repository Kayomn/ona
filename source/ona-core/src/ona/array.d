module ona.array;

private {
	import ona.memory;
}

/**
 * A wrapped array of `Type` that is bundled with an `ona.memory.Allocator` reference, giving the
 * subject array additional memory safety and lifetime control at the cost of a larger memory
 * footprint.
 */
public struct Array(Type) {
	alias elements this;

	private const (Allocator)* allocator;

	/**
	 * Wrapped array.
	 */
	public Type[] elements;

	/**
	 * Constructs with the allocation strategy `allocator`.
	 *
	 * If `null` is supplied then the `Array` will use the default runtime allocation strategy.
	 * Alternatively, if no specialized `ona.memory.Allocator` is needed then the default
	 * constructor will work as well.
	 */
	@safe
	public this(const (Allocator)* allocator) pure nothrow {
		this.allocator = allocator;
	}

	/**
	 * Returns the `ona.memory.Allocator` used by the `Array`.
	 *
	 * If `null` is returned then the `Array` is using the default runtime allocation strategy.
	 */
	@safe
	public const (Allocator)* allocatorOf() pure const nothrow {
		return this.allocator;
	}

	/**
	 * Attempts to create a new `Array` of `length` number of elements using `allocator` to
	 * allocate the requested memory for it. A `null` `allocator` will default to using the runtime
	 * default `Allocator`.
	 *
	 * For memory safety, all elements in the returned `Array` are default-initialized to whatever
	 * the value of `Type.init` is.
	 *
	 * As it is possible for `Allocator`s to fail to allocate memory, the returned `Array` should
	 * be checked that it is the expected size, and not `null`.
	 */
	public static Array make(const (Allocator)* allocator, size_t length) {
		Array!(Type) array = allocator;
		array.elements = alloc!(Type)(allocator, length);

		if (array.elements) {
			static if (__traits(isZeroInit, Type)) {
				zeroMemory(array.elements);
			} else {
				writeMemory(array.elements, Type.init);
			}
		}

		return array;
	}

	/**
	 * Attempts to create a new `Array` containing `elements` using `allocator` to allocate the
	 * requested memory for it. A `null` `allocator` will default to using the runtime default
	 * `Allocator`.
	 *
	 * As it is possible for `Allocator`s to fail to allocate memory, the returned `Array` should be
	 * checked that it is the expected size, and not `null`.
	 */
	public static Array!(Type) of(
			const (Allocator)* allocator,
			scope inout (Type)[] elements...
		) nothrow {

		Array!(Type) array = allocator;
		array.elements = alloc!(Type)(allocator, elements.length);

		cast(void)copyMemory(array.elements, elements);

		return array;
	}

	/**
	 * Attempts to free any dynamic resources allocated to the `Array`.
	 *
	 * If `Type` has a non-trivial destructor then it is invoked for each value in the `Array`.
	 */
	public void release() nothrow {
		if (this.elements) {
			static if (hasDestructor!(Type)) {
				foreach (ref element; this.elements) {
					element.__xdtor();
				}
			}

			dealloc(this.allocator, this.elements);
		}
	}
}

/**
 * Wraps `value` in a slice of `Type`.
 *
 * The returned slice references `value` directly. Therefore, changes to `value` will effect the
 * returned slice and vice versa.
 *
 * Additionally, the validity of the returned slice memory is managed by the lifetime of `value`;
 * `value` being deleted in memory will make the slice point to invalid memory.
 */
public scope Type[] sliceWrap(Type)(auto ref Type value) pure nothrow {
	return (&value)[0 .. 1];
}
