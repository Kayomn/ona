module ona.string;

private {
	import ona.algorithm;
	import ona.array;
	import ona.memory;
}

/**
 * Managed data type for dynamically allocated sequences of text.
 *
 * `String` is designed to avoid dynamic allocation where possible, through the use of small-string
 * optimization (SSO), however when necessary it will without informing the callsite.
 *
 * `String`s are "visibly immutable" meaning that their contents cannot be changed, only assigned
 * to new contents. Because of this, a `String` instance may not be mutated.
 *
 * For the sake of memory safety, all character sequences passed to `String` are always copied so
 * that the runtime may take ownership of the memory.
 *
 * Unlike string literals and many C-like APIs, `String`s are not null terminated. For converting
 * a `String`, or any character sequence, to a C-compatible character sequence see
 * `sentineledSequence`.
 */
public struct String {
	static assert(
		(Data.sizeof == 24),
		"ona.string.String.Data violates its size contract"
	);

	private align(1) struct AllocatedString {
		char[] buffer;

		size_t* refCounter;
	}

	private align(1) struct StackedString {
		char[stackableMax] stack;

		ubyte length;
	}

	private align(1) union Data {
		AllocatedString allocated;

		StackedString stacked;
	}

	private enum Type : ubyte {
		empty,
		allocated,
		stacked
	}

	private enum invalidAccessMessage = "Attempt to access invalid ona.string.String index";

	/**
	 * The maximum number of 8-byte values that can be stored in a `String` before dynamic
	 * allocation is necessary.
	 */
	public enum stackableMax = 22;

	private Data data;

	private Type type;

	/**
	 * Constructs a single-character `String` from `c`.
	 *
	 * As the character can be stored in auxillary stack-space no allocation is needed.
	 */
	public this(char c) pure nothrow {
		this.data.stacked.stack[0] = c;
		this.data.stacked.length = 1;
		this.type = Type.stacked;
	}

	/**
	 * Constructs a new `String` from source.
	 *
	 * If allocation fails then an empty `String` is returned. To validate that the `String` was
	 * allocated properly, check that the returned `String` is not empty.
	 *
	 * `String.from` may try and optimize for zero-allocations provided `sources` is small enough
	 * to fit inside its auxillary stackspace. In this scenario, allocations are guaranteed to
	 * never fail. See `String.stackableMax` for the amount of bytes that be put in auxillary
	 * stack-space on the current platform.
	 */
	public this(const (char)[] source) nothrow {
		if (source.length > StackedString.stack.length) {
			this.data.allocated.buffer = alloc!(char)(null, source.length);

			if (this.data.allocated.buffer.ptr) {
				// Allocation ok, now write to the new buffer.
				this.type = Type.allocated;

				copyMemory(this.data.allocated.buffer, source);
			}
		} else {
			// Strings that are small enough to fit inside the stack space allocated for the string
			// address may as well be just stored in the memory used for the address, completely
			// circumventing the need for dynamic allocation.
			this.type = Type.stacked;
			this.data.stacked.length = cast(ubyte)source.length;

			copyMemory(this.data.stacked.stack, source);
		}
	}

	public this(this) pure nothrow {
		if ((this.type == Type.allocated) && this.data.allocated.refCounter) {
			(*this.data.allocated.refCounter) += 1;
		}
	}

	public ~this() nothrow {
		if ((this.type == Type.allocated) && this.data.allocated.refCounter &&
					(((*this.data.allocated.refCounter) -= 1) == 0)) {

			dealloc(null, this.data.allocated.refCounter);
			dealloc(null, this.data.allocated.buffer);
		}
	}

	/**
	 * Returns the held character sequence as a slice.
	 *
	 * Note that the returned value is considered a weak reference, meaning that it will does not
	 * increment the `String` instance reference counter. Therefore, `String.all` runs the risk of
	 * escaping the scope of the source `String` when used improperly, such as attempting to store
	 * the reference in a structure that exists beyond the current scope.
	 */
	public const (char)[] all() return const pure nothrow {
		final switch (this.type) {
			case Type.empty: return "";

			case Type.allocated: return this.data.allocated.buffer;

			case Type.stacked: return this.data.stacked.stack[0 .. this.data.stacked.length];
		}
	}

	/**
	 * Returns the character at index `index` of the `String`.
	 *
	 * If `index` is an invalid index then an assertion will be raised.
	 */
	public char at(size_t index) const pure nothrow {
		final switch (this.type) {
			case Type.empty: assert(false, invalidAccessMessage);

			case Type.allocated: return this.data.allocated.buffer[index];

			case Type.stacked: {
				assert((index < this.data.stacked.length), invalidAccessMessage);

				return this.data.stacked.stack[index];
			}
		}
	}

	/**
	 * Returns the number of characters in the `String`.
	 */
	public size_t count() const pure nothrow {
		final switch (this.type) {
			case Type.empty: return 0;

			case Type.allocated: return this.data.allocated.buffer.length;

			case Type.stacked: return cast(size_t)this.data.stacked.length;
		}
	}

	/**
	 * Returns a pointer to the current memory in the `String`.
	 *
	 * The returned pointer is in the strongest sense a weak reference. As such, keeping hold of it
	 * beyond the current scope can result in unsafe memory operations.
	 */
	public const (char)* pointerOf() pure const nothrow {
		return this.all().ptr;
	}

	/**
	 * Returns a slice of the held characters from index `a` to length `b`.
	 *
	 * Note that the returned value is considered a weak reference, meaning that it will does not
	 * increment the `String` instance reference counter. Therefore, `String.slice` runs the risk
	 * of escaping the scope of the source `String` when used improperly, such as attempting to
	 * store the reference in a structure that exists beyond the current scope.
	 */
	public const (char)[] slice(size_t a, size_t b) return pure const nothrow {
		final switch (this.type) {
			case Type.empty: return ""[a .. b];

			case Type.allocated: return this.data.allocated.buffer[a .. b];

			case Type.stacked: {
				assert(b < this.data.stacked.length);

				return this.data.stacked.stack[a .. b];
			}
		}
	}
}

/**
 * Returns `ptr` wrapped in a `char` slice.
 *
 * `strptr` assumes that somewhere in the memory pointing to `ptr` there is a null terminating
 * byte, and will keep searching until one is found. Because of this, it is potentially very
 * dangerous to use on C-style character pointers that have no assurance of null termination, and
 * may cause noticeable process pauses while searching frantically through system memory for a null
 * terminating byte.
 */
@system
public const (char)[] strptr(const (char)* ptr) pure nothrow {
	size_t i;

	while (ptr[i]) {
		i += 1;
	}

	return ptr[0 .. i];
}

/**
 * Attempts to allocate a buffer large enough for all `String`s in `sources` and copy the
 * contents of each into the returned `String`.
 *
 * `String.concat` is very similar to `String.from` in that it creates a new `String` instance,
 * however it differs in that it creates new `String`s from combinations of other `String`s,
 * rather than character slices.
 *
 * If allocation fails then an empty `String` is returned. To validate that the `String` was
 * allocated properly, check that the returned `String` is not empty.
 *
 * `String.concat` may try and optimize for zero-allocations provided `sources` is small enough
 * to fit inside its auxillary stackspace. In this scenario, allocations are guaranteed to
 * never fail. See `String.stackableMax` for the amount of bytes that be put in auxillary
 * stack-space on the current platform.
 */
public static String concatString(scope String[] sources...) nothrow {
	String str;
	size_t sumLength;

	foreach (source; sources) {
		sumLength += source.count();
	}

	if (sumLength > String.StackedString.stack.length) {
		str.data.allocated.buffer = alloc!(char)(null, sumLength);

		if (str.data.allocated.buffer) {
			// Allocation ok, now time to populate the new buffer with the input strings.
			size_t cursor;

			foreach (source; sources) {
				copyMemory(str.data.allocated.buffer[cursor .. $], source.all());

				cursor += source.count();
			}

			str.type = String.Type.allocated;
		}
	} else {
		size_t cursor;
		str.type = String.Type.stacked;
		str.data.stacked.length = cast(ubyte)sumLength;

		foreach (source; sources) {
			copyMemory(str.data.stacked.stack[cursor .. $], source.all());

			cursor += source.count();
		}
	}

	return str;
}

/**
 * Formats `value` as a decimal-representing `String`.
 *
 * If allocation fails then an empty `String` is returned. To validate that the `String` was
 * allocated properly, check that the returned `String` is not empty.
 */
public String decString(Type)(Type value) nothrow {
	static if (__traits(isIntegral, Type)) {
		if (value) {
			enum base = 10;
			char[28] buffer;
			size_t bufferCount;

			static if (__traits(isUnsigned, Type)) {
				// Unsigend types need not apply.
				Type n1 = value;
			} else {
				Type n1 = void;

				if (value < 0) {
					// Negative value.
					n1 = -value;
					buffer[0] = '-';
					bufferCount += 1;
				} else {
					// Positive value.
					n1 = value;
				}
			}

			while (n1) {
				buffer[bufferCount] = cast(char)((n1 % base) + '0');
				n1 = (n1 / base);
				bufferCount += 1;
			}

			foreach (i; 0 .. (bufferCount / 2)) {
				swap(buffer[i], buffer[bufferCount - i - 1]);
			}

			return String(buffer[0 .. bufferCount]);
		} else {
			// Return string of zero.
			return String("0");
		}
	} else static if (__traits(isFloating, Type)) {
		return String("0.0");
	}
}

/**
 * Attempts to allocate and format `values` into a single `String`.
 *
 * If allocation fails then an empty `String` is returned. To validate that the `String` was
 * allocated properly, check that the returned `String` is not empty.
 *
 * See `String.from` and `String.concat` for further information about the characteristics of
 * `String.format` for single and multi-argument calls respectively.
 */
public String formatString(immutable (char)[] fmt = "", Types...)(auto ref Types values) nothrow {
	String formatImpl(Type)(auto ref Type value) nothrow {
		import ona.meta : isSliceable;

		static if (is(Type == String)) {
			return value;
		} else static if (__traits(hasMember, Type, "toString")) {
			return value.toString();
		} else static if (is(Type == char)) {
			return String(value);
		} else static if (__traits(isArithmetic, Type)) {
			return decString(value);
		} else static if (is(Type == PointerType*, PointerType)) {
			return hexString(cast(size_t)value);
		} else static if (isSliceable!(Type)) {
			return String("[]");
		} else static if (is(Type == struct)) {
			return String("{}");
		} else static if (is(Type == enum)) {
			return String("enum {}");
		} else static if (is(Type == union)) {
			return String("union {}");
		}
	}

	if (values.length == 1) {
		return formatImpl(values[0]);
	} else {
		String[values.length] texts;

		static foreach (i; 0 .. values.length) {
			texts[i] = formatImpl(values[i]);
		}

		return concatString(texts);
	}
}

/**
 * Formats `value` as a hexadecimal-representing `String`.
 *
 * If allocation fails then an empty `String` is returned. To validate that the `String` was
 * allocated properly, check that the returned `String` is not empty.
 */
public String hexString(Type)(Type value) nothrow {
	static if (__traits(isIntegral, Type)) {
		if (value) {
			enum hexLength = (Type.sizeof << 1);
			enum hexPrefix = "0x";
			enum hexDigits = "0123456789ABCDEF";
			char[hexPrefix.length + hexLength] buffer;
			size_t bufferCount = hexPrefix.length;

			copyMemory(buffer, hexPrefix);

			for (size_t j = ((hexLength - 1) * 4); bufferCount < buffer.length; j -= 4) {
				buffer[bufferCount] = hexDigits[(value >> j) & 0x0f];
				bufferCount += 1;
			}

			return String(buffer[0 .. bufferCount]);
		} else {
			// Return string of zero.
			return String("0x0");
		}
	} else static if (__traits(isFloating, Type)) {
		return String("0x0");
	}
}

/**
 * Copies the contents of `source` into `destination`, returning the resulting sequence as a sub-
 * slice of `destination`, or a sentineled "empty" character sequence if either `destination` or
 * `source` have a length of `0`.
 *
 * If `destination` is not big enough to store `source` then `source` will be truncated, and the
 * last available character in `destination` becomes the null-terminating sentinel character. This
 * behavior can be checked for by confirming that the returned sequence is the expected length plus
 * the null-terminating sentinel character.
 */
public const (char)[] sentineledBuffer(char[] destination, const (char)[] source) pure nothrow {
	immutable (size_t) written = copyMemory(destination, source);

	if (written) {
		if (written < destination.length) {
			// Write the null terminator into the buffer.
			destination[written] = '\0';

			return destination[0 .. (written + 1)];
		}

		// Else if no space overwrite the last character in the buffer.
		destination[$ - 1] = '\0';

		return destination[0 .. written];
	}

	// Fallback string.
	return "\0";
}

/**
 * Attempts to return a slice to a copy of `str` with an additional null terminating sentinel byte
 * on the end.
 *
 * Note that the returned slice is ephemeral as it uses thread-local storage for the sequence
 * buffer. This has the benefit of greatly reducing the allocations and deallocations necessary to
 * convert strings on the fly to zero overhead for most character sequences but means that the
 * buffer contents is overwritten every new time `sentineledSequence` is called. This also means
 * that references to the buffer aren't even safe in the callsite scope, let alone any others.
 *
 * For these reasons, this function is a very low-level feature of the library and must be avoided
 * where possible.
 *
 * If the existing thread-local buffer fails to extend its buffer size where necessary then null is
 * returned. This may want to be optionally checked before passing the erroneous value along to the
 * next C API-based function, however this may not always be necessary, and can in fact depend
 * entirely around the robustness of the subject API.
 */
@system
public const (char)[] sentineledSequence(const (char)[] str) nothrow {
	static char[] buffer;
	// String length plus null terminating sentinel.
	size_t requiredLength = (str.length + 1);

	if (requiredLength > buffer.length) {
		buffer = realloc(null, buffer, requiredLength);

		if (buffer == null) {
			return null;
		}
	}

	copyMemory(buffer, str);

	buffer[str.length] = 0;

	return buffer[0 .. requiredLength];
}

/**
 * Generatively splits `str` into sub-string `char` slices wherever a character matching
 * `delimiter` is present.
 *
 * See `ona.algorithm.Enumerator` for more detail on how to retieve split tokens.
 */
@safe
public Enumerator!(const (char)[]) split(ref String str, char delimiter) pure nothrow {
	size_t cursor;
	size_t cursorLast;

	return Enumerator!(const (char)[])((i, produce) {
		while (cursor < str.count()) {
			immutable (char) c = str.at(cursor);

			if (c == delimiter) {
				produce(str.slice(cursorLast, cursor));

				cursor += 1;
				cursorLast = cursor;

				return;
			}

			cursor += 1;
		}

		if (cursorLast != cursor) {
			produce(str.slice(cursorLast, cursor));

			cursorLast = cursor;
		}
	});
}
