module ona.meta;

template anySatisfy(alias Constraint, Types...) {
	static foreach (Type; Types) {
		static if (!is(typeof(anySatisfy) == bool) && Constraint!(Type)) {
			enum anySatisfy = true;
		}
	}

	// If not yet defined...
	static if (!is(typeof(anySatisfy) == bool)) {
		enum anySatisfy = false;
	}
}

template isCallable(Types...) if (Types.length == 1) {
    static if (is(typeof(& Types[0].opCall) == delegate)) {
        // Type is a object which has a member function opCall().
        enum bool isCallable = true;
	} else static if (is(typeof(& Types[0].opCall) V : V*) && is(V == function)) {
        // Type is a type which has a static member function opCall().
        enum bool isCallable = true;
	} else {
        enum bool isCallable = isSomeFunction!(Types);
	}
}

/**
 * Is `true` if `Type` is, or can be implicitly converted to, a slice type, otherwise `false`.
 *
 * The following things are true of something that is sliceable:
 *
 *   * It may be passed to a function that accepts a slice with a matching element type.
 *
 *   * It support the slice operator.
 */
public enum isSliceable(Type) = (__traits(isStaticArray, Type) ||is(Type == Element[], Element));

template isSomeFunction(Types...) if (Types.length == 1)
{
	static if (
			(is(typeof(& Types[0]) U : U*) && is(U == function)) ||
			is(typeof(& Types[0]) U == delegate)
		) {

		// Type is a (nested) function symbol.
		enum bool isSomeFunction = true;
	} else static if (is(Types[0] W) || is(typeof(Types[0]) W)) {
		// Type is an expression or a type.  Take the type of it and examine.
		static if (is(W F : F*) && is(F == function)) {
			enum bool isSomeFunction = true; // function pointer
		} else {
			enum bool isSomeFunction = (is(W == function) || is(W == delegate));
		}
	} else {
		enum bool isSomeFunction = false;
	}
}

alias AliasSeq(Types...) = Types;

template Fields(Type) {
	static if (is(Type == struct) || is(Type == union)) {
		alias Fields = typeof(Type.tupleof[0 .. ($ - __traits(isNested, Type))]);
	} else static if (is(Type == class)) {
		alias Fields = typeof(Type.tupleof);
	} else {
		alias Fields = AliasSeq!(Type);
	}
}

template FunctionTypeOf(func...) if (func.length == 1 && isCallable!(func))
{
    static if (is(typeof(& func[0]) Fsym : Fsym*) && is(Fsym == function) || is(typeof(& func[0]) Fsym == delegate))
    {
        alias FunctionTypeOf = Fsym; // HIT: (nested) function symbol
    }
    else static if (is(typeof(& func[0].opCall) Fobj == delegate))
    {
        alias FunctionTypeOf = Fobj; // HIT: callable object
    }
    else static if (is(typeof(& func[0].opCall) Ftyp : Ftyp*) && is(Ftyp == function))
    {
        alias FunctionTypeOf = Ftyp; // HIT: callable type
    }
    else static if (is(func[0] T) || is(typeof(func[0]) T))
    {
        static if (is(T == function))
            alias FunctionTypeOf = T;    // HIT: function
        else static if (is(T Fptr : Fptr*) && is(Fptr == function))
            alias FunctionTypeOf = Fptr; // HIT: function pointer
        else static if (is(T Fdlg == delegate))
            alias FunctionTypeOf = Fdlg; // HIT: delegate
        else
            static assert(0);
    }
    else
        static assert(0);
}

template Parameters(Func...) if (Func.length == 1 && isCallable!(Func)) {
	static if (is(FunctionTypeOf!(Func) P == function)) {
		alias Parameters = P;
	} else {
		static assert(false, "argument has no parameters");
	}
}

template ReturnType(func...) if (func.length == 1 && isCallable!func)
{
    static if (is(FunctionTypeOf!func R == return))
        alias ReturnType = R;
    else
        static assert(0, "argument has no return type");
}

template Unqual(Type)
{
	version (none) // Error: recursive alias declaration @@@BUG1308@@@
	{
			 static if (is(Type U ==     const (U))) alias Unqual = Unqual!(U);
		else static if (is(Type U == immutable (U))) alias Unqual = Unqual!(U);
		else static if (is(Type U ==     inout (U))) alias Unqual = Unqual!(U);
		else static if (is(Type U ==    shared (U))) alias Unqual = Unqual!(U);
		else                                    alias Unqual =        Type;
	}
	else // workaround
	{
			 static if (is(Type U ==          immutable (U))) alias Unqual = U;
		else static if (is(Type U == shared inout const (U))) alias Unqual = U;
		else static if (is(Type U == shared inout       (U))) alias Unqual = U;
		else static if (is(Type U == shared       const (U))) alias Unqual = U;
		else static if (is(Type U == shared             (U))) alias Unqual = U;
		else static if (is(Type U ==        inout const (U))) alias Unqual = U;
		else static if (is(Type U ==        inout       (U))) alias Unqual = U;
		else static if (is(Type U ==              const (U))) alias Unqual = U;
		else                                             	  alias Unqual = Type;
	}
}
