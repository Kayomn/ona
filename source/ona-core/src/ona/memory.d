module ona.memory;

private {
	import clib = core.stdc.stdlib;
	import ona.meta;
	import ona.string;
	import ona.serial;
}

private struct __InoutWorkaroundStruct {

}

/**
 * An interface that defines an allocation strategy.
 *
 * Effectively the structure is laid out like an inline vtable. All structures that wish to be
 * allocators have this structure as their first field. All functions which refer to a "context"
 * are referring to any memory that comes after the address of `Allocator` in memory.
 *
 * Because of this, it is typically advised that all allocators be allocated as dynamic memory,
 * with enough additional padding at the end of the allocation to store whatever user data is
 * needed for the callbacks to work.
 */
public struct Allocator {
	/**
	 * Callback for dynamically allocating `size` bytes of memory with `context` as the allocation
	 * state.
	 *
	 * If `memory` is a non-`null` value then the function will attempt to re-allocate `memory`
	 * into a buffer of `size` bytes. After calling the function in this scenario, `memory` should
	 * be considered an invalid reference and should not not be used further.
	 *
	 * If the allocator fails to allocate `size` bytes of memory then `null` is returned.
	 */
	ubyte[] function(void* context, size_t size, void[] memory) nothrow allocate;

	/**
	 * Callback for dynamically de-allocating `ptr` with `context` as the allocation state.
	 *
	 * if `memory` is `null` then the function will de-allocate all memory allocated by the
	 * `Allocator`.
	 *
	 * `Allocator`s that do not support the freeing of individual memory regions and will do
	 * nothing when supplied a non-`null` value for `ptr`.
	 */
	void function(void* context, void* ptr) nothrow deallocate;
}

/**
 * `Allocator` implementation that uses a "bump the pointer" memory allocation strategy. Each new
 * call to allocate memory grabs the next number of bytes request in memory and increments the
 * pointer in anticipation for the next allocation.
 *
 * This approach sacrifices memory re-allocation efficiency and individual de-allocation in favor
 * of constant-time allocations in best-case scenarios.
 *
 * It should be noted that allocation is only not constant-time when the pointer overruns the
 * existing heap space allocated to the allocator, at which point it will request more using
 * `Allocator.parentAllocator`.
 */
public struct BumpAllocator {
	private Allocator base = {
		allocate: (context, size, memory) {
			auto allocator = cast(BumpAllocator*)context;
			immutable (size_t) newHeapPointer = (allocator.heapPointer + size);
			size_t heapSize = allocator.heap.length;

			if (!heapSize) {
				return null;
			}

			if (newHeapPointer > heapSize) {
				heapSize *= 2;
				allocator.heap = alloc!(ubyte)(allocator.parentAllocator, heapSize);

				if (!allocator.heap) {
					return null;
				}
			}

			scope (exit) allocator.heapPointer = newHeapPointer;

			if (memory) {
				ubyte[] allocation = allocator.heap[allocator.heapPointer .. newHeapPointer];

				copyMemory(allocation, memory);

				return allocation;
			}

			return allocator.heap[allocator.heapPointer .. newHeapPointer];
		}
	};

	/**
	 * Used for allocating `BumpAllocator`'s heap buffers.
	 *
	 * Unfortunately nothing comes from nothing.
	 */
	private const (Allocator)* parentAllocator;

	/**
	 * Current bump pointer step on the heap.
	 *
	 * This will increment and deincrement as memory is allocated and de-allocated respectively.
	 */
	private size_t heapPointer;

	/**
	 * The number of bytes at which point the heap will no longer be able to grow to accomodate.
	 *
	 * A value of `0` informs the `BumpAllocator` that it may take as much memory as it wishes it /
	 * needs to.
	 */
	private size_t growLimit;

	/**
	 * Reserved local heap buffer of memory used for allocation.
	 */
	private ubyte[] heap;

	/**
	 * Returns a pointer to the `BumpAllocator`'s boxed `Allocator` interface.
	 *
	 * Note that once the original `BumpAllocator` goes out of scope, so too will the pointer to
	 * the `Allocator`.
	 */
	@safe
	public pragma(inline, true) const (Allocator)* allocator() pure const return nothrow {
		return &this.base;
	}

	/**
	 * Resets the heap pointer to `0`, effectively making all memory previously allocated by the
	 * `BumpAllocator` invalid.
	 *
	 * Attempting to use references to memory that were allocated by this `BumpAllocator` but
	 * before calling this function is erroneous and undefined behavior.
	 */
	@safe
	public void reset() pure nothrow {
		this.heapPointer = 0;
	}

	/**
	 * Returns `true` if the `BumpAllocator` has been initialized with dynamic memory, otherwise
	 * `false` if not.
	 */
	@safe
	public bool exists() pure const nothrow {
		return (this.heap != null);
	}

	/**
	 * Attempts to release `BumpAllocator` heap and resets the heap pointer.
	 */
	public void release() nothrow {
		if (this.heap) {
			dealloc(this.parentAllocator, this.heap);
		}

		this.heapPointer = 0;
	}

	/**
	 * Returns the number bytes that have been allocated by `BumpAllocator` but not yet released.
	 *
	 * As `BumpAllocator` uses a bump pointer approach memory can only be freed all in one go via
	 * `BumpAllocator.reset`. Because of this, `BumpAllocator.used` is a bad value to use as a
	 * heuristic for actively used memory.
	 */
	@safe
	public size_t used() pure const nothrow {
		return this.heapPointer;
	}
}

/**
 * Nullable-like wrapper for stack-allocated types, allowing them to be optionally present inside
 * the wrapper.
 */
public struct Optional(Type) {
	/**
	 * Current value.
	 */
	private Type value;

	/**
	 * Whether or not a value is currently held within.
	 */
	private bool valueFlag;

	/**
	 * Returns `true` if the `Optional` contains a value, otherwise `false`.
	 */
	@safe
	public bool hasValue() pure const nothrow {
		return this.valueFlag;
	}

	/**
	 * Assigns `value` to the internal value.
	 */
	@safe
	public void assignValue(Type value) pure nothrow {
		this.value = value;
		this.valueFlag = true;
	}

	/**
	 * Returns the stored value of the `Optional`, provided it has one.
	 *
	 * If the `Optional` is empty then an assertion is raised.
	 */
	@safe
	public ref Type valueOf() pure nothrow {
		assert(this.valueFlag);

		return this.value;
	}
}

/**
 * A reference-counted wrapper for dynamic memory resources managed by an `Allocator` instance.
 */
public struct Ref(Type) if (isDynamicResource!(Type)) {
	alias refValue this;

	private size_t* refCounter;

	/**
	 * Direct access to the reference counted value.
	 *
	 * All members contained in `Type` are automatically forwarded to the scope of `Ref`, so
	 * direct access of this member is not typically necessary.
	 */
	public Type refValue;

	@safe
	private this(size_t* refCounter, Type refValue) pure nothrow {
		this.refCounter = refCounter;
		this.refValue = refValue;
	}

	@safe
	public this(this) pure nothrow {
		if (this.refCounter) {
			(*this.refCounter) += 1;
		}
	}

	public ~this() nothrow {
		if (this.refCounter && (((*this.refCounter) -= 1) == 0)) {
			dealloc(this.refValue.allocatorOf(), this.refCounter);
			this.refValue.release();
		}
	}
}

/**
 * Implements an allocation strategy that is nearly equivalent to `BumpAllocator` except no dynamic
 * memory is used, instead opting to create a buffer on the stack of `size` bytes in length.
 *
 * Because static memory is used, once the original allocation is full only `null` pointers are
 * returned when calls to `StackAllocator.base.allocate` are made.
 */
public struct StackAllocator(size_t size) {
	private Allocator base = {
		allocate: (context, size, memory) {
			auto allocator = cast(StackAllocator*)context;

			if (memory) {
				// If the re-allocated memory is the last memory that was allocated then some
				// intelligent re-allocation can be made that re-uses the existing buffer on top of
				// any new memory if needed.
				immutable (size_t) startPointer = (
					((allocator.stack.ptr + allocator.stackPointer) == (memory.ptr + memory.length)) ?
					cast(size_t)((cast(ubyte*)memory.ptr) - allocator.stack.ptr) :
					allocator.stackPointer
				);

				immutable (size_t) stackPointer = (startPointer + size);

				if (stackPointer < allocator.stack.length) {
					ubyte[] allocation = allocator.stack[startPointer .. stackPointer];
					allocator.stackPointer = stackPointer;

					copyMemory(allocation, memory);

					return allocation;
				}
			} else {
				immutable (size_t) newStackPointer = (allocator.stackPointer + size);

				if (newStackPointer < allocator.stack.length) {
					scope (exit) allocator.stackPointer = newStackPointer;

					return allocator.stack[allocator.stackPointer .. newStackPointer];
				}
			}

			// Out of stack memory.
			return null;
		}
	};

	/**
	 * Current bump pointer step on the stack.
	 *
	 * This will increment and deincrement as memory is allocated and de-allocated respectively.
	 */
	private size_t stackPointer;

	/**
	 * Reserved local stack buffer of memory used for allocation.
	 */
	private ubyte[size] stack;

	/**
	 * Returns a pointer to the `StackAllocator`'s boxed `Allocator` interface.
	 *
	 * Note that once the original `StackAllocator` goes out of scope, so too will the pointer to
	 * the `Allocator`.
	 */
	@safe
	public pragma(inline, true) const (Allocator)* allocatorOf() pure const return nothrow {
		return &this.base;
	}

	/**
	 * Resets the stack pointer to `0`, effectively making all memory previously allocated by the
	 * `StackAllocator` invalid.
	 *
	 * Attempting to use references to memory that were allocated by this `StackAllocator` but
	 * before calling this function is erroneous and undefined behavior.
	 */
	@safe
	public void reset() pure nothrow {
		this.stackPointer = 0;
	}

	/**
	 * Returns the number bytes that have been allocated by `StackAllocator` but not yet released.
	 *
	 * As `StackAllocator` uses a bump pointer approach memory can only be freed all in one go via
	 * `StackAllocator.reset`. Because of this, `StackAllocator.used` is a bad value to use as a
	 * heuristic for actively used memory.
	 */
	@safe
	public size_t used() pure const nothrow {
		return this.stackPointer;
	}
}

/**
 * A type-safe union wrapper for a discriminated set of types identified by `Types`.
 *
 * Any of the allowed types may be assigned to the `Union`, overwritting the previously stored
 * value, making it functionally equivalent to a regular `union` but with inherent type safety
 * through templating and runtime type information.
 *
 * While in the default construction state no value type can be retrieved until an initial value is
 * assigned to it.
 *
 * A union is limited to `ubyte.max` types.
 */
public struct Union(Types...) if (Types.length < ubyte.max) {
	private template isAllowedType(OtherType) {
		static bool isAllowedTypeImpl() {
			bool res = false;

			static foreach (Type; Types) {
				static if (is(OtherType == Type)) {
					res = true;
				}
			}

			return res;
		}

		enum isAllowedType = isAllowedTypeImpl();
	}

	private ubyte[() {
		size_t sizeofMax;

		static foreach (Type; Types) {
			if (Type.sizeof > sizeofMax) {
				sizeofMax = Type.sizeof;
			}
		}

		return sizeofMax;
	}()] store;

	private ubyte typeIndex;

	static foreach (Type; Types) {
		/**
		 * Constructs a `Union` containing `value`.
		 */
		public this(Type value) nothrow {
			this.assignImpl(value);
		}
	}

	public this(this) nothrow {
		foreach (Type; Types) {
			static if (hasElaborateCopyConstructor!(Type)) {
				Type* value = this.as!(Type);

				if (value) {
					value.__xpostblit();
				}
			}
		}
	}

	public ~this() nothrow {
		static foreach (Type; Types) {
			static if (hasDestructor!(Type)) {
				Type* value = this.as!(Type);

				if (value) {
					value.__xdtor();
				}
			}
		}
	}

	/**
	 * A dynamic value casting function for converting the `Union` to `Type`, returning a
	 * pointer to the value if `Type` matched the type of the held value, or `null` otherwise.
	 */
	public Type* as(Type)() pure inout nothrow if (isAllowedType!(Type)) {
		return (this.isType!(Type) ? cast(Type*)this.store.ptr : null);
	}

	static foreach (i, Type; Types) {
		private void assignImpl(ref Type value) nothrow {
			emplace(cast(Type*)this.store.ptr, value);

			this.typeIndex = (i + 1);
		}
	}

	static foreach (Type; Types) {
		/**
		 * Assigns `value` to the `Union`, replacing the previously stored value if one
		 * existed.
		 */
		public ref Type opAssign(Type value) nothrow {
			this.assignImpl(value);

			return *cast(Type*)this.store.ptr;
		}
	}

	/**
	 * Returns `true` if the `Union` is not empty, otherwise `false`.
	 */
	@safe
	public bool hasValue() pure const nothrow {
		return (this.typeIndex != 0);
	}

	/**
	 * Returns true if the currently held value is `ExpectedType`, otherwise `false`
	 */
	@safe
	public bool isType(ExpectedType)() pure const nothrow if (isAllowedType!(ExpectedType)) {
		static foreach (i, Type; Types) {
			static if (is(Type == ExpectedType)) {
				return (this.typeIndex == (i + 1));
			}
		}
	}

	/**
	 * Functions as a switch statement for the opaque `Union` instance, implementing per-type
	 * behavior for the potential members held within, calling the appropriate function callback
	 * according to the held value.
	 *
	 * Example:
	 * ```d
	 * Union!(long, double) algebraic = 1L;
	 *
	 * algebraic.match(
	 *   (long val) => val += 1,
	 *   (double val) => val += 0.5
	 * );
	 * ```
	 *
	 * Unless a value is specified, `Union` is initialized to a state where it contains no value
	 * and must be handled accordingly. Whether or not a `Union` instance contains a value can be
	 * checked by confirming `Union.typeOf` is not `null`.
	 */
	public auto match(
			HandlerFuncs...
		)(
			HandlerFuncs handlerFuncs
		) inout nothrow if (HandlerFuncs.length != 0) {

		enum handlerMap = () {
			// Space for all types plus a potential final callable for handling empty TaggedUnions.
			size_t[Types.length + 1] handlerMap;
			bool isFallbackAdded;

			foreach (typeIndex, Type; Types) {
				bool isAdded;

				foreach (handlerIndex, HandlerFunc; HandlerFuncs) {
					static if (isSomeFunction!(HandlerFunc)) {
						alias HandlerArgs = Parameters!(HandlerFunc);

						static if (HandlerArgs.length == 0) {
							handlerMap[Types.length] = (handlerIndex + 1);
							isFallbackAdded = true;
						} else {
							static assert(
								((HandlerArgs.length == 1) && isAllowedType!(HandlerArgs[0])),

								(typeof(HandlerFunc).stringof ~
										" does not handle any allowed types")
							);

							static if (is(HandlerArgs[0] == Type)) {
								assert(!isAdded,
										(HandlerFunc.stringof ~ " appears more than once"));

								handlerMap[typeIndex] = (handlerIndex + 1);
								isAdded = true;
							}
						}
					}
				}
			}

			return handlerMap;
		}();

		static if (is(ReturnType!(HandlerFuncs[0]) == void)) {
			// Non-returning match implementation.
			foreach (i, Type; Types) {
				Type* value = this.as!(Type);

				if (value) {
					enum handlerIndex = handlerMap[i];

					static if (handlerIndex) {
						handlerFuncs[handlerIndex - 1](*value);
					}
				}
			}

			static if (handlerMap[Types.length]) {
				handlerFuncs[handlerMap[Types.length] - 1]();
			}
		} else {
			// Returning match implementation.
			foreach (i, Type; Types) {
				Type* value = this.as!(Type);

				if (value) {
					enum handlerIndex = handlerMap[i];

					static if (handlerIndex) {
						return handlerFuncs[handlerIndex - 1](*value);
					}
				}
			}

			static if (handlerMap[Types.length]) {
				return handlerFuncs[handlerMap[Types.length] - 1]();
			} else {
				static assert(false, "No returning visitor supplied for fallback");
			}
		}
	}

	/**
	 * Resets the contents of the `Union`, wiping it of any value.
	 *
	 * While in this state no value type can be retrieved until a new value is assigned to it.
	 */
	public void reset() pure nothrow {
		this.typeIndex = 0;

		zeroMemory(this.store);
	}

	/**
	 * Attempts to format the contained type within the `Union` as a `ona.string.String`, returning
	 * the result.
	 *
	 * Should `ona.string.String` fail to allocate an empty `String` is returned. Use
	 * `ona.string.String.count` to check that the returned `ona.string.String` contains data.
	 *
	 * If the `Union` contains no value then an empty `ona.string.String` is returned.
	 */
	public String toString() const nothrow {
		if (this.typeIndex) {
			foreach (Type; Types) {
				Type* value = this.as!(Type);

				if (value) {
					return formatString(*value);
				}
			}
		}

		return String();
	}
}

/**
 * An empty `Optional` of type `Type`.
 */
public enum empty(Type) = Optional!(Type).init;

/**
 * Is `true` if `Type` has defined destructor semantics, otherwise `false`.
 */
public enum hasDestructor(Type) = (is(Type == struct) && __traits(hasMember, Type, "__xdtor"));

/**
 * Is `true` if `Type` uses elaborate assignment semantics, such as an `opApply` overload.
 */
public template hasElaborateAssign(Type) {
	static if (__traits(isStaticArray, Type) && Type.length) {
		enum bool hasElaborateAssign = hasElaborateAssign!(typeof(Type.init[0]));
	} else static if (is(Type == struct)) {
		enum hasElaborateAssign = (
			is(typeof(Type.init.opAssign(rvalueOf!(Type)))) ||
			is(typeof(Type.init.opAssign(lvalueOf!(Type)))) ||
			anySatisfy!(.hasElaborateAssign, Fields!(Type))
		);
	} else {
		enum hasElaborateAssign = false;
	}
}

/**
 * Is `true` if `Type` uses elaborate copy construction semantics, such as a copy or postblit
 * constructor, otherwise `false`.
 */
public template hasElaborateCopyConstructor(Type) {
	static if (__traits(isStaticArray, Type) && Type.length) {
		enum hasElaborateCopyConstructor = hasElaborateCopyConstructor!(typeof(Type.init[0]));
	} else static if (is(Type == struct)) {
		enum hasElaborateCopyConstructor =
				(__traits(hasCopyConstructor, Type) || __traits(hasPostblit, Type));
	} else {
		enum hasElaborateCopyConstructor = false;
	}
}

/**
 * Is `true` if `Lhs` is assignable to `Rhs`, which defaults to be the same as `Lhs`, otherwise
 * `false`.
 */
public enum isAssignable(Lhs, Rhs = Lhs) = (
	__traits(compiles, lvalueOf!(Lhs) = rvalueOf!(Rhs)) &&
	__traits(compiles, lvalueOf!(Lhs) = lvalueOf!(Rhs))
);

/**
 * Returns the rvalue of `Type`.
 */
public Type rvalueOf(Type)(Type val) {
	return val;
}

/**
 * Returns the rvalue of `Type`.
 */
public Type rvalueOf(Type)(inout (__InoutWorkaroundStruct) = __InoutWorkaroundStruct.init);

/**
 * Returns the locator value of `Type`.
 */
public ref Type lvalueOf(Type)(inout (__InoutWorkaroundStruct) = __InoutWorkaroundStruct.init);

/**
 * Is `true` if `Type` is a manageable resource type, otherwise `false`.
 */
public enum isDynamicResource(Type) =
		(__traits(hasMember, Type, "allocatorOf") && __traits(hasMember, Type, "release"));

/**
 * An empty / "null" `Ref` of type `Type`.
 */
public enum nullRef(Type) = Ref!(Type).init;

private __gshared const (Allocator) mallocator = {
	allocate: (context, size, memory) {
		ubyte* ptr =
				cast(ubyte*)(memory.ptr ? clib.realloc(memory.ptr, size) : clib.malloc(size));

		return (ptr ? ptr[0 .. size] : null);
	},

	deallocate: (context, ptr) {
		clib.free(ptr);
	}
};

/**
 * Dynamically allocates a single `Type` using `allocator`, making no guarantees about the
 * default data inside the returned pointer.
 *
 * If `allocator` is `null` then the default runtime `Allocator` is used.
 *
 * If memory fails to allocate then `null` is returned instead.
 */
public Type* alloc(Type)(const (Allocator)* allocator) nothrow {
	return cast(Type*)allocRaw(allocator, Type.sizeof).ptr;
}

/**
 * Dynamically allocates `length` number of `Type` as an array using `allocator`, making no
 * guarantees about the default data inside the returned slice.
 *
 * If `allocator` is `null` then the default runtime `Allocator` is used.
 *
 * If the allocator fails to allocate memory or `length` is `0`, `null` is returned instead.
 */
public Type[] alloc(Type)(const (Allocator)* allocator, size_t length) nothrow {
	return cast(Type[])allocRaw(allocator, (Type.sizeof * length));
}

/**
 * Dynamically allocates `byteSize` bytes of raw memory using `allocator`, making no guarantees
 * about the default data inside the returned slice.
 *
 * If `allocator` is `null` then the default runtime `Allocator` is used.
 *
 * If memory fails to allocate then `null` is returned instead.
 */
public ubyte[] allocRaw(const (Allocator)* allocator, size_t byteSize) nothrow {
	if (allocator) {
		return allocator.allocate(cast(void*)allocator, byteSize, null);
	}

	return mallocator.allocate(null, byteSize, null);
}

/**
 * Returns the raw bytes of `value` by reference.
 */
public pragma(inline, true) ubyte[] bytesOf(Type)(ref Type value) pure nothrow {
	return (cast(ubyte*)&value)[0 .. Type.sizeof];
}

/**
 * Returns a static array raw bytes of `value` equal in the size to the number of bytes in `Type`.
 */
public pragma(inline, true) ubyte[Type.sizeof] bytesOf(Type)(Type value) pure nothrow {
	return (cast(ubyte*)&value)[0 .. Type.sizeof];
}

/**
 * Copies the data from `source` into `destination`, returning the number of bytes that are
 * actually written based on the minimum size of `destination` and `source`.
 */
public size_t copyMemory(void[] destination, const (void)[] source) pure nothrow {
	immutable (size_t) size =
			((destination.length < source.length) ? destination.length : source.length);

	foreach (i; 0 .. size) {
		*cast(ubyte*)(destination.ptr + i) = *cast(ubyte*)(source.ptr + i);
	}

	return size;
}

/**
 * Allocates a `BumpAllocator` with `initialSize` bytes of memory using `parentAllocator` as the
 * allocation strategy for the initial heap allocation and any future re-allocation.
 *
 *`growLimit` acts as the hard limit of any future growth once the current buffer limit is reached,
 * with `0` effectively allowing the `BumpAllocator` to grow as much as it wants.
 */
public BumpAllocator createBumpAllocator(
			size_t initialSize, size_t growLimit, const (Allocator)* parentAllocator) nothrow {

	BumpAllocator allocator = {
		growLimit: growLimit,
		parentAllocator: parentAllocator,
		heap: alloc!(ubyte)(parentAllocator, initialSize)
	};

	return allocator;
}

/**
 * Attempts to deallocate a single value at `value` using `allocator` and resets the pointer to
 * `null`.
 *
 * If `allocator` is `null` then the default runtime `Allocator` is used.
 *
 * Attempting to free a `value` that points to memory not allocated by `allocator` is erroneous and
 * undefined behavior.
 *
 * Attempting to free a `value` equal to `null` does nothing.
 */
public void dealloc(Type)(const (Allocator)* allocator, auto ref Type* value) nothrow {
	if (value) {
		if (allocator) {
			if (allocator.deallocate) {
				allocator.deallocate(cast(void*)allocator, value);
			}
		} else {
			mallocator.deallocate(null, value);
		}

		value = null;
	}
}

/**
 * Attempts to deallocate the array at `value` using `allocator` and resets the slice to `null`.
 *
 * If `allocator` is `null` then the default runtime `Allocator` is used.
 *
 * Attempting to deallocate memory that has already been freed or that does not belong to the
 * specified `Allocator` is an erroneous action and undefined behavior.
 *
 * If `allocator` is `null` then the default engine `Allocator` is used.
 *
 * Attempting to free a `value` equal to `null` does nothing.
 */
public void dealloc(Type)(const (Allocator)* allocator, auto ref Type[] value) nothrow {
	if (value) {
		if (allocator) {
			if (allocator.deallocate) {
				allocator.deallocate(cast(void*)allocator, value.ptr);
			}
		} else {
			mallocator.deallocate(null, value.ptr);
		}

		value = null;
	}
}

/**
 * Emplaces `Type.init` into `chunk` of type `Type`, performing in-place initialization of `chunk`
 * as `Type`.
 *
 * This allows for both trivial and non-trivial initialization of any data type without invoking
 * copy or move semantics with ease.
 */
public void emplace(Type)(Type* chunk) pure nothrow {
    emplaceRef!(Type)(*chunk);
}

/**
 * Emplaces `args` into `chunk` of type `Type`, performing in-place initialization of `chunk` as
 * `Type`.
 *
 * This allows for both trivial and non-trivial initialization of any data type without invoking
 * copy or move semantics with ease.
 */
public void emplace(Type, Args...)(
		Type* chunk,
		auto ref Args args
	) if (is(Type == struct) || (Args.length == 1)) {

    emplaceRef!(Type)(*chunk, args);
}

private void emplaceInitializer(Type)(scope ref Type chunk) @trusted pure nothrow {
	static if (!hasElaborateAssign!(Type) && isAssignable!(Type)) {
		// Basically for any non-structs.
		chunk = Type.init;
	} else {
		static if (__traits(isZeroInit, Type)) {
			import core.stdc.string : memset;

			// Fast.
			memset(&chunk, 0, Type.sizeof);
		} else {
			// Not so fast.
			static immutable (Type) init = Type.init;

			memcpy(&chunk, &init, Type.sizeof);
		}
	}
}

private void emplaceRef(ChunkType, Args...)(
		ref ChunkType chunk,
		auto ref Args args
	) if (is(ChunkType == Unqual!(ChunkType))) {

	emplaceRef!(ChunkType, ChunkType)(chunk, args);
}

private void emplaceRef(Type, ChunkType, Args...)(ref ChunkType chunk, auto ref Args args) {
	static if (args.length == 0) {
		static assert(
			is(typeof({static Type i;})),
			(
				"Cannot emplace a " ~
				Type.stringof ~
				" because " ~
				Type.stringof ~
				".this() is annotated with @disable."
			)
		);

		emplaceInitializer(chunk);
	} else static if ((!is(Type == struct) && (Args.length == 1)) || (Args.length == 1) &&
			is(typeof({Type t = args[0];})) || is(typeof(Type(args)))) {

		static struct S {
			Type payload;

			this(ref Args x) {
				static if (Args.length == 1) {
					static if (is(typeof(payload = x[0]))) {
						payload = x[0];
					} else {
						payload = Type(x[0]);
					}
				} else {
					payload = Type(x);
				}
			}
		}

		if (__ctfe) {
			static if (is(typeof(chunk = Type(args)))) {
				chunk = Type(args);
			} else static if (args.length == 1 && is(typeof(chunk = args[0]))) {
				chunk = args[0];
			} else {
				assert(
					false,
					("CTFE emplace doesn't support " ~ Type.stringof ~ " from " ~ Args.stringof
				));
			}
		} else {
			S* p = () @trusted { return cast(S*)&chunk; }();

			static if (ChunkType.sizeof > 0) {
				emplaceInitializer(*p);
			}

			p.__ctor(args);
		}
	} else static if (is(typeof(chunk.__ctor(args)))) {
		// This catches the rare case of local types that keep a frame pointer
		emplaceInitializer(chunk);
		chunk.__ctor(args);
	} else {
		// We can't emplace. Try to diagnose a disabled postblit.
		static assert(
			!(Args.length == 1 && is(Args[0] : Type)),
			(
				"Cannot emplace a " ~
				Type.stringof ~
				" because " ~
				Type.stringof ~
				".this(this) is annotated with @disable."
			)
		);

		// We can't emplace.
		static assert(
			false,
			(Type.stringof ~ " cannot be emplaced from " ~ Args[].stringof ~ '.')
		);
	}
}

/**
 * Attempts to allocate and assign a new `Ref` instance containing resource `value`, returning
 * `nullRef` if any of the required allocations fail.
 *
 * When wrapping the resource a reference counter will need to be allocated. This allocation will
 * be performed using the `Allocator` currently assigned to the `value`, falling back to the
 * default runtime `Allocator` if one is not specified.
 *
 * It is important to **never** call `makeRef` twice on the same `value`, as it will create two
 * different reference counters for that memory resource in the program.
 */
public Ref!(Type) makeRef(Type)(Type value) nothrow if (isDynamicResource!(Type)) {
	Ref!(Type) ref_ = Ref!(Type)(alloc!(size_t)(value.allocatorOf()), value);

	if (ref_.refCounter) {
		(*ref_.refCounter) = 1;

		return ref_;
	}

	return nullRef!(Type);
}

/**
 * Creates an `Optional` with `value` inside.
 */
@safe
public Optional!(Type) optional(Type)(auto ref Type value) pure nothrow {
	return Optional!(Type)(value, true);
}

/**
 * Re-allocates `source` in a new slice equal in size to `length` using `Allocator` and
 * effectively destroys `source`, resetting it to `null`.
 *
 * If `length` is smaller than `source.length` then `source` is truncated, whereas if it is greater
 * then the additional memory is added to the end as padding, making no guarantees about the
 * data after the `source` values end.
 *
 * This function takes care cleaning up `source`, and as such it is strongly advised that `dealloc`
 * not be called on `source` after using this function, as realloc may attempt to re-use the same
 * buffer starting address.
 *
 * If `allocator` is `null` then the default engine `Allocator` is used.
 *
 * If the allocator fails to allocate memory, `source.length` is `0`, or `length` is `0`, `null` is
 * returned instead.
 */
public Type[] realloc(Type)(
			const (Allocator)* allocator, ref Type[] source, size_t length) nothrow {

	scope (exit) source = null;

	if (allocator) {
		return cast(Type[])
				allocator.allocate(cast(void*)allocator, (Type.sizeof * length), source);
	}

	return cast(Type[])mallocator.allocate(null, (Type.sizeof * length), source);
}

/**
 * Swaps the value in `a` with the value in `b`.
 */
@safe
public void swap(Type)(ref Type a, ref Type b) pure nothrow {
	Type temp = a;
	a = b;
	b = temp;
}

/**
 * Writes `value` into `destination` for as many times as it will fit.
 */
public void writeMemory(Type)(Type[] destination, auto ref Type value) pure nothrow {
	Type* destPtr = destination.ptr;
	Type* destEnd = (destPtr + destination.length);

	while (destPtr != destEnd) {
		(*destPtr) = value;
		destPtr += 1;
	}
}

/**
 * Zeroes the memory in `destination`.
 */
public void zeroMemory(void[] destination) pure nothrow {
	writeMemory(cast(ubyte[])destination, 0);
}
