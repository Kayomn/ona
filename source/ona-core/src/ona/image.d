module ona.image;

private {
	import ona.os;
	import ona.math;
	import ona.memory;
	import ona.string;
}

/**
 * 32-bit true-color value with opacity.
 */
public struct Color {
	enum : Color {
		/**
		 * Black shade.
		 */
		black = rgb(0, 0, 0),

		/**
		 * White shade.
		 */
		white = rgb(Color.max, Color.max, Color.max),

		/**
		 * Red color.
		 */
		red = rgb(Color.max, 0, 0),

		/**
		 * Green color.
		 */
		green = rgb(0, Color.max, 0),

		/**
		 * Blue color.
		 */
		blue = rgb(0, 0, Color.max),

		/**
		 * Yellow color.
		 */
		yellow = rgb(Color.max, Color.max, 0),

		/**
		 * Cyan color.
		 */
		cyan = rgb(0, Color.max, Color.max),

		/**
		 * Magenta color.
		 */
		magenta = rgb(Color.max, 0, Color.max)
	}

	/**
	 * The number of supported color channel components in a `Color`.
	 */
	enum components = 4;

	/**
	 * Maximum value of a value channel.
	 */
	enum max = 255;

	union {
		struct {
			/**
			* Red, green and blue color channel values, where `0` represents absolute black and
			* `Color.max` is pure white.
			*/
			ubyte r, g, b;

			/**
			* Alpha transparency channel value, where `0` is complete invisibility and `Color.max`
			* is full opacity.
			*/
			ubyte a;
		}

		/**
		 * Raw color memory value.
		 */
		uint value;
	}

	/**
	 * Returns the color value components in red, green, blue, alpha order as a `Vec4` of `float`s
	 * normalized between `0` and `1`.
	 */
	@safe
	Vector4 normalized() pure const nothrow {
		return Vector4(
			(this.r / (cast(float)max)),
			(this.g / (cast(float)max)),
			(this.b / (cast(float)max)),
			(this.a / (cast(float)max))
		);
	}
}

/**
 * A linear collection of colored 2D points composing a visual representation on a plane.
 */
public struct Image {
	private const (Allocator)* allocator;

	private Color[] pixels;

	private Point2 dimensions;

	/**
	 * Returns the pixel dimensions of `Image`.
	 */
	@safe
	public Point2 dimensionsOf() const pure nothrow {
		return this.dimensions;
	}

	/**
	 * Returns the a slice pointing to the `Image` pixel buffer.
	 *
	 * Using this buffer, the image data may be manipulated or read.
	 */
	public inout (Color)[] pixelsOf() inout pure nothrow {
		return this.pixels;
	}

	/**
	 * Attempts to free any dynamically allocated resources and reset the `Image` instance.
	 */
	public void release() nothrow {
		dealloc(this.allocator, this.pixels);

		this.dimensions = Point2(0, 0);
	}
}

/**
 * Error codes for image data created or encoded in memory as an `Image`.
 */
public enum ImageError {
	none,
	unsupportedFormat,
	outOfMemory
}

/**
 * Statically allocates a solid-color `Image` of dimensions` size with `color`.
 */
public template imageSolid(Point2 dimensions, Color color) {
	enum Color[dimensions.x * dimensions.y] pixels = color;
	enum imageSolid = Image(null, pixels, dimensions);
}

/**
 * Image loading function signature.
 */
public alias ImageLoader = Image function(const (Allocator)*, String, ImageError*, FileReadError*);

/**
 * Attempts to create an image from the memory at `pixelPointer` of size `dimensions` using
 * `allocator`. A `null` `allocator` will use the default runtime `Allocator`.
 *
 * Should an error occur during `createImageFrom` then an invalid `Image` is returned and, if
 * `error` is not `null`, the error code is written to it.
 *
 * A `null` `pixelPointer` or `dimensions.x` and / or `dimensions.y` less than 1 pixel will result
 * in an `ImageError.unsupportedFormat` error.
 *
 * Failure to allocate the necessary memory for the `Image` pixel buffer will result in an
 * `ImageError.outOfMemory` error.
 */
pragma(mangle, "CreateImageFrom")
public extern (C) Image createImageFrom(
	const (Allocator)* allocator,
	Point2 dimensions,
	const (Color)* pixelPointer,
	ImageError* error
) nothrow {

	if (pixelPointer && (dimensions.x > 0) && (dimensions.y > 0)) {
		immutable (size_t) imageSize = (dimensions.x * dimensions.y);

		Image image = {
			allocator: allocator,
			dimensions: dimensions,
			pixels: alloc!(Color)(allocator, imageSize)
		};

		if ((copyMemory(image.pixels, pixelPointer[0 .. imageSize]) != imageSize) && error) {
			*error = ImageError.outOfMemory;
		}

		return image;
	} else if (error) {
		*error = ImageError.unsupportedFormat;
	}

	return Image();
}

/**
 * Attempts to create a solid color image of `color` with `dimensions` in memory using `allocator`.
 * A `null` `allocator` will use the default runtime `Allocator`.
 *
 * Should an error occur during `createImageSolid` then a default-initialized `Image`
 * is returned and, if `error` is not `null`, the error code is written to it.
 *
 * A `dimensions.x` and / or `dimensions.y` less than 1 pixel will result in an
 * `ImageError.unsupportedFormat` error.
 *
 * Failure to allocate the necessary memory for the `Image` pixel buffer will result in an
 * `ImageError.outOfMemory` error.
 */
pragma(mangle, "CreateImageSolid")
public extern (C) Image createImageSolid(
		const (Allocator)* allocator,
		Point2 dimensions,
		Color color,
		ImageError* error
	) nothrow {

	if ((dimensions.x > 0) && (dimensions.y > 0)) {
		Color[] pixels = alloc!(Color)(allocator, cast(size_t)(dimensions.x * dimensions.y));

		if (pixels.length) {
			writeMemory(pixels, color);

			return Image(allocator, pixels, dimensions);
		} else if (error) {
			*error = ImageError.outOfMemory;
		}
	} else if (error) {
		*error = ImageError.unsupportedFormat;
	}

	return Image();
}

/**
 * Initializes a `Color` using the 8-bit greyscale spectrum format.
 */
@safe
pragma(mangle, "Greyscale")
public extern (C) Color greyscale(ubyte value) pure nothrow {
	return Color(value, value, value, Color.max);
}

/**
 * Initializes a `Color` using the the 8-bit true-color spectrum format.
 */
@safe
pragma(mangle, "Rgb")
public extern (C) Color rgb(ubyte red, ubyte green, ubyte blue) pure nothrow {
	return Color(red, green, blue, Color.max);
}
