module ona.algorithm;

/**
 * Represents an iterable value range of unknown length of types sequence `Types`.
 */
public struct Enumerator(Types...) {
	private void delegate(size_t, void delegate(Types) nothrow) nothrow iterate;

	private size_t iterations;

	/**
	 * Constructs the `Enumerator` with `action` as the per-iteration behavior.
	 */
	public this(typeof(iterate) action) nothrow {
		this.iterate = action;
	}

	/**
	 * Iterates the enumerator once, writing the result to `values` and returning `true` if the
	 * `Enumerator` has more values to produce, otherwise `false`.
	 *
	 * ```d
	 * Enumerator!(int) myEnumerator = (i, produce) { while (i < 10) produce(cast(int)i * 10) };
	 * int value = void;
	 *
	 * while (myEnumerator.next(value)) {
	 *   // ...
	 * }
	 * ```
	 *
	 * Calling `Enumerator.next` past the enumeratable range of values is valid, but will only
	 * produce `values` equal to `Type.init` and `false` return values.
	 */
	public bool next(out Types values) nothrow {
		assert(this.iterate);

		bool hasNext;

		this.iterate(this.iterations, (Types producedValues) {
			values = producedValues;
			hasNext = true;
		});

		this.iterations += 1;

		return hasNext;
	}

	/**
	 * Allows for enumeration via `foreach` loops.
	 */
	public int opApply(int delegate(Types) nothrow loopBody) nothrow {
		Types producedValues = void;
		int loopState = 0;

		while ((loopState == 0) && this.next(producedValues)) {
			loopState = loopBody(producedValues);
		}

		return loopState;
	}
}

/**
 * Returns the product of a Cantor pairing between values `first` and `second`.
 *
 * See: https://en.wikipedia.org/wiki/Pairing_function#Cantor_pairing_function for more
 * information.
 */
public Type cantorPair(Type)(Type first, Type second) nothrow if (__traits(isIntegral, Type)) {
	return ((((first + second) * (first + second + 1)) / 2) + second);
}

/**
 * Calculate the largest of two or more values.
 */
public Types[0] max(Types...)(Types values) nothrow if (values.length > 1) {
	static if (values.length == 2) {
		return ((values[0] < values[1]) ? values[1] : values[0]);
	} else {
		Types[0] maxValue;

		static foreach (value; values) {
			maxValue = max(maxValue, value);
		}

		return maxValue;
	}
}

/**
 * Returns the smallest of two values.
 */
public pragma(inline, true) Type min(Type)(Type a, Type b) nothrow {
	return ((a < b) ? a : b);
}
