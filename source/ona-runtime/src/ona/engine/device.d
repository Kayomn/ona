module ona.engine.device;

private {
	import bindbc.sdl;
	import ona.collections;
	import ona.engine.graphics;
	import ona.os;
	import ona.math;
	import ona.memory;
	import ona.string;
}

/**
 * Opaque device state for managing the process.
 */
public struct Device {
	private ulong timeNow;

	private ulong timeLast;

	package SDL_Window* window;

	private SDL_Event event;

	package GraphicsServer graphics;

	package Rasterizer2D rasterizer2D;

	private DeviceFrame frame;

	private Appender!(Module) modules;

	private Appender!(System) systems;

	/**
	 * Releases the `Device` and all associated resources, allowing the program to cleanly exit.
	 */
	public void release() {
		unloadGraphics(this.graphics);

		if (this.window) {
			SDL_DestroyWindow(this.window);
			SDL_Quit();
		}

		foreach (ref sys; this.systems.all()) {
			sys.exitCallback(sys.userdata);
			sys.release();
		}

		this.systems.release();

		foreach (ref mod; this.modules.all()) {
			mod.release();
		}

		this.modules.release();

		this.frame = DeviceFrame.init;
	}

	/**
	 * Submits the `Device`, marking the end of a frame update and swapping the window buffers.
	 */
	public void submit() {
		this.graphics.swapper(&this);
	}

	/**
	 * Updates the `Device` state for a single frame, processing all systems currently loaded in
	 * memory using the updated state.
	 */
	public bool update() {
		SDL_Scancode scancode = void;
		this.timeLast = this.timeNow;
		this.timeNow = SDL_GetPerformanceCounter();

		frame.deltaTime = (
			(this.timeNow - this.timeLast) * (1000 / cast(float)SDL_GetPerformanceFrequency())
		);

		eventLoop: while (SDL_PollEvent(&this.event)) switch (this.event.type) {
			case SDL_QUIT: return false;

			case SDL_KEYDOWN: {
					scancode = this.event.key.keysym.scancode;
					frame.keysHeld[scancode] = true;
					frame.keysPressed[scancode] = true;

					continue eventLoop;
				}

			case SDL_KEYUP: {
					scancode = this.event.key.keysym.scancode;
					frame.keysHeld[scancode] = false;
					frame.keysReleased[scancode] = true;

					continue eventLoop;
				}

			default: continue eventLoop;
		}

		clearGraphics(this.graphics);

		foreach (system; this.systems.all()) {
			system.processCallback(system.userdata, &this.frame);
		}

		return true;
	}
}

/**
 * Cached state of the device for a single update frame.
 */
public struct DeviceFrame {
	/**
	 * Maximum number of keys supported by conventional keyboard hardware.
	 */
	public enum keys = 320;

	/**
	 * Time difference between the current update frame and the last.
	 *
	 * This value is useful for making operations that occur over a set of frames independent of
	 * the CPU clock speed.
	 */
	public float deltaTime = 0.0f;

	private bool[keys] keysHeld;

	private bool[keys] keysPressed;

	private bool[keys] keysReleased;
}

private struct Module {
	alias Callback = extern (C) void function (Device*);

	Library library;

	Callback initCallback;

	Callback exitCallback;

	void release() {
		library.release();

		this.initCallback = null;
		this.exitCallback = null;
	}
}

private struct System {
	alias InitCallback = extern (C) void function(void*, Device*);

	alias ProcessCallback = extern (C) void function(void*, DeviceFrame*);

	alias ExitCallback = extern (C) void function(void*);

	void* userdata;

	InitCallback initCallback;

	ProcessCallback processCallback;

	ExitCallback exitCallback;

	void release() {
		dealloc(null, this.userdata);

		this.initCallback = null;
		this.processCallback = null;
		this.exitCallback = null;
	}
}

/**
 * Error codes used when acquiring a `Device` instance.
 */
public enum DeviceError {
	none,
	graphics,
	alreadyExists,
	badSize
}

/**
 * Set of scancodes mapped to user-friendly keyboard key names.
 */
public enum Key {
	unknown,

	a = 4,
	b = 5,
	c = 6,
	d = 7,
	e = 8,
	f = 9,
	g = 10,
	h = 11,
	i = 12,
	j = 13,
	k = 14,
	l = 15,
	m = 16,
	n = 17,
	o = 18,
	p = 19,
	q = 20,
	r = 21,
	s = 22,
	t = 23,
	u = 24,
	v = 25,
	w = 26,
	x = 27,
	y = 28,
	z = 29,

	one = 30,
	two = 31,
	three = 32,
	four = 33,
	five = 34,
	six = 35,
	seven = 36,
	eight = 37,
	nine = 38,
	zero = 39,

	return_ = 40,
	escape = 41,
	backspace = 42,
	tab = 43,
	space = 44,

	minus = 45,
	equals = 46,
	leftBracket = 47,
	rightBracket = 48,
	backslash = 49,
	nonUsHash = 50,
	semicolon = 51,
	apostrophe = 52,
	grave = 53,
	comma = 54,
	period = 55,
	slash = 56,

	capslock = 57,

	f1 = 58,
	f2 = 59,
	f3 = 60,
	f4 = 61,
	f5 = 62,
	f6 = 63,
	f7 = 64,
	f8 = 65,
	f9 = 66,
	f10 = 67,
	f11 = 68,
	f12 = 69,

	printscreen = 70,
	scrolllock = 71,
	pause = 72,
	insert = 73,
	home = 74,
	pageup = 75,
	delete_ = 76,
	end = 77,
	pagedown = 78,
	right = 79,
	left = 80,
	down = 81,
	up = 82,

	leftControl = 224,
	leftShift = 225,
	leftAlt = 226,
	leftGui = 227,
	rightControl = 228,
	rightShift = 229,
	rightAlt = 230,
	rightGui = 231,
	mode = 257,
	audioNext = 258,
	audioPrevious = 259,
	audioStop = 260,
	audioPlay = 261,
	audioMute = 262,
	mediaSelect = 263,
	acceleratorWww = 264,
	acceleratorMail = 265,
	acceleratorCalculator = 266,
	acceleratorComputer = 267,
	acceleratorSearch = 268,
	acceleratorHome = 269,
	acceleratorBackward = 270,
	acceleratorForward = 271,
	acceleratorStop = 272,
	acceleratorRefresh = 273,
	acceleratorBookmarks = 274,

	brightnessDown = 275,
	brightnessUp = 276,
	displaySwitch = 277,
}

/**
 * Severity level of a log message.
 */
public enum LogLevel {
	notice,
	warning,
	critical
}

public alias ResourceId = uint;

/**
 * Attempts to cretae a `Device` with `title` as the application name and `size` as the display
 * dimensions in pixels. If an error occurs during initialization then an invalid `Device` and
 * writes the error to `error`, provided it is not `null`.
 *
 * While `title` allows for a `ona.string.String` of any length, `Device` titles are truncated once
 * they pass 255 characters.
 *
 * Using a `size` below the supported value on the current platform will result in a
 * `DeviceError.badSize` error.
 *
 * Calling `createDevice` while a `Device` has already been created and still exists will result in
 * a `DeviceError.alreadyExists` error.
 *
 * Failure to initialize the `Device` graphics state will result in a `DeviceError.graphics` error.
 */
public Device createDevice(String title, Point2 size, DeviceError* error) nothrow {
	enum initFlags = SDL_INIT_EVERYTHING;
	enum minSize = Point2(64, 64);

	if (!SDL_WasInit(initFlags) && (SDL_Init(initFlags) == 0)) {
		if ((size.x > minSize.x) && (size.y > minSize.y)) {
			enum windowPos = SDL_WINDOWPOS_CENTERED;
			enum windowFlags = (SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
			char[256] titleBuffer;
			Device device;

			// Fixes a bug on KDE desktops where launching the process disables the default
			// compositor.
			SDL_SetHint(SDL_HINT_VIDEO_X11_NET_WM_BYPASS_COMPOSITOR, "0");

			device.window = SDL_CreateWindow(
				sentineledBuffer(titleBuffer, title.all()).ptr,
				windowPos,
				windowPos,
				size.x,
				size.y,
				windowFlags
			);

			// TODO(Kayomn): Handle window error.
			if (device.window) {
				GraphicsError graphicsError;
				device.graphics = loadGraphics(device, &graphicsError);

				if (!graphicsError) {
					device.timeNow = SDL_GetPerformanceCounter();

					return device;
				}

				if (error) *error = DeviceError.graphics;
			}
		} else if (error) {
			*error = DeviceError.badSize;
		}
	} else if (error) {
		*error = DeviceError.alreadyExists;
	}

	return Device();
}

/**
 * Returns the width and height properties of `device` in a `Point2`.
 *
 * Modifying the property information does not alter the actual device state.
 */
pragma(mangle, "GetDeviceSize")
public extern (C) Point2 getDeviceSize(Device* device) nothrow {
	Point2 size;

	SDL_GetWindowSize(device.window, &size.x, &size.y);

	return size;
}

/**
 * Returns the title property of `device` as a `ona.string.String`.
 */
pragma(mangle, "GetDeviceTitle")
public extern (C) String getDeviceTitle(Device* device) nothrow {
	return String(strptr(SDL_GetWindowTitle(device.window)));
}

/**
 * Returns `true` if the scancode described by `key` is marked as active according to
 * `deviceFrame`, otherwise `false`.
 *
 * Because the global input state will never be actively updated at the same time as the function
 * is called, it is safe to call this without any kind of locking synchronization.
 */
pragma(mangle, "InputKeyHeld")
public extern (C) bool inputKeyHeld(DeviceFrame* deviceFrame, ushort key) {
	return deviceFrame.keysHeld[key];
}

/**
 * Returns `true` if the scancode described by `key` is marked as having been pressed according to
 * `deviceFrame`, otherwise `false`.
 *
 * Because the global input state will never be actively updated at the same time as the function
 * is called, it is safe to call this without any kind of locking synchronization.
 */
pragma(mangle, "InputKeyPressed")
public extern (C) bool inputKeyPressed(DeviceFrame* deviceFrame, ushort key) {
	return deviceFrame.keysPressed[key];
}

/**
 * Returns `true` if the scancode described by `key` is marked as having been released according to
 * `deviceFrame`, otherwise `false`.
 *
 * Because the global input state will never be actively updated at the same time as the function
 * is called, it is safe to call this without any kind of locking synchronization.
 */
pragma(mangle, "InputKeyReleased")
public extern (C) bool inputKeyReleased(DeviceFrame* deviceFrame, ushort key) {
	return deviceFrame.keysReleased[key];
}

/**
 * Attemps to load the dynamic module from the library at `moduleLibraryPath`, returning `true` if
 * it was successful, otherwise `false`.
 *
 * A module may have the following hooks, or none at all:
 *
 *   * `OnaModuleInit(Device*)` - Module initialization entry-point.
 *
 *   * `OnaModuleExit(Device*)` - Module de-initialization entry-point.
 *
 * If `device` does not exist then an assertion is raised.
 */
public bool loadDeviceModule(ref Device device, String moduleLibraryPath) {
	LibraryError error;
	Library library = openLibrary(moduleLibraryPath, &error);

	if (!error) {
		alias Func = Module.Callback;

		Module mod = {
			library: library,
			initCallback: cast(Func)searchLibrary(library, "OnaModuleInit"),
			exitCallback: cast(Func)searchLibrary(library, "OnaModuleExit")
		};

		if (mod.initCallback) {
			mod.initCallback(&device);
		}

		if (device.modules.append(mod)) {
			return true;
		}
	}

	return false;
}

/**
 * Logs `message` to the system's stdout file.
 *
 * If `level` is `LogLevel.critical` then the process will exit.
 */
public void log(LogLevel level, const (char)[][] messages...) nothrow {
	static immutable (string[]) logLevelNames = ["Notice", "Warning", "Critical"];

	writeFile(outFile, logLevelNames[level], null);
	writeFile(outFile, ": ", null);

	foreach (message; messages) {
		writeFile(outFile, message, null);
	}

	writeFile(outFile, "\n", null);
}

/**
 * Type-unsafe function for attempting to register a new system process of `userdataSize` bytes in
 * size with `initCallback`, `processCallback`, and `exitCallback` as the respective
 * initialization, process update, and de-initialization callback functions.
 *
 * In order for the function to successfully initialize the function the following conditions must
 * be met:
 *
 *   * `userdataSize` must be greater than `0`.
 *
 *   * `initCallback`, `processCallback`, and `exitCallback` must all point to non-`null` memory
 *     addresses.
 *
 * If the runtime fails to allocate memory for the system then a critical error is logged.
 *
 * If `registerDeviceSystem` is successful then `true` is returned, otherwise `false` is returned.
 *
 * If `device` does not exist then an assertion is raised.
 */
pragma(mangle, "RegisterDeviceSystem")
public extern (C) bool registerDeviceSystem(
	Device* device,
	size_t userdataSize,
	void* initCallback,
	void* processCallback,
	void* exitCallback
) {
	if (userdataSize && initCallback && processCallback && exitCallback) {
		void* userdata = allocRaw(null, userdataSize).ptr;

		if (userdata) {
			System system = {
				userdata: userdata,
				initCallback: cast(System.InitCallback)initCallback,
				processCallback: cast(System.ProcessCallback)processCallback,
				exitCallback: cast(System.ExitCallback)exitCallback
			};

			if (device.systems.append(system)) {
				system.initCallback(system.userdata, device);

				return true;
			}
		}

		log(LogLevel.critical, "Failed to allocate memory for system");

		return true;
	}

	return false;
}
