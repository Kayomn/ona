module ona.engine.graphics.opengl;

private {
	import bindbc.sdl;
	import ona.collections;
	import ona.engine.device;
	import ona.engine.graphics.common;
	import ona.math;
	import ona.memory;
	import ona.string;
	import ona.image;
}

private align (1) struct GLbitfield {
	uint value;

	public this(Values...)(Values values) {
		static foreach (value; values) {
			this.value |= value.value;
		}
	}
}

private align (1) struct GLenum {
	uint value;
}

// OpenGL 1.1
private enum GL_DEPTH_BUFFER_BIT               = GLenum(0x00000100);
private enum GL_STENCIL_BUFFER_BIT             = GLenum(0x00000400);
private enum GL_COLOR_BUFFER_BIT               = GLenum(0x00004000);
private enum GL_POINTS                         = GLenum(0x0000);
private enum GL_LINES                          = GLenum(0x0001);
private enum GL_LINE_LOOP                      = GLenum(0x0002);
private enum GL_LINE_STRIP                     = GLenum(0x0003);
private enum GL_TRIANGLES                      = GLenum(0x0004);
private enum GL_TRIANGLE_STRIP                 = GLenum(0x0005);
private enum GL_TRIANGLE_FAN                   = GLenum(0x0006);
private enum GL_NEVER                          = GLenum(0x0200);
private enum GL_LESS                           = GLenum(0x0201);
private enum GL_EQUAL                          = GLenum(0x0202);
private enum GL_LEQUAL                         = GLenum(0x0203);
private enum GL_GREATER                        = GLenum(0x0204);
private enum GL_NOTEQUAL                       = GLenum(0x0205);
private enum GL_GEQUAL                         = GLenum(0x0206);
private enum GL_ALWAYS                         = GLenum(0x0207);
private enum GL_ZERO                           = GLenum(0);
private enum GL_ONE                            = GLenum(1);
private enum GL_SRC_COLOR                      = GLenum(0x0300);
private enum GL_ONE_MINUS_SRC_COLOR            = GLenum(0x0301);
private enum GL_SRC_ALPHA                      = GLenum(0x0302);
private enum GL_ONE_MINUS_SRC_ALPHA            = GLenum(0x0303);
private enum GL_DST_ALPHA                      = GLenum(0x0304);
private enum GL_ONE_MINUS_DST_ALPHA            = GLenum(0x0305);
private enum GL_DST_COLOR                      = GLenum(0x0306);
private enum GL_ONE_MINUS_DST_COLOR            = GLenum(0x0307);
private enum GL_SRC_ALPHA_SATURATE             = GLenum(0x0308);
private enum GL_NONE                           = GLenum(0);
private enum GL_FRONT_LEFT                     = GLenum(0x0400);
private enum GL_FRONT_RIGHT                    = GLenum(0x0401);
private enum GL_BACK_LEFT                      = GLenum(0x0402);
private enum GL_BACK_RIGHT                     = GLenum(0x0403);
private enum GL_FRONT                          = GLenum(0x0404);
private enum GL_BACK                           = GLenum(0x0405);
private enum GL_LEFT                           = GLenum(0x0406);
private enum GL_RIGHT                          = GLenum(0x0407);
private enum GL_FRONT_AND_BACK                 = GLenum(0x0408);
private enum GL_NO_ERROR                       = GLenum(0);
private enum GL_INVALID_ENUM                   = GLenum(0x0500);
private enum GL_INVALID_VALUE                  = GLenum(0x0501);
private enum GL_INVALID_OPERATION              = GLenum(0x0502);
private enum GL_OUT_OF_MEMORY                  = GLenum(0x0505);
private enum GL_CW                             = GLenum(0x0900);
private enum GL_CCW                            = GLenum(0x0901);
private enum GL_POINT_SIZE                     = GLenum(0x0B11);
private enum GL_POINT_SIZE_RANGE               = GLenum(0x0B12);
private enum GL_POINT_SIZE_GRANULARITY         = GLenum(0x0B13);
private enum GL_LINE_SMOOTH                    = GLenum(0x0B20);
private enum GL_LINE_WIDTH                     = GLenum(0x0B21);
private enum GL_LINE_WIDTH_RANGE               = GLenum(0x0B22);
private enum GL_LINE_WIDTH_GRANULARITY         = GLenum(0x0B23);
private enum GL_POLYGON_MODE                   = GLenum(0x0B40);
private enum GL_POLYGON_SMOOTH                 = GLenum(0x0B41);
private enum GL_CULL_FACE                      = GLenum(0x0B44);
private enum GL_CULL_FACE_MODE                 = GLenum(0x0B45);
private enum GL_FRONT_FACE                     = GLenum(0x0B46);
private enum GL_DEPTH_RANGE                    = GLenum(0x0B70);
private enum GL_DEPTH_TEST                     = GLenum(0x0B71);
private enum GL_DEPTH_WRITEMASK                = GLenum(0x0B72);
private enum GL_DEPTH_CLEAR_VALUE              = GLenum(0x0B73);
private enum GL_DEPTH_FUNC                     = GLenum(0x0B74);
private enum GL_STENCIL_TEST                   = GLenum(0x0B90);
private enum GL_STENCIL_CLEAR_VALUE            = GLenum(0x0B91);
private enum GL_STENCIL_FUNC                   = GLenum(0x0B92);
private enum GL_STENCIL_VALUE_MASK             = GLenum(0x0B93);
private enum GL_STENCIL_FAIL                   = GLenum(0x0B94);
private enum GL_STENCIL_PASS_DEPTH_FAIL        = GLenum(0x0B95);
private enum GL_STENCIL_PASS_DEPTH_PASS        = GLenum(0x0B96);
private enum GL_STENCIL_REF                    = GLenum(0x0B97);
private enum GL_STENCIL_WRITEMASK              = GLenum(0x0B98);
private enum GL_VIEWPORT                       = GLenum(0x0BA2);
private enum GL_DITHER                         = GLenum(0x0BD0);
private enum GL_BLEND_DST                      = GLenum(0x0BE0);
private enum GL_BLEND_SRC                      = GLenum(0x0BE1);
private enum GL_BLEND                          = GLenum(0x0BE2);
private enum GL_LOGIC_OP_MODE                  = GLenum(0x0BF0);
private enum GL_COLOR_LOGIC_OP                 = GLenum(0x0BF2);
private enum GL_DRAW_BUFFER                    = GLenum(0x0C01);
private enum GL_READ_BUFFER                    = GLenum(0x0C02);
private enum GL_SCISSOR_BOX                    = GLenum(0x0C10);
private enum GL_SCISSOR_TEST                   = GLenum(0x0C11);
private enum GL_COLOR_CLEAR_VALUE              = GLenum(0x0C22);
private enum GL_COLOR_WRITEMASK                = GLenum(0x0C23);
private enum GL_DOUBLEBUFFER                   = GLenum(0x0C32);
private enum GL_STEREO                         = GLenum(0x0C33);
private enum GL_LINE_SMOOTH_HINT               = GLenum(0x0C52);
private enum GL_POLYGON_SMOOTH_HINT            = GLenum(0x0C53);
private enum GL_UNPACK_SWAP_BYTES              = GLenum(0x0CF0);
private enum GL_UNPACK_LSB_FIRST               = GLenum(0x0CF1);
private enum GL_UNPACK_ROW_LENGTH              = GLenum(0x0CF2);
private enum GL_UNPACK_SKIP_ROWS               = GLenum(0x0CF3);
private enum GL_UNPACK_SKIP_PIXELS             = GLenum(0x0CF4);
private enum GL_UNPACK_ALIGNMENT               = GLenum(0x0CF5);
private enum GL_PACK_SWAP_BYTES                = GLenum(0x0D00);
private enum GL_PACK_LSB_FIRST                 = GLenum(0x0D01);
private enum GL_PACK_ROW_LENGTH                = GLenum(0x0D02);
private enum GL_PACK_SKIP_ROWS                 = GLenum(0x0D03);
private enum GL_PACK_SKIP_PIXELS               = GLenum(0x0D04);
private enum GL_PACK_ALIGNMENT                 = GLenum(0x0D05);
private enum GL_MAX_TEXTURE_SIZE               = GLenum(0x0D33);
private enum GL_MAX_VIEWPORT_DIMS              = GLenum(0x0D3A);
private enum GL_SUBPIXEL_BITS                  = GLenum(0x0D50);
private enum GL_TEXTURE_1D                     = GLenum(0x0DE0);
private enum GL_TEXTURE_2D                     = GLenum(0x0DE1);
private enum GL_POLYGON_OFFSET_UNITS           = GLenum(0x2A00);
private enum GL_POLYGON_OFFSET_POINT           = GLenum(0x2A01);
private enum GL_POLYGON_OFFSET_LINE            = GLenum(0x2A02);
private enum GL_POLYGON_OFFSET_FILL            = GLenum(0x8037);
private enum GL_POLYGON_OFFSET_FACTOR          = GLenum(0x8038);
private enum GL_TEXTURE_BINDING_1D             = GLenum(0x8068);
private enum GL_TEXTURE_BINDING_2D             = GLenum(0x8069);
private enum GL_TEXTURE_WIDTH                  = GLenum(0x1000);
private enum GL_TEXTURE_HEIGHT                 = GLenum(0x1001);
private enum GL_TEXTURE_INTERNAL_FORMAT        = GLenum(0x1003);
private enum GL_TEXTURE_BORDER_COLOR           = GLenum(0x1004);
private enum GL_TEXTURE_RED_SIZE               = GLenum(0x805C);
private enum GL_TEXTURE_GREEN_SIZE             = GLenum(0x805D);
private enum GL_TEXTURE_BLUE_SIZE              = GLenum(0x805E);
private enum GL_TEXTURE_ALPHA_SIZE             = GLenum(0x805F);
private enum GL_DONT_CARE                      = GLenum(0x1100);
private enum GL_FASTEST                        = GLenum(0x1101);
private enum GL_NICEST                         = GLenum(0x1102);
private enum GL_BYTE                           = GLenum(0x1400);
private enum GL_UNSIGNED_BYTE                  = GLenum(0x1401);
private enum GL_SHORT                          = GLenum(0x1402);
private enum GL_UNSIGNED_SHORT                 = GLenum(0x1403);
private enum GL_INT                            = GLenum(0x1404);
private enum GL_UNSIGNED_INT                   = GLenum(0x1405);
private enum GL_FLOAT                          = GLenum(0x1406);
private enum GL_DOUBLE                         = GLenum(0x140A);
private enum GL_CLEAR                          = GLenum(0x1500);
private enum GL_AND                            = GLenum(0x1501);
private enum GL_AND_REVERSE                    = GLenum(0x1502);
private enum GL_COPY                           = GLenum(0x1503);
private enum GL_AND_INVERTED                   = GLenum(0x1504);
private enum GL_NOOP                           = GLenum(0x1505);
private enum GL_XOR                            = GLenum(0x1506);
private enum GL_OR                             = GLenum(0x1507);
private enum GL_NOR                            = GLenum(0x1508);
private enum GL_EQUIV                          = GLenum(0x1509);
private enum GL_INVERT                         = GLenum(0x150A);
private enum GL_OR_REVERSE                     = GLenum(0x150B);
private enum GL_COPY_INVERTED                  = GLenum(0x150C);
private enum GL_OR_INVERTED                    = GLenum(0x150D);
private enum GL_NAND                           = GLenum(0x150E);
private enum GL_SET                            = GLenum(0x150F);
private enum GL_TEXTURE                        = GLenum(0x1702);
private enum GL_COLOR                          = GLenum(0x1800);
private enum GL_DEPTH                          = GLenum(0x1801);
private enum GL_STENCIL                        = GLenum(0x1802);
private enum GL_STENCIL_INDEX                  = GLenum(0x1901);
private enum GL_DEPTH_COMPONENT                = GLenum(0x1902);
private enum GL_RED                            = GLenum(0x1903);
private enum GL_GREEN                          = GLenum(0x1904);
private enum GL_BLUE                           = GLenum(0x1905);
private enum GL_ALPHA                          = GLenum(0x1906);
private enum GL_RGB                            = GLenum(0x1907);
private enum GL_RGBA                           = GLenum(0x1908);
private enum GL_POINT                          = GLenum(0x1B00);
private enum GL_LINE                           = GLenum(0x1B01);
private enum GL_FILL                           = GLenum(0x1B02);
private enum GL_KEEP                           = GLenum(0x1E00);
private enum GL_REPLACE                        = GLenum(0x1E01);
private enum GL_INCR                           = GLenum(0x1E02);
private enum GL_DECR                           = GLenum(0x1E03);
private enum GL_VENDOR                         = GLenum(0x1F00);
private enum GL_RENDERER                       = GLenum(0x1F01);
private enum GL_VERSION                        = GLenum(0x1F02);
private enum GL_EXTENSIONS                     = GLenum(0x1F03);
private enum GL_NEAREST                        = GLenum(0x2600);
private enum GL_LINEAR                         = GLenum(0x2601);
private enum GL_NEAREST_MIPMAP_NEAREST         = GLenum(0x2700);
private enum GL_LINEAR_MIPMAP_NEAREST          = GLenum(0x2701);
private enum GL_NEAREST_MIPMAP_LINEAR          = GLenum(0x2702);
private enum GL_LINEAR_MIPMAP_LINEAR           = GLenum(0x2703);
private enum GL_TEXTURE_MAG_FILTER             = GLenum(0x2800);
private enum GL_TEXTURE_MIN_FILTER             = GLenum(0x2801);
private enum GL_TEXTURE_WRAP_S                 = GLenum(0x2802);
private enum GL_TEXTURE_WRAP_T                 = GLenum(0x2803);
private enum GL_PROXY_TEXTURE_1D               = GLenum(0x8063);
private enum GL_PROXY_TEXTURE_2D               = GLenum(0x8064);
private enum GL_REPEAT                         = GLenum(0x2901);
private enum GL_R3_G3_B2                       = GLenum(0x2A10);
private enum GL_RGB4                           = GLenum(0x804F);
private enum GL_RGB5                           = GLenum(0x8050);
private enum GL_RGB8                           = GLenum(0x8051);
private enum GL_RGB10                          = GLenum(0x8052);
private enum GL_RGB12                          = GLenum(0x8053);
private enum GL_RGB16                          = GLenum(0x8054);
private enum GL_RGBA2                          = GLenum(0x8055);
private enum GL_RGBA4                          = GLenum(0x8056);
private enum GL_RGB5_A1                        = GLenum(0x8057);
private enum GL_RGBA8                          = GLenum(0x8058);
private enum GL_RGB10_A2                       = GLenum(0x8059);
private enum GL_RGBA12                         = GLenum(0x805A);
private enum GL_RGBA16                         = GLenum(0x805B);
private enum GL_VERTEX_ARRAY                   = GLenum(0x8074);

private enum GL_UNSIGNED_BYTE_3_3_2            = GLenum(0x8032);
private enum GL_UNSIGNED_SHORT_4_4_4_4         = GLenum(0x8033);
private enum GL_UNSIGNED_SHORT_5_5_5_1         = GLenum(0x8034);
private enum GL_UNSIGNED_INT_8_8_8_8           = GLenum(0x8035);
private enum GL_UNSIGNED_INT_10_10_10_2        = GLenum(0x8036);
private enum GL_TEXTURE_BINDING_3D             = GLenum(0x806A);
private enum GL_PACK_SKIP_IMAGES               = GLenum(0x806B);
private enum GL_PACK_IMAGE_HEIGHT              = GLenum(0x806C);
private enum GL_UNPACK_SKIP_IMAGES             = GLenum(0x806D);
private enum GL_UNPACK_IMAGE_HEIGHT            = GLenum(0x806E);
private enum GL_TEXTURE_3D                     = GLenum(0x806F);
private enum GL_PROXY_TEXTURE_3D               = GLenum(0x8070);
private enum GL_TEXTURE_DEPTH                  = GLenum(0x8071);
private enum GL_TEXTURE_WRAP_R                 = GLenum(0x8072);
private enum GL_MAX_3D_TEXTURE_SIZE            = GLenum(0x8073);
private enum GL_UNSIGNED_BYTE_2_3_3_REV        = GLenum(0x8362);
private enum GL_UNSIGNED_SHORT_5_6_5           = GLenum(0x8363);
private enum GL_UNSIGNED_SHORT_5_6_5_REV       = GLenum(0x8364);
private enum GL_UNSIGNED_SHORT_4_4_4_4_REV     = GLenum(0x8365);
private enum GL_UNSIGNED_SHORT_1_5_5_5_REV     = GLenum(0x8366);
private enum GL_UNSIGNED_INT_8_8_8_8_REV       = GLenum(0x8367);
private enum GL_UNSIGNED_INT_2_10_10_10_REV    = GLenum(0x8368);
private enum GL_BGR                            = GLenum(0x80E0);
private enum GL_BGRA                           = GLenum(0x80E1);
private enum GL_MAX_ELEMENTS_VERTICES          = GLenum(0x80E8);
private enum GL_MAX_ELEMENTS_INDICES           = GLenum(0x80E9);
private enum GL_CLAMP_TO_EDGE                  = GLenum(0x812F);
private enum GL_TEXTURE_MIN_LOD                = GLenum(0x813A);
private enum GL_TEXTURE_MAX_LOD                = GLenum(0x813B);
private enum GL_TEXTURE_BASE_LEVEL             = GLenum(0x813C);
private enum GL_TEXTURE_MAX_LEVEL              = GLenum(0x813D);
private enum GL_SMOOTH_POINT_SIZE_RANGE        = GLenum(0x0B12);
private enum GL_SMOOTH_POINT_SIZE_GRANULARITY  = GLenum(0x0B13);
private enum GL_SMOOTH_LINE_WIDTH_RANGE        = GLenum(0x0B22);
private enum GL_SMOOTH_LINE_WIDTH_GRANULARITY  = GLenum(0x0B23);
private enum GL_ALIASED_LINE_WIDTH_RANGE       = GLenum(0x846E);

// OpenGL 1.5
private enum GL_BUFFER_SIZE                    = GLenum(0x8764);
private enum GL_BUFFER_USAGE                   = GLenum(0x8765);
private enum GL_QUERY_COUNTER_BITS             = GLenum(0x8864);
private enum GL_CURRENT_QUERY                  = GLenum(0x8865);
private enum GL_QUERY_RESULT                   = GLenum(0x8866);
private enum GL_QUERY_RESULT_AVAILABLE         = GLenum(0x8867);
private enum GL_ARRAY_BUFFER                   = GLenum(0x8892);
private enum GL_ELEMENT_ARRAY_BUFFER           = GLenum(0x8893);
private enum GL_ARRAY_BUFFER_BINDING           = GLenum(0x8894);
private enum GL_ELEMENT_ARRAY_BUFFER_BINDING   = GLenum(0x8895);
private enum GL_VERTEX_ATTRIB_ARRAY_BUFFER_BINDING = GLenum(0x889F);
private enum GL_READ_ONLY                      = GLenum(0x88B8);
private enum GL_WRITE_ONLY                     = GLenum(0x88B9);
private enum GL_READ_WRITE                     = GLenum(0x88BA);
private enum GL_BUFFER_ACCESS                  = GLenum(0x88BB);
private enum GL_BUFFER_MAPPED                  = GLenum(0x88BC);
private enum GL_BUFFER_MAP_POINTER             = GLenum(0x88BD);
private enum GL_STREAM_DRAW                    = GLenum(0x88E0);
private enum GL_STREAM_READ                    = GLenum(0x88E1);
private enum GL_STREAM_COPY                    = GLenum(0x88E2);
private enum GL_STATIC_DRAW                    = GLenum(0x88E4);
private enum GL_STATIC_READ                    = GLenum(0x88E5);
private enum GL_STATIC_COPY                    = GLenum(0x88E6);
private enum GL_DYNAMIC_DRAW                   = GLenum(0x88E8);
private enum GL_DYNAMIC_READ                   = GLenum(0x88E9);
private enum GL_DYNAMIC_COPY                   = GLenum(0x88EA);
private enum GL_SAMPLES_PASSED                 = GLenum(0x8914);
private enum GL_SRC1_ALPHA                     = GLenum(0x8589);

// OpenGL 2.0
private enum GL_BLEND_EQUATION_RGB             = GLenum(0x8009);
private enum GL_VERTEX_ATTRIB_ARRAY_ENABLED    = GLenum(0x8622);
private enum GL_VERTEX_ATTRIB_ARRAY_SIZE       = GLenum(0x8623);
private enum GL_VERTEX_ATTRIB_ARRAY_STRIDE     = GLenum(0x8624);
private enum GL_VERTEX_ATTRIB_ARRAY_TYPE       = GLenum(0x8625);
private enum GL_CURRENT_VERTEX_ATTRIB          = GLenum(0x8626);
private enum GL_VERTEX_PROGRAM_POINT_SIZE      = GLenum(0x8642);
private enum GL_VERTEX_ATTRIB_ARRAY_POINTER    = GLenum(0x8645);
private enum GL_STENCIL_BACK_FUNC              = GLenum(0x8800);
private enum GL_STENCIL_BACK_FAIL              = GLenum(0x8801);
private enum GL_STENCIL_BACK_PASS_DEPTH_FAIL   = GLenum(0x8802);
private enum GL_STENCIL_BACK_PASS_DEPTH_PASS   = GLenum(0x8803);
private enum GL_MAX_DRAW_BUFFERS               = GLenum(0x8824);
private enum GL_DRAW_BUFFER0                   = GLenum(0x8825);
private enum GL_DRAW_BUFFER1                   = GLenum(0x8826);
private enum GL_DRAW_BUFFER2                   = GLenum(0x8827);
private enum GL_DRAW_BUFFER3                   = GLenum(0x8828);
private enum GL_DRAW_BUFFER4                   = GLenum(0x8829);
private enum GL_DRAW_BUFFER5                   = GLenum(0x882A);
private enum GL_DRAW_BUFFER6                   = GLenum(0x882B);
private enum GL_DRAW_BUFFER7                   = GLenum(0x882C);
private enum GL_DRAW_BUFFER8                   = GLenum(0x882D);
private enum GL_DRAW_BUFFER9                   = GLenum(0x882E);
private enum GL_DRAW_BUFFER10                  = GLenum(0x882F);
private enum GL_DRAW_BUFFER11                  = GLenum(0x8830);
private enum GL_DRAW_BUFFER12                  = GLenum(0x8831);
private enum GL_DRAW_BUFFER13                  = GLenum(0x8832);
private enum GL_DRAW_BUFFER14                  = GLenum(0x8833);
private enum GL_DRAW_BUFFER15                  = GLenum(0x8834);
private enum GL_BLEND_EQUATION_ALPHA           = GLenum(0x883D);
private enum GL_MAX_VERTEX_ATTRIBS             = GLenum(0x8869);
private enum GL_VERTEX_ATTRIB_ARRAY_NORMALIZED = GLenum(0x886A);
private enum GL_MAX_TEXTURE_IMAGE_UNITS        = GLenum(0x8872);
private enum GL_FRAGMENT_SHADER                = GLenum(0x8B30);
private enum GL_VERTEX_SHADER                  = GLenum(0x8B31);
private enum GL_MAX_FRAGMENT_UNIFORM_COMPONENTS = GLenum(0x8B49);
private enum GL_MAX_VERTEX_UNIFORM_COMPONENTS  = GLenum(0x8B4A);
private enum GL_MAX_VARYING_FLOATS             = GLenum(0x8B4B);
private enum GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS = GLenum(0x8B4C);
private enum GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS = GLenum(0x8B4D);
private enum GL_SHADER_TYPE                    = GLenum(0x8B4F);
private enum GL_FLOAT_VEC2                     = GLenum(0x8B50);
private enum GL_FLOAT_VEC3                     = GLenum(0x8B51);
private enum GL_FLOAT_VEC4                     = GLenum(0x8B52);
private enum GL_INT_VEC2                       = GLenum(0x8B53);
private enum GL_INT_VEC3                       = GLenum(0x8B54);
private enum GL_INT_VEC4                       = GLenum(0x8B55);
private enum GL_BOOL                           = GLenum(0x8B56);
private enum GL_BOOL_VEC2                      = GLenum(0x8B57);
private enum GL_BOOL_VEC3                      = GLenum(0x8B58);
private enum GL_BOOL_VEC4                      = GLenum(0x8B59);
private enum GL_FLOAT_MAT2                     = GLenum(0x8B5A);
private enum GL_FLOAT_MAT3                     = GLenum(0x8B5B);
private enum GL_FLOAT_MAT4                     = GLenum(0x8B5C);
private enum GL_SAMPLER_1D                     = GLenum(0x8B5D);
private enum GL_SAMPLER_2D                     = GLenum(0x8B5E);
private enum GL_SAMPLER_3D                     = GLenum(0x8B5F);
private enum GL_SAMPLER_CUBE                   = GLenum(0x8B60);
private enum GL_SAMPLER_1D_SHADOW              = GLenum(0x8B61);
private enum GL_SAMPLER_2D_SHADOW              = GLenum(0x8B62);
private enum GL_DELETE_STATUS                  = GLenum(0x8B80);
private enum GL_COMPILE_STATUS                 = GLenum(0x8B81);
private enum GL_LINK_STATUS                    = GLenum(0x8B82);
private enum GL_VALIDATE_STATUS                = GLenum(0x8B83);
private enum GL_INFO_LOG_LENGTH                = GLenum(0x8B84);
private enum GL_ATTACHED_SHADERS               = GLenum(0x8B85);
private enum GL_ACTIVE_UNIFORMS                = GLenum(0x8B86);
private enum GL_ACTIVE_UNIFORM_MAX_LENGTH      = GLenum(0x8B87);
private enum GL_SHADER_SOURCE_LENGTH           = GLenum(0x8B88);
private enum GL_ACTIVE_ATTRIBUTES              = GLenum(0x8B89);
private enum GL_ACTIVE_ATTRIBUTE_MAX_LENGTH    = GLenum(0x8B8A);
private enum GL_FRAGMENT_SHADER_DERIVATIVE_HINT = GLenum(0x8B8B);
private enum GL_SHADING_LANGUAGE_VERSION       = GLenum(0x8B8C);
private enum GL_CURRENT_PROGRAM                = GLenum(0x8B8D);
private enum GL_POINT_SPRITE_COORD_ORIGIN      = GLenum(0x8CA0);
private enum GL_LOWER_LEFT                     = GLenum(0x8CA1);
private enum GL_UPPER_LEFT                     = GLenum(0x8CA2);
private enum GL_STENCIL_BACK_REF               = GLenum(0x8CA3);
private enum GL_STENCIL_BACK_VALUE_MASK        = GLenum(0x8CA4);
private enum GL_STENCIL_BACK_WRITEMASK         = GLenum(0x8CA5);

// ARB_uniform_buffer_object 3.1
private enum GL_UNIFORM_BUFFER                 = GLenum(0x8A11);
private enum GL_UNIFORM_BUFFER_BINDING         = GLenum(0x8A28);
private enum GL_UNIFORM_BUFFER_START           = GLenum(0x8A29);
private enum GL_UNIFORM_BUFFER_SIZE            = GLenum(0x8A2A);
private enum GL_MAX_VERTEX_UNIFORM_BLOCKS      = GLenum(0x8A2B);
private enum GL_MAX_GEOMETRY_UNIFORM_BLOCKS    = GLenum(0x8A2C);
private enum GL_MAX_FRAGMENT_UNIFORM_BLOCKS    = GLenum(0x8A2D);
private enum GL_MAX_COMBINED_UNIFORM_BLOCKS    = GLenum(0x8A2E);
private enum GL_MAX_UNIFORM_BUFFER_BINDINGS    = GLenum(0x8A2F);
private enum GL_MAX_UNIFORM_BLOCK_SIZE         = GLenum(0x8A30);
private enum GL_MAX_COMBINED_VERTEX_UNIFORM_COMPONENTS = GLenum(0x8A31);
private enum GL_MAX_COMBINED_GEOMETRY_UNIFORM_COMPONENTS = GLenum(0x8A32);
private enum GL_MAX_COMBINED_FRAGMENT_UNIFORM_COMPONENTS = GLenum(0x8A33);
private enum GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT = GLenum(0x8A34);
private enum GL_ACTIVE_UNIFORM_BLOCK_MAX_NAME_LENGTH = GLenum(0x8A35);
private enum GL_ACTIVE_UNIFORM_BLOCKS          = GLenum(0x8A36);
private enum GL_UNIFORM_TYPE                   = GLenum(0x8A37);
private enum GL_UNIFORM_SIZE                   = GLenum(0x8A38);
private enum GL_UNIFORM_NAME_LENGTH            = GLenum(0x8A39);
private enum GL_UNIFORM_BLOCK_INDEX            = GLenum(0x8A3A);
private enum GL_UNIFORM_OFFSET                 = GLenum(0x8A3B);
private enum GL_UNIFORM_ARRAY_STRIDE           = GLenum(0x8A3C);
private enum GL_UNIFORM_MATRIX_STRIDE          = GLenum(0x8A3D);
private enum GL_UNIFORM_IS_ROW_MAJOR           = GLenum(0x8A3E);
private enum GL_UNIFORM_BLOCK_BINDING          = GLenum(0x8A3F);
private enum GL_UNIFORM_BLOCK_DATA_SIZE        = GLenum(0x8A40);
private enum GL_UNIFORM_BLOCK_NAME_LENGTH      = GLenum(0x8A41);
private enum GL_UNIFORM_BLOCK_ACTIVE_UNIFORMS  = GLenum(0x8A42);
private enum GL_UNIFORM_BLOCK_ACTIVE_UNIFORM_INDICES = GLenum(0x8A43);
private enum GL_UNIFORM_BLOCK_REFERENCED_BY_VERTEX_SHADER = GLenum(0x8A44);
private enum GL_UNIFORM_BLOCK_REFERENCED_BY_GEOMETRY_SHADER = GLenum(0x8A45);
private enum GL_UNIFORM_BLOCK_REFERENCED_BY_FRAGMENT_SHADER = GLenum(0x8A46);
private enum GL_INVALID_INDEX                  = GLenum(0xFFFFFFFFu);

// ARB_buffer_storage 4.4
private enum GL_MAP_PERSISTENT_BIT             = GLenum(0x0040);
private enum GL_MAP_COHERENT_BIT               = GLenum(0x0080);
private enum GL_DYNAMIC_STORAGE_BIT            = GLenum(0x0100);
private enum GL_CLIENT_STORAGE_BIT             = GLenum(0x0200);
private enum GL_CLIENT_MAPPED_BUFFER_BARRIER_BIT = GLenum(0x00004000);
private enum GL_BUFFER_IMMUTABLE_STORAGE       = GLenum(0x821F);
private enum GL_BUFFER_STORAGE_FLAGS           = GLenum(0x8220);

// ARB_framebuffer_object 3.0
private enum GL_INVALID_FRAMEBUFFER_OPERATION  = GLenum(0x0506);
private enum GL_FRAMEBUFFER_ATTACHMENT_COLOR_ENCODING = GLenum(0x8210);
private enum GL_FRAMEBUFFER_ATTACHMENT_COMPONENT_TYPE = GLenum(0x8211);
private enum GL_FRAMEBUFFER_ATTACHMENT_RED_SIZE = GLenum(0x8212);
private enum GL_FRAMEBUFFER_ATTACHMENT_GREEN_SIZE = GLenum(0x8213);
private enum GL_FRAMEBUFFER_ATTACHMENT_BLUE_SIZE = GLenum(0x8214);
private enum GL_FRAMEBUFFER_ATTACHMENT_ALPHA_SIZE = GLenum(0x8215);
private enum GL_FRAMEBUFFER_ATTACHMENT_DEPTH_SIZE = GLenum(0x8216);
private enum GL_FRAMEBUFFER_ATTACHMENT_STENCIL_SIZE = GLenum(0x8217);
private enum GL_FRAMEBUFFER_DEFAULT            = GLenum(0x8218);
private enum GL_FRAMEBUFFER_UNDEFINED          = GLenum(0x8219);
private enum GL_DEPTH_STENCIL_ATTACHMENT       = GLenum(0x821A);
private enum GL_MAX_RENDERBUFFER_SIZE          = GLenum(0x84E8);
private enum GL_DEPTH_STENCIL                  = GLenum(0x84F9);
private enum GL_UNSIGNED_INT_24_8              = GLenum(0x84FA);
private enum GL_DEPTH24_STENCIL8               = GLenum(0x88F0);
private enum GL_TEXTURE_STENCIL_SIZE           = GLenum(0x88F1);
private enum GL_TEXTURE_RED_TYPE               = GLenum(0x8C10);
private enum GL_TEXTURE_GREEN_TYPE             = GLenum(0x8C11);
private enum GL_TEXTURE_BLUE_TYPE              = GLenum(0x8C12);
private enum GL_TEXTURE_ALPHA_TYPE             = GLenum(0x8C13);
private enum GL_TEXTURE_DEPTH_TYPE             = GLenum(0x8C16);
private enum GL_UNSIGNED_NORMALIZED            = GLenum(0x8C17);
private enum GL_FRAMEBUFFER_BINDING            = GLenum(0x8CA6);
private enum GL_DRAW_FRAMEBUFFER_BINDING       = GL_FRAMEBUFFER_BINDING;
private enum GL_RENDERBUFFER_BINDING           = GLenum(0x8CA7);
private enum GL_READ_FRAMEBUFFER               = GLenum(0x8CA8);
private enum GL_DRAW_FRAMEBUFFER               = GLenum(0x8CA9);
private enum GL_READ_FRAMEBUFFER_BINDING       = GLenum(0x8CAA);
private enum GL_RENDERBUFFER_SAMPLES           = GLenum(0x8CAB);
private enum GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE = GLenum(0x8CD0);
private enum GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME = GLenum(0x8CD1);
private enum GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LEVEL = GLenum(0x8CD2);
private enum GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE = GLenum(0x8CD3);
private enum GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LAYER = GLenum(0x8CD4);
private enum GL_FRAMEBUFFER_COMPLETE           = GLenum(0x8CD5);
private enum GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT = GLenum(0x8CD6);
private enum GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT = GLenum(0x8CD7);
private enum GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER = GLenum(0x8CDB);
private enum GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER = GLenum(0x8CDC);
private enum GL_FRAMEBUFFER_UNSUPPORTED        = GLenum(0x8CDD);
private enum GL_MAX_COLOR_ATTACHMENTS          = GLenum(0x8CDF);
private enum GL_COLOR_ATTACHMENT0              = GLenum(0x8CE0);
private enum GL_COLOR_ATTACHMENT1              = GLenum(0x8CE1);
private enum GL_COLOR_ATTACHMENT2              = GLenum(0x8CE2);
private enum GL_COLOR_ATTACHMENT3              = GLenum(0x8CE3);
private enum GL_COLOR_ATTACHMENT4              = GLenum(0x8CE4);
private enum GL_COLOR_ATTACHMENT5              = GLenum(0x8CE5);
private enum GL_COLOR_ATTACHMENT6              = GLenum(0x8CE6);
private enum GL_COLOR_ATTACHMENT7              = GLenum(0x8CE7);
private enum GL_COLOR_ATTACHMENT8              = GLenum(0x8CE8);
private enum GL_COLOR_ATTACHMENT9              = GLenum(0x8CE9);
private enum GL_COLOR_ATTACHMENT10             = GLenum(0x8CEA);
private enum GL_COLOR_ATTACHMENT11             = GLenum(0x8CEB);
private enum GL_COLOR_ATTACHMENT12             = GLenum(0x8CEC);
private enum GL_COLOR_ATTACHMENT13             = GLenum(0x8CED);
private enum GL_COLOR_ATTACHMENT14             = GLenum(0x8CEE);
private enum GL_COLOR_ATTACHMENT15             = GLenum(0x8CEF);
private enum GL_DEPTH_ATTACHMENT               = GLenum(0x8D00);
private enum GL_STENCIL_ATTACHMENT             = GLenum(0x8D20);
private enum GL_FRAMEBUFFER                    = GLenum(0x8D40);
private enum GL_RENDERBUFFER                   = GLenum(0x8D41);
private enum GL_RENDERBUFFER_WIDTH             = GLenum(0x8D42);
private enum GL_RENDERBUFFER_HEIGHT            = GLenum(0x8D43);
private enum GL_RENDERBUFFER_INTERNAL_FORMAT   = GLenum(0x8D44);
private enum GL_STENCIL_INDEX1                 = GLenum(0x8D46);
private enum GL_STENCIL_INDEX4                 = GLenum(0x8D47);
private enum GL_STENCIL_INDEX8                 = GLenum(0x8D48);
private enum GL_STENCIL_INDEX16                = GLenum(0x8D49);
private enum GL_RENDERBUFFER_RED_SIZE          = GLenum(0x8D50);
private enum GL_RENDERBUFFER_GREEN_SIZE        = GLenum(0x8D51);
private enum GL_RENDERBUFFER_BLUE_SIZE         = GLenum(0x8D52);
private enum GL_RENDERBUFFER_ALPHA_SIZE        = GLenum(0x8D53);
private enum GL_RENDERBUFFER_DEPTH_SIZE        = GLenum(0x8D54);
private enum GL_RENDERBUFFER_STENCIL_SIZE      = GLenum(0x8D55);
private enum GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE = GLenum(0x8D56);
private enum GL_MAX_SAMPLES                    = GLenum(0x8D57);

/**
 * Techniques understood by the API for connecting vertices together when rendering.
 */
public enum VertexTopology {
	triangles = GL_TRIANGLES.value
}

private enum bind;

@bind
private static void function(GLenum) nothrow glEnable;

@bind
private static void function(int, int, int, int) nothrow glViewport;

@bind
private static int function(uint, const (char)*) nothrow glGetUniformLocation;

@bind
private static extern (C) void function(uint, int, int) nothrow glProgramUniform1i;

@bind
private static extern (C) uint function(uint, const (char)*) nothrow glGetUniformBlockIndex;

@bind
private static extern (C) void function(uint, uint, uint) nothrow glUniformBlockBinding;

@bind
private static extern (C) void function(uint) nothrow glDeleteProgram;

@bind
private static extern (C) void function(int, uint*) nothrow glDeleteBuffers;

@bind
private static extern (C) void function(int, uint*) nothrow glDeleteVertexArrays;

@bind
private static extern (C) void function(int, uint*) nothrow glDeleteTextures;

@bind
private static extern (C) void* function(uint, GLenum) nothrow glMapNamedBuffer;

@bind
private static extern (C) bool function(uint) nothrow glUnmapNamedBuffer;

@bind
private static extern (C) void function(uint) nothrow glUseProgram;

@bind
private static extern (C) void function(uint, uint) nothrow glBindTextureUnit;

@bind
private static extern (C) void function(GLenum, uint, uint) nothrow glBindBufferBase;

@bind
private static extern (C) void function(float, float, float, float) nothrow glClearColor;

@bind
private static extern (C) void function(GLbitfield) nothrow glClear;

@bind
private static extern (C) void function(int, uint*) nothrow glCreateBuffers;

@bind
private static extern (C) void function(uint, ptrdiff_t, const (void)*, GLbitfield) nothrow glNamedBufferStorage;

@bind
private static extern (C) void function(int, uint*) nothrow glCreateVertexArrays;

@bind
private static extern (C) void function(uint, uint, uint, ptrdiff_t, int) nothrow glVertexArrayVertexBuffer;

@bind
private static extern (C) void function(uint, uint) nothrow glEnableVertexArrayAttrib;

@bind
private static extern (C) void function(uint, uint, int, GLenum, bool, uint) nothrow glVertexArrayAttribFormat;

@bind
private static extern (C) void function(uint, uint, uint) nothrow glVertexArrayAttribBinding;

@bind
private static extern (C) uint function(GLenum) nothrow glCreateShader;

@bind
private static extern (C) void function(uint, int, const (char)**, const (int)*) nothrow glShaderSource;

@bind
private static extern (C) void function(uint) nothrow glCompileShader;

@bind
private static extern (C) void function(uint, GLenum, int*) nothrow glGetShaderiv;

@bind
private static extern (C) void function(uint, int, int*, char*) nothrow glGetShaderInfoLog;

@bind
private static extern (C) void function(uint) nothrow glDeleteShader;

@bind
private static extern (C) uint function() nothrow glCreateProgram;

@bind
private static extern (C) void function(uint, uint) nothrow glAttachShader;

@bind
private static extern (C) void function(uint) nothrow glLinkProgram;

@bind
private static extern (C) void function(uint, uint) nothrow glDetachShader;

@bind
private static extern (C) void function(uint, GLenum, int*) nothrow glGetProgramiv;

@bind
private static extern (C) void function(uint) nothrow glValidateProgram;

@bind
private static extern (C) void function(GLenum, int, uint*) nothrow glCreateTextures;

@bind
private static extern (C) void function(uint, int, GLenum, int, int) nothrow glTextureStorage2D;

@bind
private static extern (C) void function(
		uint, int, int, int, int, int, GLenum, GLenum, const (void)*) nothrow glTextureSubImage2D;

@bind
private static extern (C) void function(uint, GLenum, int) nothrow glTextureParameteri;

@bind
private static extern (C) void function(
		uint, ptrdiff_t, const (void)*, GLenum) nothrow glNamedBufferData;

@bind
private static extern (C) void function(GLenum, uint) nothrow glBindBuffer;

@bind
private static extern (C) void function(uint) nothrow glBindVertexArray;

@bind
private static extern (C) void function(GLenum, int, int) nothrow glDrawArrays;

@bind
private static extern (C) void function(GLenum, int, int, int) nothrow glDrawArraysInstanced;

@bind
private static extern (C) GLenum function() nothrow glGetError;

public GraphicsServer loadGraphics(ref Device device, GraphicsError* error) nothrow {
	static struct MaterialInstance {
		ubyte[] uniformData;

		uint textureId;

		ResourceId shaderId;
	}

	static struct PrimitiveInstance {
		uint bufferId;

		uint vertexArrayId;

		uint vertexCount;

		VertexTopology topology;
	}

	static struct OpenGlStorage {
		Appender!(MaterialInstance) materialInstances;

		Appender!(PrimitiveInstance) primitiveInstances;
	}

	static struct SdlAttribute {
		SDL_GLattr id;

		int value;
	}

	static extern (C) SwapInterval setSwapInterval(SwapInterval swapInterval) {
		final switch (swapInterval) with (SwapInterval) {
			case immediate: {
				// Honestly, if the hardware doesn't support immediate redraws then it probably
				// doesn't even have a video card.
				if (SDL_GL_SetSwapInterval(0) == 0) {
					return immediate;
				}

				log(
					LogLevel.critical,
					"Immediate interval swapping is not supported by this device"
				);

				return immediate;
			}

			case fixed: {
				// Try for fixed vsync, but if it can't be acquired then immediate redraws will
				// have to do.
				if (SDL_GL_SetSwapInterval(1) == 0) {
					return fixed;
				}

				log(LogLevel.notice, "Fixed interval swapping is not supported by this device");
				log(LogLevel.notice, "Attempting to use immediate interval swapping instead...");

				return setSwapInterval(immediate);
			}

			case adaptive: {
				// Try for adaptive vsync, but if it can't be acquired then fixed vsync is fine.
				if (SDL_GL_SetSwapInterval(-1) == 0) {
					return adaptive;
				}

				log(LogLevel.notice, "Adaptive interval swapping is not supported by this device");
				log(LogLevel.notice, "Attempting to use fixed interval swapping instead...");

				return setSwapInterval(fixed);
			}
		}
	}

	static extern (C) void swap(Device* device) nothrow {
		SDL_GL_SwapWindow(device.window);
	}

	static extern (C) void clear() nothrow {
		glClearColor(0f, 0f, 0f, 0f);
		glClear(GLbitfield(GL_COLOR_BUFFER_BIT, GL_DEPTH_BUFFER_BIT));
	}

	static extern (C) void clearColor(Color color) nothrow {
		immutable (Vector4) rgba = color.normalized();

		glClearColor(rgba.x, rgba.y, rgba.z, rgba.w);
		glClear(GLbitfield(GL_COLOR_BUFFER_BIT, GL_DEPTH_BUFFER_BIT));
	}

	static extern (C) void dispatchCommands(void* storage, GraphicsCommands* commands) {
		// TODO(Kayomn): Implement graphics command dispatcher.
	}

	static extern (C) ResourceId newMaterial(
		void* storage,
		const (MaterialProperties) layout,
		void[] propertyData,
		Image texture,
		ResourceId shaderId,
		MaterialError* error
	) nothrow {
		if (layout.validate(propertyData)) {
			ubyte[] uniformData = alloc!(ubyte)(null, propertyData.length);

			if (copyMemory(uniformData, propertyData) == propertyData.length) {
				immutable (Point2) textureSize = texture.dimensionsOf();
				uint textureHandle;

				glCreateTextures(GL_TEXTURE_2D, 1, &textureHandle);
				glTextureStorage2D(textureHandle, 1, GL_RGBA8, textureSize.x, textureSize.y);

				glErrorCheck: switch (glGetError().value) {
					case GL_NO_ERROR.value: {
						Color[] texturePixels = texture.pixelsOf();

						if (texturePixels) {
							glTextureSubImage2D(
								textureHandle,
								0,
								0,
								0,
								textureSize.x,
								textureSize.y,
								GL_RGBA,
								GL_UNSIGNED_BYTE,
								texturePixels.ptr
							);

							if (glGetError() == GL_NO_ERROR) {
								glTextureParameteri(
									textureHandle,
									GL_TEXTURE_MIN_FILTER,
									GL_LINEAR.value
								);

								if (glGetError() != GL_NO_ERROR) {
									glTextureParameteri(
										textureHandle,
										GL_TEXTURE_MAG_FILTER,
										GL_LINEAR.value
									);

									if (glGetError() != GL_NO_ERROR) {
										glTextureParameteri(
											textureHandle,
											GL_TEXTURE_WRAP_S,
											GL_CLAMP_TO_EDGE.value
										);

										if (glGetError() != GL_NO_ERROR) {
											glTextureParameteri(
												textureHandle,
												GL_TEXTURE_WRAP_T,
												GL_CLAMP_TO_EDGE.value
											);

											if (glGetError() != GL_NO_ERROR) {
												Appender!(MaterialInstance)* instances =
														&(cast(OpenGlStorage*)storage).
														materialInstances;

												immutable id = cast(ResourceId)instances.count();

												if (instances.append(MaterialInstance(
													uniformData,
													textureHandle,
													shaderId
												))) {
													return id;
												}

												if (error) *error = MaterialError.outOfResources;
											}
										}
									}
								}
							}

							if (error) *error = MaterialError.graphics;
						} else if (error) {
							*error = MaterialError.badImage;
						}
					} break glErrorCheck;

					case GL_INVALID_VALUE.value: {
						if (error) *error = MaterialError.badImage;
						// Invalid size.
					} break glErrorCheck;

					default: {
						if (error) *error = MaterialError.graphics;
					} break glErrorCheck;
				}
			}

			if (error) *error = MaterialError.outOfResources;
		} else if (error) {
			*error = MaterialError.badProperties;
		}

		return 0;
	}

	static extern (C) ResourceId newPrimitive(
		void* storage,
		const (VertexLayout) vertexLayout,
		scope const (void[]) vertexData,
		PrimitiveError* error
	) nothrow {
		static GLenum descriptorToGl(TypeDescriptor descriptor) {
			final switch (descriptor) with (TypeDescriptor) {
				case byte_: return GL_BYTE;
				case ubyte_: return GL_UNSIGNED_BYTE;
				case short_: return GL_SHORT;
				case ushort_: return GL_UNSIGNED_SHORT;
				case int_: return GL_INT;
				case uint_: return GL_UNSIGNED_INT;
				case float_: return GL_FLOAT;
				case double_: return GL_DOUBLE;
			}
		}

		if (vertexLayout.validate(vertexData)) {
			Appender!(PrimitiveInstance)* instances =
					&(cast(OpenGlStorage*)storage).primitiveInstances;

			immutable id = cast(ResourceId)instances.count();
			PrimitiveInstance* instance = instances.append(PrimitiveInstance());

			if (instance) {
				glCreateBuffers(1, &instance.bufferId);

				glNamedBufferStorage(
					instance.bufferId,
					vertexData.length,
					vertexData.ptr,
					GLbitfield(GL_DYNAMIC_STORAGE_BIT)
				);

				glErrorCheck: switch (glGetError().value) {
					case GL_NO_ERROR.value: {
						instance.vertexCount =
								cast(uint)(vertexData.length / vertexLayout.vertexSize);

						glCreateVertexArrays(1, &instance.vertexArrayId);

						glVertexArrayVertexBuffer(
							instance.vertexArrayId,
							0,
							instance.bufferId,
							0,
							cast(uint)vertexLayout.vertexSize
						);

						foreach (ref vertexAttribute; vertexLayout.attributes) {
							glEnableVertexArrayAttrib(
								instance.vertexArrayId,
								vertexAttribute.location
							);

							glVertexArrayAttribFormat(
								instance.vertexArrayId,
								vertexAttribute.location,
								vertexAttribute.components,
								descriptorToGl(vertexAttribute.type),
								false,
								vertexAttribute.offset
							);

							glVertexArrayAttribBinding(
								instance.vertexArrayId,
								vertexAttribute.location,
								0
							);
						}

						return id;
					}

					case GL_OUT_OF_MEMORY.value: {
						if (error) *error = PrimitiveError.outOfResources;
					} break glErrorCheck;

					default: {
						if (error) *error = PrimitiveError.badVertices;
					} break glErrorCheck;
				}

				instances.truncate(1);
			} else if (error) {
				*error = PrimitiveError.outOfResources;
			}
		} else if (error) {
			*error = PrimitiveError.badVertices;
		}

		return 0;
	}

	static immutable (SdlAttribute[]) attributes = [
		SdlAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4),
		SdlAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3),
		SdlAttribute(SDL_GL_DOUBLEBUFFER, 1),
		SdlAttribute(SDL_GL_DEPTH_SIZE, 24)
	];

	static OpenGlStorage storage;

	foreach (attribute; attributes) {
		if (SDL_GL_SetAttribute(attribute.id, attribute.value) != 0) {
			if (error) *error = GraphicsError.graphics;

			return GraphicsServer();
		}
	}

	// Since the context isn't otherwise needed it is just checked to make sure it isn't null here.
	if (SDL_GL_CreateContext(device.window)) {
		if (SDL_GL_LoadLibrary(null) == 0) {
			alias Module = ona.engine.graphics.opengl;
			immutable (Point2) size = getDeviceSize(&device);

			// Load OpenGL functions.
			static foreach (symbolName; __traits(allMembers, Module)) {
				{
					alias Symbol = __traits(getMember, Module, symbolName);

					static if (__traits(getAttributes, Symbol).length) {
						if ((Symbol = cast(typeof(Symbol))SDL_GL_GetProcAddress(
							symbolName.ptr
						)) == null) {
							if (error) *error = GraphicsError.graphics;

							return GraphicsServer();
						}
					}
				}
			}

			glEnable(GL_DEPTH_TEST);

			if (glGetError() == GL_NO_ERROR) {
				glViewport(0, 0, size.x, size.y);

				if (glGetError() == GL_NO_ERROR) {
					GraphicsServer graphics = {
						storage: &storage,
						swapper: &swap,
						clearer: &clear,
						coloredClearer: &clearColor,
						swapIntervalSetter: &setSwapInterval,
						commandDispatcher: &dispatchCommands
					};

					return graphics;
				}
			}
		}
	}

	if (error) *error = GraphicsError.graphics;

	return GraphicsServer();
}

/**
 * Changes the current swap interval for rendering buffers.
 *
 * Not all platforms support every form of swap interval listed in `SwapInterval`, and if the swap
 * interval requested by `swapInterval` is not supported by the device then a notice will be logged
 * and a suitable substitution will be used instead. The actual swap interval that was assigned for
 * the device can be determined by checking the return value.
 *
 * In the scenario that a suitable fallback cannot be identified and used, a critical error is
 * logged and `SwapInterval.immediate` is returned.
 *
 * The default swap interval is `SwapInterval.immediate`, which is to the default value of
 * `SwapInterval`. Because of this knowledge, no method of acquiring the current swap interval
 * state from `Device` is exposed, as it can simply be manually tracked at the calling site.
 *
 * If `device` does not exist then an assertion is raised.
 */

/**
 * Unloads the graphics system.
 */
public void unloadGraphics(ref GraphicsServer graphics) {
	SDL_GL_UnloadLibrary();
}
