module ona.engine.graphics.common;

private {
	import ona.collections;
	import ona.engine.device;
	import ona.image;
	import ona.math;
}

/**
 * Command queue for facillitating asynchronous graphics rasterization.
 */
public struct GraphicsCommands {
	/**
	 * Key used for batching operations by primitive / shader, reducing the number of context
	 * switches that need to take place between draws and allowing for instancing to be used while
	 * rendering.
	 *
	 * The layout of `PrimitiveDrawKey` requires that it be no larger than 64-bits, as it takes
	 * advantage of pointer reinterpreting to masquerade as a `ulong`, which is in turn used as a
	 * unique identifier that is different for every primitive / material permutation.
	 */
	struct PrimitiveDrawKey {
		/**
		 * Identifier for the primitive resource to use when rasterizing a given batch.
		 */
		ResourceId primitiveId;

		/**
		 * Identifier for the material resource to use when rasterizing a given batch.
		 */
		ResourceId materialId;
	}

	/**
	 * Command queue for drawing primitive graphics.
	 */
	Table!(PrimitiveDrawKey, Appender!(Matrix), (ref key) => *cast(ulong*)&key) primitiveDrawers;
}

/**
 * Exposes a dynamically bound API to an implementation-specific graphics backend.
 */
public struct GraphicsServer {
	alias Swapper = extern (C) void function(Device* device) nothrow;

	alias Clearer = extern (C) void function() nothrow;

	alias ColoredClearer = extern (C) void function(Color clearColor) nothrow;

	alias CommandDispatcher = extern (C) void function(
		void* storage,
		GraphicsCommands* commands
	) nothrow;

	alias MaterialNewer = extern (C) ResourceId function(
		void* storage,
		ResourceId rasteriserId,
		void[] materialData,
		Image texture,
		ResourceId shaderId,
		MaterialError* error
	) nothrow;

	alias PrimitiveNewer = extern (C) ResourceId function(
		void* storage,
		ResourceId rasteriserId,
		scope const (void[]) vertexData,
		PrimitiveError* error
	) nothrow;

	alias RasterizerNewer = extern (C) ResourceId function(
		void* storage,
		const (VertexLayout) vertexLayout,
		const (MaterialProperties) materialProperties,
		ResourceId shaderId,
		RasterizerError* error
	) nothrow;

	alias SwapIntervalSetter = extern (C) SwapInterval function(SwapInterval swapInterval) nothrow;

	/**
	 * Handle pointing to the implementation-specific state and resource storage.
	 */
	void* storage;

	/**
	 * Graphics backbuffer swapping function.
	 */
	Swapper swapper;

	/**
	 * Graphics backbuffer clearing function.
	 */
	Clearer clearer;

	/**
	 * Graphics backbuffer colored clearing function.
	 */
	ColoredClearer coloredClearer;

	/**
	 * Graphics backbuffer swapping mode setter function.
	 */
	SwapIntervalSetter swapIntervalSetter;

	/**
	 * Dispatches all commands in a `GraphicsCommand` and clears its command queue.
	 */
	CommandDispatcher commandDispatcher;

	/**
	 * Material graphics resource allocation and initialization function.
	 *
	 * See `MaterialError` for more information on error handling.
	 */
	MaterialNewer materialNewer;

	/**
	 * Primitive graphics resource allocation and initialization function.
	 *
	 * See `PrimitiveError` for more information on error handling.
	 */
	PrimitiveNewer primitiveNewer;
}

/**
 * Runtime-serializable meta-information about the a subsection of memory within a buffer for
 * treating it like a collection of vertices.
 */
public struct LayoutAttribute {
	/**
	 * Shader input index to bind the vertex data to.
	 *
	 * Each vertex attribute must declare a unique location value for the shader to correctly
	 * interact with it.
	 */
	ubyte location;

	/**
	 * Vertex data type.
	 */
	TypeDescriptor type;

	/**
	 * Vertex component count.
	 *
	 * A common example of a type with trailing components is a vector or a matrix, however this
	 * rule also includes arrays.
	 */
	ushort components;

	/**
	 * Vertex data name.
	 *
	 * Mostly arbitrary and always user-defined, this value has no requirements in its formatting
	 * or uniqueness.
	 */
	string name;

	/**
	 * Byte offset between vertex data in interleaved arrays of multiple values.
	 *
	 * For example, with this given layout:
	 *   - `Float3 pos`
	 *   - `Float2 uv`
	 *
	 * Then the offsets would be:
	 *   - `0 bytes`
	 *   - `12 bytes`
	 *
	 * Because a `3`-component `float` is comprised of values that are `4` bytes each.
	 */
	uint offset;
}

/**
 * Runtime-serializable meta-information about the layout of a memory buffer for treating it like a
 * block of material properties.
 */
public struct MaterialProperties {
	/**
	 * Sequence of property attributes describing meta-information about how the material memory
	 * should be interpreted.
	 */
	LayoutAttribute[] properties;

	/**
	 * Validates that `data` may be used with the `MaterialLayout`, returning `true` if valid,
	 * otherwise `false`.
	 */
	bool validate(const (void)[] data) const pure nothrow {
		size_t propertiesSize;

		foreach (property; this.properties) {
			propertiesSize += (typeDescriptorSize(property.type) * property.components);
		}

		return (data.length == propertiesSize);
	}
}

/**
 * A source of information used to construct a `Shader`.
 */
public struct ShaderSource {
	/**
	 * Meta-info describing how to interpret the source.
	 */
	ShaderType type;

	/**
	 * Source text.
	 */
	string text;
}

/**
 * Runtime-serializable meta-information about the layout of a memory buffer for treating it like a
 * collection of vertices.
 */
public struct VertexLayout {
	/**
	 * Byte size of a vertex.
	 */
	size_t vertexSize;

	/**
	 * Sequence of vertex attributes describing meta-information about how the vertex memory should
	 * be interpreted.
	 */
	LayoutAttribute[] attributes;

	/**
	 * Validates that `data` may be used with the `VertexLayout`, returning `true` if valid,
	 * otherwise `false`.
	 */
	bool validate(const (void)[] data) const pure nothrow {
		if (this.vertexSize && ((data.length % this.vertexSize) == 0)) {
			size_t propertiesSize;

			foreach (attribute; this.attributes) {
				propertiesSize += (typeDescriptorSize(attribute.type) * attribute.components);
			}

			return (data.length == propertiesSize);
		}

		return false;
	}
}

/**
 * Error codes used when loading a `GraphicsServer`.
 *
 * `GraphicsError.graphics` is a generic error that occurs whenever an implementation-specific
 * error occurs in the backend that can't be adequately expressed in any useful way.
 */
public enum GraphicsError {
	none,
	graphics
}

/**
 * Error codes used when creating materials.
 *
 * `MaterialError.badImage` occurs when an invalid `Image` is supplied to the graphics API.
 * Typically the problem is caused by using an empty `Image` that has no pixel data, but may also
 * be caused by an `Image` that excedes the max texture size of the platform hardware.
 *
 * `MaterialError.outOfResources` is a catch-all error for regular memory allocations and / or
 * graphics API-specific resource allocation fails.
 *
 * `MaterialError.badProperties` occurs when the provided property data is invalid for the target
 * shader. See `MaterialProperties.validate` for more information on what constitutes invalid data.
 *
 * `MaterialError.graphics` is a generic error that occurs whenever an implementation-specific
 * error occurs in the backend that can't be adequately expressed in any useful way.
 */
public enum MaterialError {
	none,
	graphics,
	badImage,
	outOfResources,
	badProperties
}

/**
 * Error codes used when creating primitives.
 *
 * `PrimitiveError.outOfResources` is a catch-all error for regular memory allocations and / or
 * graphics API-specific resource allocation fails.
 *
 * `PrimitiveError.badVertices` occurs when the provided vertex data is invalid for the target
 * shader. See `VertexLayout.validate` for more information on what constitutes invalid data.
 *
 * `PrimitiveError.graphics` is a generic error that occurs whenever an implementation-specific
 * error occurs in the backend that can't be adequately expressed in any useful way.
 */
public enum PrimitiveError {
	none,
	graphics,
	outOfResources,
	badVertices
}

/**
 * Error codes used when creating rasterizers.
 */
public enum RasterizerError {
	none,
	graphics
}

/**
 * Error codes used when compiling, linking, and verifying a `Shader`.
 */
public enum ShaderError {
	none,
	outOfResources,
	badSources,
	compilation,
	linking,
	validation
}

/**
 * Composite identifiers for shader data. Values can be combined using bit flag syntax to infer
 * composite or linked shaders.
 */
public enum ShaderType {
	empty = 0,
	vertex = 0x1,
	fragment = 0x2
}

/**
 * The time at which the front and back rendering buffers are swapped to display a new frame; With
 * "immediate" being as fast as the device can output swaps, "fixed" being synchronized with the
 * vertical retrace of the device, and "adaptive" being like "fixed" but with better handling when
 * a vertical retrace is missed - such as when dropped frames occur.
 */
public enum SwapInterval {
	immediate,
	fixed,
	adaptive
}

/**
 * Describes a shader-serializable data type.
 */
public enum TypeDescriptor {
	byte_,
	ubyte_,
	short_,
	ushort_,
	int_,
	uint_,
	float_,
	double_
}

/**
 * Error codes used when creating uniforms.
 */
public enum UniformError {
	none,
	outOfResources
}

private size_t typeDescriptorSize(TypeDescriptor typeDescriptor) pure nothrow {
	final switch (typeDescriptor) with (TypeDescriptor) {
		case byte_, ubyte_: return 1;

		case short_, ushort_: return 2;

		case int_, uint_: return 4;

		case float_: return 4;

		case double_: return 8;
	}
}
