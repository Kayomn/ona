module ona.engine.graphics;

public {
	import ona.engine.graphics.common;
	import ona.engine.graphics.opengl;
}

private {
	import ona.collections;
	import ona.engine.device;
	import ona.image;
	import ona.math;
	import ona.memory;
}

/**
 * A handle to a simple composite of vertices that does not store any indexing data. `Primitive`
 * vertices are rendered in order of appearance from front to back. `Primitive`s are ideal for
 * representing simple 2D geometry that would not benefit from vertex indexing.
 */
public struct Primitive2D {
	/**
	 * Resource handle.
	 */
	ResourceId id;
}

/**
 * A handle to a grouping of shading information that details what to fill the inside of a
 * `Primitive` with. `Material`s are `Images` on steroids, able to add to or completely redefine
 * shading behavior with property blocks and shader overrides.
 */
public struct Material2D {
	/**
	 * Resource handle.
	 */
	ResourceId id;
}

/**
 * A handle to a program that manages the dispatch of rasterisation commands to the device display.
 */
public struct Rasterizer2D {
	/**
	 * Resource handle.
	 */
	ResourceId id;
}

/**
 * Runtime-dynamic program used for processing graphical data.
 */
public struct Shader {
	/**
	 * Resource handle.
	 */
	ResourceId id;
}

/**
 * Represents a position, orientation, and scale in 2D space.
 */
public struct Transform2D {
	/**
	 * X-axis used for skewing and scaling.
	 *
	 * Combined with `Transform2D.yAxis` this can be used to rotate the `Transform`.
	 */
	Vector2 xAxis;

	/**
	 * Y-axis used for skewing and scaling.
	 *
	 * Combined with `Transform2D.yAxis` this can be used to rotate the `Transform`.
	 */
	Vector2 yAxis;

	/**
	 * Origin position in 2D space.
	 */
	Vector2 origin;

	/**
	 * Converts the `Transform2D` into a `Matrix`.
	 *
	 * Typically this data representation is only needed when being passed to a low-level renderer
	 * component.
	 */
	Matrix toMatrix() pure const nothrow {
		return Matrix();
	}
}

/**
 * Renderable 2D point.
 */
public struct Vertex2D {
	/**
	 * Position in 2D space.
	 */
	Vector2 position;

	/**
	 * Normalized material coordinates between `0` and `1`, with `Vector2(0f, 0f)` being the top-
	 * left and `Vector2(1f, 1f)` as the bottom-right.
	 */
	Vector2 uv;
}

/**
 * Clears the `graphics` backbuffer to black.
 */
public void clearGraphics(ref GraphicsServer graphics) {
	graphics.clearer();
}

/**
 * Attempts to create a `Material2D` from `image` and `tint`. If an error occurs then an invalid
 * `Material2D` is returned and the error is written to `error`, provided it is not `null`.
 *
 * See `ona.graphics.common.MaterialError` for more information on error handling.
 */
pragma(mangle, "CreateMaterial2D")
public extern (C) Material2D createMaterial2D(
	Device* device,
	Image image,
	Color tint,
	MaterialError* error
) nothrow {
	return Material2D(device.graphics.materialNewer(
		&device.graphics,
		device.rasterizer2D.id,
		bytesOf(tint.normalized()),
		image,
		0,
		error
	));
}

/**
 * Attempts to create a `Primitive2D` from `vertices`. If an error occurs then an invalid
 * `Primitive2D` is returned and the error is written to `error`, provided it is not `null`.
 *
 * See `ona.graphics.common.PrimitiveError` for more information on error handling.
 */
pragma(mangle, "CreatePrimitive2D")
public extern (C) Primitive2D createPrimitive2D(
	Device* device,
	scope const (Vertex2D)[] vertices,
	PrimitiveError* error
) nothrow {
	return Primitive2D(device.graphics.primitiveNewer(
		&device.graphics,
		device.rasterizer2D.id,
		vertices,
		error
	));
}

/**
 * Queues `primitive` with `material` for drawing at `transform`.
 */
pragma(mangle, "DrawPrimitive2D")
public extern (C) void drawPrimitive2D(
	GraphicsCommands* commands,
	Primitive2D primitive,
	Material2D material,
	Transform2D transform
) nothrow {
	Appender!(Matrix)* drawBatch = commands.primitiveDrawers.lookupOrInsert(
		GraphicsCommands.PrimitiveDrawKey(primitive.id, material.id),
		null
	);

	if (drawBatch) {
		drawBatch.append(transform.toMatrix());
	}
}

/**
 * Clears the `graphics` backbuffer to black.
 */
pragma(mangle, "SetSwapInterval")
public extern (C) void setSwapInterval(Device* device, SwapInterval swapInterval) nothrow {
	device.graphics.swapIntervalSetter(swapInterval);
}
