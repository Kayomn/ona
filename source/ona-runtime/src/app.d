module app;

private {
	import ona.engine;
	import ona.lua;
	import ona.math;
	import ona.os;
	import ona.string;
}

private extern (C) void main() {
	LuaState lua = createLuaState(null);

	scope (exit) lua.release();

	if (lua.executeFile(String("./config.lua"))) {
		DeviceError deviceError;

		Device device = createDevice(
			lua.getGlobal([String("Device"), String("title")]).toString(),
			Point2(
				lua.getGlobal([String("Device"), String("width")]).toInteger(),
				lua.getGlobal([String("Device"), String("height")]).toInteger()
			),
			&deviceError
		);

		scope (exit) device.release();

		if (!deviceError) {
			LuaVar moduleNames = lua.getGlobal([String("Engine"), String("modules")]);

			if (moduleNames.luaTypeOf() == LuaType.table) {
				size_t moduleNamesCount = lua.valueLength(moduleNames);

				for (size_t i = 1; i <= moduleNamesCount; i += 1) {
					String moduleName = lua.valueIndex(moduleNames, i).toString();

					if (!loadDeviceModule(device, moduleName)) {
						log(LogLevel.warning, "Failed to load module ", moduleName.all());
					}
				}
			}

			while (device.update()) {
				device.submit();
			}
		}
	}
}
