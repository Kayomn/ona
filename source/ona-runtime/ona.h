#pragma once

typedef unsigned long size_t;

typedef struct {} Device;

typedef struct {} DeviceFrame;

#define NULL (void*)0

#define Vector2_Zero (Vector2){0, 0}

#define Vector3_Zero (Vector3){0, 0, 0}

#define Vector4_Zero (Vector4){0, 0, 0, 0}

typedef struct {
	float x, y;
} Vector2;

typedef struct {
	float x, y, z;
} Vector3;

typedef struct {
	int x, y, z, w;
} Vector4;

Vector2 Vector2_Add(Vector2 a, Vector2 b) {
	return (Vector2){(a.x + b.x), (a.y + b.y)};
}

Vector3 Vector3_Add(Vector3 a, Vector3 b) {
	return (Vector3){(a.x + b.x), (a.y + b.y), (a.z + b.z)};
}

Vector4 Vector4_Add(Vector4 a, Vector4 b) {
	return (Vector4){(a.x + b.x), (a.y + b.y), (a.z + b.z), (a.w + b.w)};
}

#define Point2_Zero (Point2){0, 0}

#define Point3_Zero (Point3){0, 0, 0}

#define Point4_Zero (Point4){0, 0, 0, 0}

typedef struct {
	int x, y;
} Point2;

typedef struct {
	int x, y, z;
} Point3;

typedef struct {
	int x, y, z, w;
} Point4;

typedef enum {
	IoFlags_None,
	IoFlags_Read = 0x1,
	IoFlags_Write = 0x2
} IoFlags;

typedef struct {
	unsigned char raw[24];
} String;

typedef struct {
	int handle;

	IoFlags accessFlags;
} File;

extern File OutFile;

typedef enum {
	FileWriteError_None,
	FileWriteError_BadFile,
	FileWriteError_BadAccess,
	FileWriteError_OutOfResources,
	FileWriteError_Io
} FileWriteError;

void RegisterDeviceSystem(
	Device* device,
	size_t dataSize,
	void* initCallback,
	void* processCallback,
	void* exitCallback
);

Point2 GetDeviceSize(Device* device);

String GetDeviceTitle(Device* device);

size_t WriteFile(File file, size_t inLength, void* inBuffer, FileWriteError* error);
