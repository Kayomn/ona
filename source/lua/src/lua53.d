module lua53;

/**
 * File taken from:
 * https://github.com/BindBC/bindbc-lua/blob/master/source/bindbc/lua/v53/bindstatic.d
 */

private {
    import core.stdc.config : c_long;
	import core.stdc.stdarg : va_list;
	import core.stdc.stdint : intptr_t;
	import core.stdc.stdio : BUFSIZ, FILE;
}

// Macros
@nogc nothrow {
    // luaconf.h
    int lua_equal(lua_State* L, int idx1, int idx2) {
        pragma(inline, true)
        return lua_compare(L, idx1, idx2, LUA_OPEQ);
    }

    int lua_lessthan(lua_State* L, int idx1, int idx2) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        return lua_compare(L, idx1, idx2, LUA_OPLT);
    }

    // lauxlib.h
    void luaL_checkversion(lua_State* L) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        luaL_checkversion_(L, LUA_VERSION_NUM, LUAL_NUMSIZES);
    }

    int luaL_loadfile(lua_State* L, const(char)* filename) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        return luaL_loadfilex(L, filename, null);
    }

    void luaL_newlibtable(lua_State* L, const(luaL_Reg)[] l) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        lua_createtable(L, 0, cast(int)l.length - 1);
    }

    void luaL_newlib(lua_State* L, const(luaL_Reg)[] l) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        luaL_newlibtable(L, l);
        luaL_setfuncs(L, l.ptr, 0);
    }

    void luaL_argcheck(lua_State* L, bool cond, int arg, const(char)* extramsg) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        if(!cond) luaL_argerror(L, arg, extramsg);
    }

    const(char)* luaL_checkstring(lua_State* L, int arg) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        return luaL_checklstring(L, arg, null);
    }

    const(char)* luaL_optstring(lua_State* L, int arg, const(char)* d) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        return luaL_optlstring(L, arg, d, null);
    }

    const(char)* luaL_typename(lua_State* L, int i) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        return lua_typename(L, lua_type(L, i));
    }

    bool luaL_dofile(lua_State* L, const(char)* filename) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        return luaL_loadfile(L, filename) != 0 || lua_pcall(L, 0, LUA_MULTRET, 0) != 0;
    }

    bool luaL_dostring(lua_State* L, const(char)* str) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        return luaL_loadstring(L, str) != 0 || lua_pcall(L, 0, LUA_MULTRET, 0) != 0;
    }

    void luaL_getmetatable(lua_State* L, const(char)* tname) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        lua_getfield(L, LUA_REGISTRYINDEX, tname);
    }

    // TODO: figure out what luaL_opt is supposed to do

    int luaL_loadbuffer(lua_State *L, const(char)* buff, size_t sz, const(char)* name) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        return luaL_loadbufferx(L, buff, sz, name, null);
    }

    void luaL_addchar(luaL_Buffer* B, char c) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        if(B.n < B.size || luaL_prepbuffsize(B, 1)) { // @suppress(dscanner.style.undocumented_declaration)
            B.b[B.n++] = c;
        }
    }

    void luaL_addsize(luaL_Buffer* B, size_t s) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        B.n += s;
    }

    char* luaL_prepbuffer(luaL_Buffer* B) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        return luaL_prepbuffsize(B, LUAL_BUFFERSIZE);
    }

    lua_Unsigned luaL_checkunsigned(lua_State* L, int a) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        return cast(lua_Unsigned)luaL_checkinteger(L, a);
    }

    lua_Unsigned luaL_optunsigned(lua_State* L, int a, lua_Unsigned d) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        return cast(lua_Unsigned)luaL_optinteger(L, a, d);
    }

    int luaL_checkint(lua_State* L, int a) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        return cast(int)luaL_checkinteger(L, a);
    }

    int luaL_optint(lua_State* L, int a, int d) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        return cast(int)luaL_optinteger(L, a, d);
    }

    c_long luaL_checklong(lua_State* L, int a) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        return cast(c_long)luaL_checkinteger(L, a);
    }

    c_long luaL_optlong(lua_State* L, int a, int d) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        return cast(c_long)luaL_optinteger(L, a, d);
    }

    // lua.h
    int lua_upvalueindex(int i) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        return LUA_REGISTRYINDEX - i;
    }

    void lua_call(lua_State* L, int n, int r) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        lua_callk(L, n, r, 0, null);
    }

    int lua_pcall(lua_State* L, int n, int r, int f) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        return lua_pcallk(L, n, r, f, 0, null);
    }

    int lua_yield(lua_State* L, int n) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        return lua_yieldk(L, n, 0, null);
    }

    void* lua_getextraspace(lua_State* L) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        return cast(void*)((cast(char*)L) - LUA_EXTRASPACE);
    }

    lua_Number lua_tonumber(lua_State* L, int i) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        return lua_tonumberx(L, i, null);
    }

    lua_Integer lua_tointeger(lua_State* L, int i) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        return lua_tointegerx(L, i, null);
    }

    void lua_pop(lua_State* L, int n) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        lua_settop(L, -n - 1);
    }

    void lua_newtable(lua_State* L) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        lua_createtable(L, 0, 0);
    }

    void lua_register(lua_State* L, const(char)* n, lua_CFunction f) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        lua_pushcfunction(L, f);
        lua_setglobal(L, n);
    }

    void lua_pushcfunction(lua_State* L, lua_CFunction f) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        lua_pushcclosure(L, f, 0);
    }

    bool lua_isfunction(lua_State* L, int n) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        return lua_type(L, n) == LUA_TFUNCTION;
    }

    bool lua_istable(lua_State* L, int n) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        return lua_type(L, n) == LUA_TTABLE;
    }

    bool lua_islightuserdata(lua_State* L, int n) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        return lua_type(L, n) == LUA_TLIGHTUSERDATA;
    }

    bool lua_isnil(lua_State* L, int n) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        return lua_type(L, n) == LUA_TNIL;
    }

    bool lua_isboolean(lua_State* L, int n) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        return lua_type(L, n) == LUA_TBOOLEAN;
    }

    bool lua_isthread(lua_State* L, int n) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        return lua_type(L, n) == LUA_TTHREAD;
    }

    bool lua_isnone(lua_State* L, int n) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        return lua_type(L, n) == LUA_TNONE;
    }

    bool lua_isnoneornil(lua_State* L, int n) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        return lua_type(L, n) <= 0;
    }

    void lua_pushliteral(lua_State* L, const(char)[] s) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        lua_pushlstring(L, s.ptr, s.length);
    }

    void lua_pushglobaltable(lua_State* L) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        lua_rawgeti(L, LUA_REGISTRYINDEX, LUA_RIDX_GLOBALS);
    }

    const(char)* lua_tostring(lua_State* L, int i) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        return lua_tolstring(L, i, null);
    }

    void lua_insert(lua_State* L, int idx) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        lua_rotate(L, idx, 1);
    }

    void lua_remove(lua_State* L, int idx) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        lua_rotate(L, idx, -1);
        lua_pop(L, 1);
    }

    void lua_replace(lua_State* L, int idx) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        lua_copy(L, -1, idx);
        lua_pop(L, 1);
    }

    void lua_pushunsigned(lua_State* L, lua_Unsigned n) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        lua_pushinteger(L, cast(lua_Integer)n);
    }

    lua_Unsigned lua_tounsignedx(lua_State* L, int i, int* pi) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        return cast(lua_Unsigned)lua_tointegerx(L, i, pi);
    }

    lua_Unsigned lua_tounsigned(lua_State* L, int i) { // @suppress(dscanner.style.undocumented_declaration)
        pragma(inline, true)
        return lua_tounsignedx(L, i, null);
    }
}

// compatibility function aliases
// luaconf.h
alias lua_strlen = lua_rawlen;
alias lua_objlen = lua_rawlen;

// luaconf.h
alias LUA_INT32 = int;

alias LUAI_UMEM = size_t;

alias LUAI_MEM = ptrdiff_t;

alias LUA_NUMBER = double;

alias LUA_INTEGER = ptrdiff_t;

alias LUA_UNSIGNED = uint;

alias LUA_KCONTEXT =  intptr_t;

enum LUAI_MAXSTACK = 1_000_000; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_EXTRASPACE = (void*).sizeof; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_IDSIZE = 60; // @suppress(dscanner.style.undocumented_declaration)

enum LUAL_BUFFERSIZE = cast(int)(0x80 * (void*).sizeof * lua_Integer.sizeof); // @suppress(dscanner.style.undocumented_declaration)

// lauxlib.h
enum LUA_ERRFILE = LUA_ERRERR+1; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_LOADED_TABLE = "_LOADED"; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_PRELOAD_TABLE = "_PRELOAD"; // @suppress(dscanner.style.undocumented_declaration)

struct luaL_Reg { // @suppress(dscanner.style.undocumented_declaration)
	const(char)* name; // @suppress(dscanner.style.undocumented_declaration)
	lua_CFunction func; // @suppress(dscanner.style.undocumented_declaration)
}

enum LUAL_NUMSIZES = lua_Integer.sizeof * 16 + lua_Number.sizeof; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_NOREF = -2; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_REFNIL = -1; // @suppress(dscanner.style.undocumented_declaration)

struct luaL_Buffer { // @suppress(dscanner.style.undocumented_declaration)
	char* b; // @suppress(dscanner.style.undocumented_declaration)

	size_t size; // @suppress(dscanner.style.undocumented_declaration)

	size_t n; // @suppress(dscanner.style.undocumented_declaration)

	lua_State* L; // @suppress(dscanner.style.undocumented_declaration)

	char[LUAL_BUFFERSIZE] initb; // @suppress(dscanner.style.undocumented_declaration)
}

alias LUA_FILEHANDLE = FILE*; // @suppress(dscanner.style.undocumented_declaration)

struct luaL_Stream { // @suppress(dscanner.style.undocumented_declaration)
	FILE* f; // @suppress(dscanner.style.undocumented_declaration)

	lua_CFunction closef; // @suppress(dscanner.style.undocumented_declaration)
}

// lua.h
enum LUA_VERSION_MAJOR = "5"; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_VERSION_MINOR = "3"; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_VERSION_NUM = 503; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_VERSION_RELEASE = 5; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_VERSION = "Lua " ~ LUA_VERSION_MAJOR ~ "." ~ LUA_VERSION_MINOR; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_RELEASE = LUA_VERSION ~ "." ~ LUA_VERSION_RELEASE; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_SIGNATURE = "\x1bLua"; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_MULTRET = -1; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_REGISTRYINDEX = -LUAI_MAXSTACK - 1000; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_OK = 0; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_YIELD = 1; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_ERRRUN = 2; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_ERRSYNTAX = 3; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_ERRMEM = 4; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_ERRGCMM = 5; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_ERRERR = 6; // @suppress(dscanner.style.undocumented_declaration)

struct lua_State; // @suppress(dscanner.style.undocumented_declaration)

nothrow {
	alias lua_CFunction = extern (C) int function(lua_State*);

	alias lua_KFunction = extern (C) int function(lua_State*,int,lua_KContext);

	alias lua_Reader = extern (C) const(char)* function(lua_State*,void*,size_t);

	alias lua_Writer = extern (C) int function(lua_State*,const(void)*,size_t,void*);

	alias lua_Alloc = extern (C) void* function(void*,void*,size_t,size_t);
}

enum LUA_TNONE = -1; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_TNIL = 0; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_TBOOLEAN = 1; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_TLIGHTUSERDATA = 2; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_TNUMBER = 3; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_TSTRING = 4; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_TTABLE = 5; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_TFUNCTION = 6; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_TUSERDATA = 7; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_TTHREAD = 8; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_NUMTAGS = 9; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_MINSTACK = 20; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_RIDX_MAINTHREAD = 1; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_RIDX_GLOBALS = 2; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_RIDX_LAST = LUA_RIDX_GLOBALS; // @suppress(dscanner.style.undocumented_declaration)

alias lua_Number = LUA_NUMBER;

alias lua_Integer = LUA_INTEGER;

alias lua_Unsigned = LUA_UNSIGNED;

alias lua_KContext = LUA_KCONTEXT;

enum LUA_OPADD = 0; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_OPSUB = 1; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_OPMUL = 2; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_OPMOD = 3; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_OPPOW = 4; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_OPDIV = 5; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_OPIDIV = 6; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_OPBAND = 7; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_OPBOR = 8; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_OPBXOR = 9; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_OPSHL = 10; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_OPSHR = 11; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_OPUNM = 12; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_OPBNOT = 13; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_OPEQ = 0; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_OPLT = 1; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_OPLE = 2; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_GCSTOP = 0; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_GCRESTART = 1; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_GCCOLLECT = 2; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_GCCOUNT = 3; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_GCCOUNTB = 4; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_GCSTEP = 5; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_GCSETPAUSE = 6; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_GCSETSTEPMUL = 7; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_GCISRUNNING = 9; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_HOOKCALL = 0; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_HOOKRET = 1; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_HOOKLINE = 2; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_HOOKCOUNT = 3; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_HOOKTAILRET = 4; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_MASKCALL = 1 << LUA_HOOKCALL; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_MASKRET = 1 << LUA_HOOKRET; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_MASKLINE = 1 << LUA_HOOKLINE; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_MASKCOUNT = 1 << LUA_HOOKCOUNT; // @suppress(dscanner.style.undocumented_declaration)

struct lua_Debug { // @suppress(dscanner.style.undocumented_declaration)
	int event; // @suppress(dscanner.style.undocumented_declaration)

	const(char)* name; // @suppress(dscanner.style.undocumented_declaration)

	const(char)* namewhat; // @suppress(dscanner.style.undocumented_declaration)

	const(char)* what; // @suppress(dscanner.style.undocumented_declaration)

	const(char)* source; // @suppress(dscanner.style.undocumented_declaration)

	int currentline; // @suppress(dscanner.style.undocumented_declaration)

	int linedefined; // @suppress(dscanner.style.undocumented_declaration)

	int lastlinedefined; // @suppress(dscanner.style.undocumented_declaration)

	ubyte nups; // @suppress(dscanner.style.undocumented_declaration)

	ubyte nparams; // @suppress(dscanner.style.undocumented_declaration)

	char isvararg; // @suppress(dscanner.style.undocumented_declaration)

	char istailcall; // @suppress(dscanner.style.undocumented_declaration)

	char[LUA_IDSIZE] short_src; // @suppress(dscanner.style.undocumented_declaration)

	private void* i_ci;
}

alias lua_Hook = void function(lua_State*,lua_Debug*) nothrow;

// lualib.h
enum LUA_VERSUFFIX = "_" ~ LUA_VERSION_MAJOR ~ "_" ~ LUA_VERSION_MINOR; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_COLIBNAME = "coroutine"; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_TABLIBNAME = "table"; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_IOLIBNAME = "io"; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_OSLIBNAME = "os"; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_STRLIBNAME = "string"; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_UTF8LIBNAME = "utf8"; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_BITLIBNAME = "bit32"; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_MATHLIBNAME = "math"; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_DBLIBNAME = "debug"; // @suppress(dscanner.style.undocumented_declaration)

enum LUA_LOADLIBNAME = "package"; // @suppress(dscanner.style.undocumented_declaration)

extern(C) @nogc nothrow {
	// lauxlib.h
	void luaL_checkversion_(lua_State*,lua_Number,size_t); // @suppress(dscanner.style.undocumented_declaration)

	int luaL_getmetafield(lua_State*,int,const(char)*); // @suppress(dscanner.style.undocumented_declaration)

	int luaL_callmeta(lua_State*, int,const(char)*); // @suppress(dscanner.style.undocumented_declaration)

	const(char)* luaL_tolstring(lua_State*,int,size_t*); // @suppress(dscanner.style.undocumented_declaration)

	int luaL_argerror(lua_State*,int,const(char)*); // @suppress(dscanner.style.undocumented_declaration)

	const(char)* luaL_checklstring(lua_State*,int,size_t*); // @suppress(dscanner.style.undocumented_declaration)

	const(char)* luaL_optlstring(lua_State*,int,const(char)*,size_t*); // @suppress(dscanner.style.undocumented_declaration)

	lua_Number luaL_checknumber(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	lua_Number luaL_optnumber(lua_State*,int,lua_Number); // @suppress(dscanner.style.undocumented_declaration)

	lua_Integer luaL_checkinteger(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	lua_Integer luaL_optinteger(lua_State*,int,lua_Integer); // @suppress(dscanner.style.undocumented_declaration)

	void luaL_checkstack(lua_State*,int,const(char)*); // @suppress(dscanner.style.undocumented_declaration)

	void luaL_checktype(lua_State*,int,int); // @suppress(dscanner.style.undocumented_declaration)

	void luaL_checkany(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	int luaL_newmetatable(lua_State*,const(char)*); // @suppress(dscanner.style.undocumented_declaration)

	void luaL_setmetatable(lua_State*,const(char)*); // @suppress(dscanner.style.undocumented_declaration)

	void* luaL_testudata(lua_State*,int,const(char)*); // @suppress(dscanner.style.undocumented_declaration)

	void* luaL_checkudata(lua_State*,int,const(char)*); // @suppress(dscanner.style.undocumented_declaration)

	void luaL_where(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	int luaL_error(lua_State*,const(char)*,...); // @suppress(dscanner.style.undocumented_declaration)

	int luaL_checkoption(lua_State*,int,const(char)*); // @suppress(dscanner.style.undocumented_declaration)

	int luaL_fileresult(lua_State*,int,const(char)*); // @suppress(dscanner.style.undocumented_declaration)

	int luaL_execresult(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	int luaL_ref(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	void luaL_unref(lua_State*,int,int); // @suppress(dscanner.style.undocumented_declaration)

	int luaL_loadfilex(lua_State*,const(char)*,const(char)*); // @suppress(dscanner.style.undocumented_declaration)

	int luaL_loadbufferx(lua_State*,const(char)*,size_t,const(char)*,const(char)*); // @suppress(dscanner.style.undocumented_declaration)

	int luaL_loadstring(lua_State*,const(char)*); // @suppress(dscanner.style.undocumented_declaration)

	lua_State* luaL_newstate(); // @suppress(dscanner.style.undocumented_declaration)

	int luaL_len(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	const(char)* luaL_gsub(lua_State*,const(char)*,const(char)*,const(char)*); // @suppress(dscanner.style.undocumented_declaration)

	void luaL_setfuncs(lua_State*,const(luaL_Reg)*,int); // @suppress(dscanner.style.undocumented_declaration)

	int luaL_getsubtable(lua_State*,int,const(char)*); // @suppress(dscanner.style.undocumented_declaration)

	void luaL_traceback(lua_State*,lua_State*,const(char)*,int); // @suppress(dscanner.style.undocumented_declaration)

	void luaL_requiref(lua_State*,const(char)*,lua_CFunction,int); // @suppress(dscanner.style.undocumented_declaration)

	void luaL_buffinit(lua_State*,luaL_Buffer*); // @suppress(dscanner.style.undocumented_declaration)

	char* luaL_prepbuffsize(luaL_Buffer*,size_t); // @suppress(dscanner.style.undocumented_declaration)

	void luaL_addlstring(luaL_Buffer*,const(char)*,size_t); // @suppress(dscanner.style.undocumented_declaration)

	void luaL_addstring(luaL_Buffer*, const(char)*); // @suppress(dscanner.style.undocumented_declaration)

	void luaL_addvalue(luaL_Buffer*); // @suppress(dscanner.style.undocumented_declaration)

	void luaL_pushresult(luaL_Buffer*); // @suppress(dscanner.style.undocumented_declaration)

	void luaL_pushresultsize(luaL_Buffer*,size_t); // @suppress(dscanner.style.undocumented_declaration)

	char* luaL_buffinitsize(lua_State*,luaL_Buffer*,size_t); // @suppress(dscanner.style.undocumented_declaration)

	// lua.h
	lua_State* lua_newstate(lua_Alloc,void*); // @suppress(dscanner.style.undocumented_declaration)

	lua_State* lua_close(lua_State*); // @suppress(dscanner.style.undocumented_declaration)

	lua_State* lua_newthread(lua_State*); // @suppress(dscanner.style.undocumented_declaration)

	lua_CFunction lua_atpanic(lua_State*,lua_CFunction); // @suppress(dscanner.style.undocumented_declaration)

	const(lua_Number)* lua_version(lua_State*); // @suppress(dscanner.style.undocumented_declaration)

	int lua_absindex(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	int lua_gettop(lua_State*); // @suppress(dscanner.style.undocumented_declaration)

	void lua_settop(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	void lua_pushvalue(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	void lua_rotate(lua_State*,int,int); // @suppress(dscanner.style.undocumented_declaration)

	void lua_copy(lua_State*,int,int); // @suppress(dscanner.style.undocumented_declaration)

	int lua_checkstack(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	void lua_xmove(lua_State*,lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	int lua_isnumber(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	int lua_isstring(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	int lua_iscfunction(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	int lua_isinteger(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	int lua_isuserdata(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	int lua_type(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	const(char)* lua_typename(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	lua_Number lua_tonumberx(lua_State*,int,int*); // @suppress(dscanner.style.undocumented_declaration)

	lua_Integer lua_tointegerx(lua_State*,int,int*); // @suppress(dscanner.style.undocumented_declaration)

	int lua_toboolean(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	const(char)* lua_tolstring(lua_State*,int,size_t*); // @suppress(dscanner.style.undocumented_declaration)

	size_t lua_rawlen(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	lua_CFunction lua_tocfunction(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	void* lua_touserdata(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	lua_State* lua_tothread(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	const(void)* lua_topointer(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	void lua_arith(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	int lua_rawequal(lua_State*,int,int); // @suppress(dscanner.style.undocumented_declaration)

	int lua_compare(lua_State*,int,int,int); // @suppress(dscanner.style.undocumented_declaration)

	void lua_pushnil(lua_State*); // @suppress(dscanner.style.undocumented_declaration)

	void lua_pushnumber(lua_State*,lua_Number); // @suppress(dscanner.style.undocumented_declaration)

	void lua_pushinteger(lua_State*,lua_Integer); // @suppress(dscanner.style.undocumented_declaration)

	void lua_pushlstring(lua_State*,const(char)*,size_t); // @suppress(dscanner.style.undocumented_declaration)

	void lua_pushstring(lua_State*,const(char)*); // @suppress(dscanner.style.undocumented_declaration)

	const(char)* lua_pushvfstring(lua_State*,const(char)*,va_list); // @suppress(dscanner.style.undocumented_declaration)

	const(char)* lua_pushfstring(lua_State*,const(char)*,...); // @suppress(dscanner.style.undocumented_declaration)

	void lua_pushcclosure(lua_State*,lua_CFunction,int); // @suppress(dscanner.style.undocumented_declaration)

	void lua_pushboolean(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	void lua_pushlightuserdata(lua_State*,void*); // @suppress(dscanner.style.undocumented_declaration)

	int lua_pushthread(lua_State*); // @suppress(dscanner.style.undocumented_declaration)

	void lua_getglobal(lua_State*,const(char)*); // @suppress(dscanner.style.undocumented_declaration)

	void lua_gettable(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	void lua_getfield(lua_State*,int,const(char)*); // @suppress(dscanner.style.undocumented_declaration)

	int lua_geti(lua_State*,int,lua_Integer); // @suppress(dscanner.style.undocumented_declaration)

	void lua_rawget(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	void lua_rawgeti(lua_State*,int,int); // @suppress(dscanner.style.undocumented_declaration)

	void lua_rawgetp(lua_State*,int,const(void)*); // @suppress(dscanner.style.undocumented_declaration)

	void lua_createtable(lua_State*,int,int); // @suppress(dscanner.style.undocumented_declaration)

	void* lua_newuserdata(lua_State*,size_t); // @suppress(dscanner.style.undocumented_declaration)

	int lua_getmetatable(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	void lua_getuservalue(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	void lua_setglobal(lua_State*,const(char)*); // @suppress(dscanner.style.undocumented_declaration)

	void lua_settable(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	void lua_setfield(lua_State*,int,const(char)*); // @suppress(dscanner.style.undocumented_declaration)

	void lua_seti(lua_State*,int,lua_Integer); // @suppress(dscanner.style.undocumented_declaration)

	void lua_rawset(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	void lua_rawseti(lua_State*,int,int); // @suppress(dscanner.style.undocumented_declaration)

	void lua_rawsetp(lua_State*,int,const(void)*); // @suppress(dscanner.style.undocumented_declaration)

	int lua_setmetatable(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	int lua_setuservalue(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	void lua_callk(lua_State*,int,int,lua_KContext,lua_CFunction); // @suppress(dscanner.style.undocumented_declaration)

	int lua_pcallk(lua_State*,int,int,int,lua_KContext,lua_CFunction); // @suppress(dscanner.style.undocumented_declaration)

	int lua_load(lua_State*,lua_Reader,void*,const(char)*); // @suppress(dscanner.style.undocumented_declaration)

	int lua_dump(lua_State*,lua_Writer,void*); // @suppress(dscanner.style.undocumented_declaration)

	int lua_yieldk(lua_State*,int,lua_KContext,lua_CFunction); // @suppress(dscanner.style.undocumented_declaration)

	int lua_resume(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	int lua_status(lua_State*); // @suppress(dscanner.style.undocumented_declaration)

	int lua_isyieldable(lua_State*); // @suppress(dscanner.style.undocumented_declaration)

	int lua_gc(lua_State*,int,int); // @suppress(dscanner.style.undocumented_declaration)

	int lua_error(lua_State*); // @suppress(dscanner.style.undocumented_declaration)

	int lua_next(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	void lua_concat(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	void lua_len(lua_State*,int); // @suppress(dscanner.style.undocumented_declaration)

	size_t plua_stringtonumber(lua_State*,const(char)*); // @suppress(dscanner.style.undocumented_declaration)

	lua_Alloc lua_getallocf(lua_State*,void**); // @suppress(dscanner.style.undocumented_declaration)

	void lua_setallocf(lua_State*,lua_Alloc,void*); // @suppress(dscanner.style.undocumented_declaration)

	int lua_getstack(lua_State*,int,lua_Debug*); // @suppress(dscanner.style.undocumented_declaration)

	int lua_getinfo(lua_State*,const(char)*,lua_Debug*); // @suppress(dscanner.style.undocumented_declaration)

	const(char)* lua_getlocal(lua_State*,const(lua_Debug)*,int); // @suppress(dscanner.style.undocumented_declaration)

	const(char)* lua_setlocal(lua_State*,const(lua_Debug)*,int); // @suppress(dscanner.style.undocumented_declaration)

	const(char)* lua_getupvalue(lua_State*,int,int); // @suppress(dscanner.style.undocumented_declaration)

	const(char)* lua_setupvalue(lua_State*,int,int); // @suppress(dscanner.style.undocumented_declaration)

	int lua_sethook(lua_State*,lua_Hook,int,int); // @suppress(dscanner.style.undocumented_declaration)

	lua_Hook lua_gethook(lua_State*); // @suppress(dscanner.style.undocumented_declaration)

	int lua_gethookmask(lua_State*); // @suppress(dscanner.style.undocumented_declaration)

	int lua_gethookcount(lua_State*); // @suppress(dscanner.style.undocumented_declaration)

	// lualib.h
	int luaopen_base(lua_State*); // @suppress(dscanner.style.undocumented_declaration)

	int luaopen_coroutine(lua_State*); // @suppress(dscanner.style.undocumented_declaration)

	int luaopen_table(lua_State*); // @suppress(dscanner.style.undocumented_declaration)

	int luaopen_io(lua_State*); // @suppress(dscanner.style.undocumented_declaration)

	int luaopen_os(lua_State*); // @suppress(dscanner.style.undocumented_declaration)

	int luaopen_string(lua_State*); // @suppress(dscanner.style.undocumented_declaration)

	int luaopen_bit32(lua_State*); // @suppress(dscanner.style.undocumented_declaration)

	int luaopen_utf8(lua_State*); // @suppress(dscanner.style.undocumented_declaration)

	int luaopen_math(lua_State*); // @suppress(dscanner.style.undocumented_declaration)

	int luaopen_debug(lua_State*); // @suppress(dscanner.style.undocumented_declaration)

	int luaopen_package(lua_State*); // @suppress(dscanner.style.undocumented_declaration)

	void luaL_openlibs(lua_State*); // @suppress(dscanner.style.undocumented_declaration)
}
