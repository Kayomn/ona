module ona.lua;

private {
	import lua53;
	import ona.collections;
	import ona.memory;
	import ona.string;
}

/**
 * Handle for a Lua virtual machine instance in memory.
 */
public struct LuaState {
	private const (Allocator)* allocator;

	private lua_State* state;

	private Appender!(LuaVar) argBuffer;

	/**
	 * Attempts to call `callable` with `args` passed to it as its input arguments and
	 * `returnBuffer` being used as the output buffer for the return values of `callable`.
	 *
	 * The number of arguments returnable is controlled by the length of `returnBuffer`, and using
	 * a buffer smaller than the actual number of return values is perfectly valid. Any values that
	 * do not fit into `returnBuffer` will be discarded, however.
	 *
	 * If `callable` is executed successfully then `true` is returned. Otherwise, `false` is
	 * returned, indicating that either `callable` is not a function or closure, or there was an
	 * error executing the function.
	 */
	public bool call(size_t n)(
				LuaVar callable, LuaVar[] returnBuffer, LuaVar[n] arguments...) nothrow {

		immutable (int) stackSize = lua_gettop(this.state);

		// Push the supposed function to the stack.
		this.luaPushValue(callable);

		if (lua_isfunction(this.state, -1)) {
			// Confirm that the value is in fact a function.
			// Then push the arguments.
			foreach_reverse (argument; arguments) {
				this.luaPushValue(argument);
			}

			if (lua_pcall(this.state, cast(int)arguments.length, LUA_MULTRET, 0)) {
				// Error.
				lua_remove(this.state, -1);

				return false;
			} else {
				// Collect return values from the stack.
				immutable (size_t) returnCount = (lua_gettop(this.state) - stackSize);

				for (size_t i; (i < returnBuffer.length) && (i < returnCount); i += 1) {
					returnBuffer[i] = this.luaStackToValue(-1);
				}

				return true;
			}
		}

		return false;
	}

	/**
	 * Executes the text in the file located at `filePath` as Lua source code, returning a `Result`
	 * indicating whether or not the text was successfully interpreted.
	 *
	 * If the text inside the file at `filePath` could not be interpreted, or some sort of I/O
	 * error occured, then the returned `Result` will contain an error message detailing what went
	 * wrong.
	 */
	public bool executeFile(String filePath) nothrow {
		return this.executeImpl!(luaL_dofile)(filePath);
	}

	/**
	 * Executes the text in `luaSource` as Lua source code, returning a `Result` indicating whether
	 * or not the text was successfully interpreted.
	 *
	 * If `luaSource` could not be interpreted then the returned value is `false`, otherwise
	 * `true`.
	 */
	public bool executeSource(String luaSource) nothrow {
		return this.executeImpl!(luaL_dofile)(luaSource);
	}

	private bool executeImpl(alias doFunc)(String str) nothrow {
		if (doFunc(this.state, sentineledSequence(str.all()).ptr) == LUA_OK) {
			return true;
		}

		return false;
	}

	/**
	 * Attempts to search for a value at the global path `symbolPath`, returning it as a `LuaVar`
	 * if found, otherwise returning a nil `LuaVar`.
	 *
	 * `symbolPath` works by converting the individual `String` symbols into a queryable nested
	 * sequence of table fields that originate from a singular global variable - denoted by the
	 * first `symbolPath` value. I.e. to access the field `"bar"` inside of the global table
	 * `"foo"` it would be expressed as `[String("Foo"), String("bar")]`.
	 *
	 * If a field in the path does not yet exist then the function will immediately return the nil
	 * `LuaVar`.
	 */
	public LuaVar getGlobal(scope String[] symbolPath) nothrow {
		if (symbolPath.length) {
			const (char)* sentineledPtr = sentineledSequence(symbolPath[0].all()).ptr;

			if (sentineledPtr) {
				size_t counter = 1;

				lua_getglobal(this.state, sentineledPtr);

				while (lua_istable(this.state, -1) && (counter < symbolPath.length)) {
					sentineledPtr = sentineledSequence(symbolPath[counter].all()).ptr;

					if (!sentineledPtr) {
						// Conversion failed, return nil.
						return LuaVar();
					}

					lua_getfield(this.state, -1, sentineledPtr);

					counter += 1;
				}

				if (counter == symbolPath.length) {
					return this.luaStackToValue(-1);
				}
			}
		}

		return LuaVar();
	}

	private void luaPushValue(LuaVar value) nothrow {
		value.store.match(
			(ref bool val) {
				lua_pushboolean(this.state, val);
			},

			(ref int val) {
				lua_pushinteger(this.state, val);
			},

			(ref float val) {
				lua_pushnumber(this.state, val);
			},

			(ref Ref!(LuaRef) val) {
				lua_rawgeti(this.state, LUA_REGISTRYINDEX, val.handle);
			}
		);
	}

	private LuaVar luaStackToValue(int stackIndex) nothrow {
		switch (lua_type(this.state, stackIndex)) {
			case LUA_TNIL: {
				scope (exit) lua_remove(this.state, -1);

				return LuaVar();
			}

			case LUA_TBOOLEAN: {
				scope (exit) lua_remove(this.state, -1);

				return LuaVar(cast(bool)lua_toboolean(this.state, stackIndex));
			}

			case LUA_TNUMBER: {
				scope (exit) lua_remove(this.state, -1);

				return LuaVar(lua_tonumber(this.state, stackIndex));
			}

			// case LUA_TLIGHTUSERDATA: return LuaVar(lua_touserdata(lua.state, stackIndex));

			case LUA_TSTRING, LUA_TFUNCTION, LUA_TTABLE: {
				Ref!(LuaRef) luaRef =
						makeRef(LuaRef(&this, luaL_ref(this.state, LUA_REGISTRYINDEX)));

				return ((luaRef == nullRef!(LuaRef)) ? LuaVar() : LuaVar(luaRef));
			}

			default: {
				// TODO(Kayomn): Don't do this.
				assert(0);
			}
		}
	}

	/**
	 * Attempts to allocate a reference-counted binding for `nativeClosure`, allowing it to be
	 * called from within the Lua virtual machine instance.
	 *
	 * Closures differ from functions in that they have access to a descriminated scope of values
	 * beyond the function inputs. In native code, closures are handled via D delegates, which in
	 * practice function the same way as a Lua closure.
	 *
	 * If the reference binding is successfully created it is returned inside a `LuaVar`,
	 * otherwise an empty `LuaVar` is returned. The presence is a value can be checked with
	 * `LuaVar.isNil`.
	 */
	public LuaVar newClosure(LuaNativeClosure nativeClosure) nothrow {
		static struct DelegateDecomposer {
			void* ctxPtr;

			void* funPtr;
		}

		extern (C) static wrapper(lua_State* luaState) nothrow {
			LuaState* lua = cast(LuaState*)lua_touserdata(luaState, lua_upvalueindex(3));

			DelegateDecomposer decomposedClosure = {
				ctxPtr: lua_touserdata(luaState, lua_upvalueindex(1)),
				funPtr: lua_touserdata(luaState, lua_upvalueindex(2))
			};

			immutable (int) argCount = lua_gettop(luaState);
			int retCount;

			foreach (i; 0 .. argCount) {
				if (!lua.argBuffer.append(lua.luaStackToValue(-1))) {
					return luaL_error(lua.state, "Out of memory");
				}
			}

			(*cast(LuaNativeClosure*)&decomposedClosure)((LuaVar retValue) {
				lua.luaPushValue(retValue);

				retCount += 1;
			}, lua.argBuffer.slice(0, argCount));

			// The arg buffer is shared between native functions, so it must be reset between
			// uses lest the arguments also then be passed to the next Lua function called.
			lua.argBuffer.clear();

			return 0;
		}

		// TODO(Kayomn): NIGHTMARE NIGHTMARE NIGHTMARE.
		DelegateDecomposer* decomposedClosure = cast(DelegateDecomposer*)&nativeClosure;

		lua_pushlightuserdata(this.state, decomposedClosure.ctxPtr);
		lua_pushlightuserdata(this.state, decomposedClosure.funPtr);
		lua_pushlightuserdata(this.state, &this);
		lua_pushcclosure(this.state, &wrapper, 3);

		return this.luaStackToValue(-1);
	}

	/**
	 * Attempts to allocate a reference-counted binding for `nativeFunction`, allowing it to be
	 * called from within the Lua virtual machine instance.
	 *
	 * Unlike `LuaState.newClosure`, `LuaState.newFunction` does not provide access to the external
	 * scope. This is preferable when a function is pure, and does not need to mutate any external
	 * state
	 *
	 * If the reference binding is successfully created it is returned inside a `LuaVar`,
	 * otherwise an empty `LuaVar` is returned. The presence is a value can be checked with
	 * `LuaVar.isNil`.
	 */
	public LuaVar newFunction(LuaNativeFunction nativeFunction) {
		extern (C) static wrapper(lua_State* luaState) nothrow {
			LuaState* lua = cast(LuaState*)lua_touserdata(luaState, lua_upvalueindex(2));
			auto func = cast(LuaNativeFunction)lua_touserdata(luaState, lua_upvalueindex(1));
			immutable (int) argCount = lua_gettop(luaState);
			int retCount;

			foreach (i; 0 .. argCount) {
				if (!lua.argBuffer.append(lua.luaStackToValue(-1))) {
					return luaL_error(lua.state, "Out of memory");
				}
			}

			func((LuaVar retValue) {
				lua.luaPushValue(retValue);

				retCount += 1;
			}, lua.argBuffer.slice(0, argCount));

			// The arg buffer is shared between native functions, so it must be reset between
			// uses lest the arguments also then be passed to the next Lua function called.
			lua.argBuffer.clear();

			return retCount;
		}

		lua_pushlightuserdata(this.state, nativeFunction);
		lua_pushlightuserdata(this.state, &this);
		lua_pushcclosure(this.state, &wrapper, 2);

		return this.luaStackToValue(-1);
	}

	/**
	 * Attempts to allocate a reference-counted copy of `str`, allowing it to be used as a Lua
	 * string within the Lua virtual machine instance.
	 *
	 * As the function copies the memory `str` in order to take ownership of it, `str` need not
	 * remain in memory after `LuaState.newString` returns.
	 *
	 * If the reference binding is successfully created it is returned inside a `LuaVar`,
	 * otherwise an empty `LuaVar` is returned. The presence is a value can be checked with
	 * `LuaVar.isNil`.
	 */
	public LuaVar newString(String str) {
		lua_pushlstring(this.state, str.pointerOf(), str.count());

		return this.luaStackToValue(-1);
	}

	/**
	 * Releases the `LuaState`, cleaning up any associated resources to it and the underlying
	 * virtual machine.
	 */
	public void release() nothrow {
		lua_close(this.state);
		this.argBuffer.release();
	}

	/**
	 * Attempts to assign `luaVar` to the value at the global path `symbolPath`.
	 *
	 * `symbolPath` works by converting the individual `String` symbols into a queryable nested
	 * sequence of table fields that originate from a singular global variable - denoted by the
	 * first `symbolPath` value. I.e. to access the field `"bar"` inside of the global table
	 * `"foo"` it would be expressed as `[String("Foo"), String("bar")]`.
	 *
	 * If a field in the path does not yet exist then the function will create one while searching
	 * the tables for the value to set.
	 *
	 * Upon successfully creating the value, and any of the missing path specified in `symbolPath`,
	 * `true` is returned. Otherwise, `false` is returned indicating that a non-nil non-table value
	 * value currently exists at one of the fields in `symbolPath`, blocking it from being
	 * generated.
	 */
	public bool setGlobal(scope String[] symbolPath, LuaVar luaVar) nothrow {
		void checkTable(const (char)* token) {
			if (lua_isnoneornil(this.state, -1)) {
				// Build the table if it does not yet exist.
				lua_pop(this.state, -1);
				lua_newtable(this.state);
				lua_setglobal(this.state, token);
				lua_getglobal(this.state, token);
			}
		}

		if (symbolPath.length) {
			const (char)* token = sentineledSequence(symbolPath[0].all()).ptr;

			if (token) {
				if (symbolPath.length > 1) {
					// Nested lookup.
					size_t count = 1;

					lua_getglobal(this.state, token);
					checkTable(token);

					while (lua_istable(this.state, -1) && (count < symbolPath.length)) {
						token = sentineledSequence(symbolPath[count].all()).ptr;

						lua_getfield(this.state, -1, token);

						if ((count + 1) == symbolPath.length) {
							// End up lookup.
							lua_settop(this.state, -2);
							this.luaPushValue(luaVar);
							lua_setfield(this.state, -2, token);

							return true;
						} else {
							// Otherwise keep going.
							checkTable(token);
						}
					}
				} else {
					this.luaPushValue(luaVar);
					lua_setglobal(this.state, token);

					return true;
				}
			}
		}

		return false;
	}

	/**
	 * Returns the `LuaVar` at index `index`, assuming that it is a Lua data type that is
	 * accessible via the index operator. Supported data types for `LuaState.valueIndex` include
	 * Lua tables and any userdata that implements the `"__index"` metamethod.
	 *
	 * `LuaState.valueIndex` is functionally equivalent to the Lua built-in index access operator
	 * "[]".
	 *
	 * Note that `LuaState.valueIndex` uses the **Lua indexing system**, and as such indices values
	 * start at `1` rather than `0`.
	 */
	public LuaVar valueIndex(LuaVar luaVar, size_t index) {
		// Push the value to the stack and get the value at its index.
		this.luaPushValue(luaVar);
		lua_pushinteger(this.state, cast(lua_Integer)index);
		lua_gettable(this.state, -2);

		// Clean up remaining value on the stack.
		scope (exit) lua_pop(this.state, -1);

		return this.luaStackToValue(-1);
	}

	/**
	 * Returns the length of `luaVar`, assuming that it is a Lua data type that has a measurable
	 * length. Supported data types for `LuaState.valueLength` include Lua strings, tables, and any
	 * userdata that implements the `"__len"` metamethod.
	 *
	 * `LuaState.valueLength` is functionally equivalent to the Lua built-in length operator "#".
	 *
	 * If `LuaState.valueLength` is being used frequently then it is recommended that the returned
	 * value be cached, as calls into the virtual machine have more overhead than simply reading
	 * an integral value on the native stack.
	 */
	public size_t valueLength(LuaVar luaVar) {
		long len = void;

		// Push the value to the stack and get its length.
		this.luaPushValue(luaVar);
		lua_len(this.state, -1);

		len = lua_tointeger(this.state, -1);

		// Clean up the two values that were pushed.
		lua_pop(this.state, -1);
		lua_pop(this.state, -1);

		return cast(size_t)len;
	}
}

/**
 * Wraps a polymorphic value retrieved from a `LuaState` instance.
 *
 * While primitive values are returned as exactly that, references returned from the virtual
 * machine are intrinsically linked to the instance. As such, keeping a `LuaVar` around beyond the
 * scope of a `LuaState` and attempting to use it will cause memory violations.
 */
public struct LuaVar {
	private Union!(bool, int, float, Ref!(LuaRef), void*) store;

	private this(Ref!(LuaRef) value) nothrow {
		this.store = value;
	}

	/**
	 * Constructs a `LuaVar` containing `value`.
	 */
	public this(bool value) nothrow {
		this.store = value;
	}

	/**
	 * Constructs a `LuaVar` containing `value`.
	 */
	public this(float value) nothrow {
		this.store = value;
	}

	/**
	 * Constructs a `LuaVar` containing `value`.
	 */
	public this(int value) nothrow {
		this.store = value;
	}

	/**
	 * Returns `true` if the `LuaVar` holds no value, otherwise `false`.
	 *
	 * Note that `LuaVar.isNil` is quicker than using `LuaVar.luaTypeOf` - which has to call into
	 * the Lua virtual machine in order to identify reference types.
	 */
	public bool isNil() pure const nothrow {
		return this.store.hasValue();
	}

	/**
	 * Converts the contained value to its defined integer representation.
	 *
	 * For scalar types the conversion is fairly simple, with booleans becoming either `1` or `0`,
	 * for `true` and `false` respectively, floats being truncated to integers, and integers
	 * themselves simply returning as normal.
	 */
	public int toInteger() const nothrow {
		return this.store.match(
			(ref bool val) => (val ? 1 : 0),
			(ref int val) => val,
			(ref float val) => cast(int)val,
			() => 0
		);

	}

	/**
	 * Converts the contained value to its defined string representation.
	 *
	 * For scalar types the conversion is fairly simple, with booleans becoming either `"true"` or
	 * `"false"`, for `true` and `false` respectively while floats and integers are represented as
	 * base-10 number strings.
	 *
	 * Strings of course are simply represented as their literal string value.
	 *
	 * It should be noted that in all instances, `LuaVar.toString` will create a new `String`
	 * instance in memory - even when the type is a Lua string reference - as it cannot guarantee
	 * the lifetime of the value.
	 */
	public String toString() const nothrow {
		return this.store.match(
			(ref bool val) => String(val ? "true" : "false"),

			(ref int val) => decString(val),

			(ref float val) => decString(val),

			(ref Ref!(LuaRef) val) {
				val.lua.luaPushValue(this);

				switch (lua_type(val.lua.state, -1)) {
					case LUA_TSTRING: {
						size_t length;

						return String(lua_tolstring(val.lua.state, -1, &length)[0 .. length]);
					}

					default: return formatString(
						val.lua.allocator,
						String(strptr(lua_tostring(val.lua.state, -1))),
						'@',
						val.handle
					);
				}
			},

			() => String("type")
		);
	}

	/**
	 * Returns the `LuaType` that identifies the type of the held value within `LuaVar`.
	 *
	 * Note that if all that needs to be confirmed is whether or not the `LuaVar` is nil then
	 * `LuaVar.isNil` may be preferrable, as it is faster.
	 *
	 * See `LuaType` for more information on the supported language types.
	 */
	public LuaType luaTypeOf() nothrow {
		return this.store.match(
			(ref bool val) => LuaType.boolean,

			(ref int val) => LuaType.integer,

			(ref float val) => LuaType.floating,

			(ref Ref!(LuaRef) val) {
				val.lua.luaPushValue(this);

				scope (exit) lua_pop(val.lua.state, -1);

				switch (lua_type(val.lua.state, -1)) {
					case LUA_TSTRING: return LuaType.string;

					case LUA_TFUNCTION: return LuaType.callable;

					case LUA_TTABLE: return LuaType.table;

					default: return LuaType.nil;
				}
			},

			() => LuaType.nil
		);
	}
}

private struct LuaRef {
	LuaState* lua;

	int handle;

	const (Allocator)* allocatorOf() pure const nothrow {
		return (this.lua ? this.lua.allocator : null);
	}

	void release() nothrow {
		if (this.lua) {
			lua_rawgeti(this.lua.state, LUA_REGISTRYINDEX, this.handle);
			lua_remove(this.lua.state, -1);
			luaL_unref(this.lua.state, LUA_REGISTRYINDEX, this.handle);

			this.handle = 0;
			this.lua = null;
		}
	}
}

/**
 * Lua supports a discriminated set of native types, with these values identifying the boxed values
 * within a `LuaVar`.
 *
 * `LuaType.nil` is the default state of a `LuaVar`, holding nothing and being effectively the same
 * as "no value".
 *
 * `LuaType.boolean` is a value that may be in a binary `true` or `false` state.
 *
 * `LuaType.integer` is an integral scalar value capable of representing any integer value within
 * the addressable 32-bit range.
 *
 * `LuaType.float` is a floating-point scalar value capable of representing any number within the
 * limitations of 32-bit accuracy.
 *
 * `LuaType.string` is a native reference to a dynamic character sequence buffer.
 *
 * `LuaType.callable` is a native reference to a function or closure.
 *
 * `LuaType.table` is a native reference to a Lua table data structure.
 */
public enum LuaType {
	nil,
	boolean,
	integer,
	floating,
	string,
	callable,
	table
}

public alias LuaNativeClosure = void delegate(Ret, LuaVar[]) nothrow;

public alias LuaNativeFunction = void function(Ret, LuaVar[]) nothrow;

public alias Ret = void delegate(LuaVar) nothrow;

/**
 * Creates and returns new instance of a `LuaState`.
 */
public LuaState createLuaState(const (Allocator)* allocator) nothrow {
	return LuaState(allocator, luaL_newstate(), Appender!(LuaVar)(allocator));
}
