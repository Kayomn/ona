module ona.stb_image;

private {
	import core.stdc.stdio : FILE, fclose;
	import ona.image;
	import ona.os;
	import ona.math;
	import ona.memory;
	import ona.string;
}

/**
 * Attemps to load image data and create an `Image` from it using `allocator`. A `null` `allocator`
 * will use the default runtime `Allocator`.
 *
 * Should an error occur during `loadStbImage` then an invalid `Image` is returned and any error
 * codes produced are written to `imageError` and `fileError`, provided each is not `null`.
 *
 * If STB_Image fails to allocate memory when loading the image buffer into memory then the result
 * is a `ImageError.outOfMemory` error.
 *
 * Any failed image manipulation operation performed as part of `createImageFrom` will be written
 * to `imageError`, provided it is not `null`.
 *
 * Any failed I/O operation performed as part of `loadStbImage` will be written to `fileError`,
 * provided it is not `null`. See `ona.io.openFile` for more information.
 */
pragma(mangle, "LoadStbImage")
public extern (C) Image loadStbImage(
	const (Allocator)* allocator,
	String filePath,
	ImageError* imageError,
	FileReadError* fileError
) nothrow {
	FileOpenError openError;
	File file = openFile(filePath, IoFlags.read, &openError);

	if (openError) {
		if (fileError) *fileError = FileReadError.badFile;
	} else {
		FILE* cFile = fdopen(file.handle, "r");

		if (cFile) {
			Point2 pixelDimensions = void;

			ubyte* pixelPointer = stbi_load_from_file(
				cFile,
				&pixelDimensions.x,
				&pixelDimensions.y,
				null,
				4
			);

			scope (exit) {
				stbi_image_free(pixelPointer);
				fclose(cFile);
			}

			if (pixelPointer) {
				return createImageFrom(
					allocator,
					pixelDimensions,
					cast(Color*)pixelPointer,
					imageError
				);
			} else if (imageError) {
				*imageError = ImageError.outOfMemory;
			}
		} else {
			file.release();

			if (fileError) *fileError = FileReadError.io;
		}
	}

	return Image();
}

private extern (C) FILE* fdopen(int fd, scope const(char)* flags) nothrow;

private extern (C) void stbi_image_free(void* ptr) nothrow;

private extern (C) const (char)* stbi_failure_reason() nothrow;

private extern (C) ubyte* stbi_load(
	const (char)*,
	int* x,
	int* y,
	int* components,
	int requiredComponents
) nothrow;

private extern (C) ubyte* stbi_load_from_file(
	FILE*,
	int* x,
	int* y,
	int* components,
	int requiredComponents
) nothrow;
